# Time Tracker

A tool designed to easily track how much time you spend for what. 

## Features

* track your times spend on various topics
* calendar and list view to analyse your time consumption
* export data to CSV for further analysis
* Ubuntu Style UI with a dark and a light theme in 8 different color flavors
* Current languages:
  * English
  * German

## Install

The easiest way to install the latest stable version of this app is to download it from the OpenStore:

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/timetracker.matdahl)

Otherwise, this app can also be built using [**clickable**](https://gitlab.com/clickable/clickable).
Once `clickable` is installed on your computer, simply clone this repository, open the root directory of it in the commandline and execute the command `clickable --arch <your--device-architecture>` with your Ubuntu Phone connected and with developer mode enabled. **Clickable** then builds, installs and start the app on your phone.

## License

Copyright (C) 2022  Matthias Dahlmanns

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
