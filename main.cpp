/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QQuickView>

#include <QUrl>
#include <QString>
#include <QDir>
#include <QStandardPaths>

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("timetracker.matdahl");

    qDebug() << "###################################\n"
                "#### Starting   Time Tracker   ####\n"
                "###################################";

    // ensure that all needed directories exist
    if (!QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::DataLocation)))
        qCritical() << "Could not create data directory '" + QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "'";

    QQuickView *view = new QQuickView();
    view->setSource(QUrl("qrc:/Main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
