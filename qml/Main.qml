/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Qt.labs.settings 1.1
import Ubuntu.Components 1.3

import "dialogs"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'timetracker.matdahl'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Component.onCompleted: {
        pages.push(Qt.resolvedUrl("pages/MainPage.qml"))
        if (settings.showManualOnStart)
            manual.show()
    }

    Settings{
        id: settings
        category: "General"
        property int newSessionDefaultStartSecond: 43200
        property int newSessionDefaultDuration: 3600
        property int newSessionDefaultPause: 900
        property bool showManualOnStart: true
    }

    Item{
        id: utils

        function formatTime(minutes){
            const hours = Math.floor(minutes/60)
            const mins  = minutes % 60
            return hours + ":" + (mins>9 ? mins : "0"+mins)
        }
    }

    /* ----- theming ----- */
    Colors{
        id: colors
        defaultIndex: 4
    }
    theme.name: colors.currentThemeName
    backgroundColor: colors.currentBackground

    PageStack{
        id: pages
    }

    ManualPopover{
        id: manual
    }
}
