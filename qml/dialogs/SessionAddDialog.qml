/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Qt.labs.settings 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3
import Ubuntu.Components.Pickers 1.3
import Ubuntu.Components.Popups 1.3

import "../components"
import "../components/listitems"

import TTCore 1.0

Item {
    id: root

    function open(){
        Backend.sessions.sessionIntervalEditModel.startDate = new Date()
        Backend.sessions.sessionIntervalEditModel.clear()
        Backend.sessions.sessionIntervalEditModel.insertInterval(settings.newSessionDefaultDuration,settings.newSessionDefaultStartSecond)
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent
        Dialog{
            id: dialog
            title: i18n.tr("New session")

            Settings{
                id: dialogSettings
                category: "SessionAddDialog"
                property alias topicUid:   inputProject.selectedTopic
                property alias projectUid: inputProject.selectedProject
            }

            Label{
                text: i18n.tr("Description")
            }
            TextField{
                id: inputDescription
                width: dialog.contentWidth
                placeholderText: i18n.tr("insert a description") + " ..."
            }
            Label{
                text: i18n.tr("Project")
            }
            ProjectSelectorButton{
                id: inputProject
                width: dialog.contentWidth
            }

            Label{
                text: i18n.tr("Date")
            }
            SessionStartDateSelectButton{
                id: inputStartDate
                model: Backend.sessions.sessionIntervalEditModel
            }

            Divider{}

            Item{
                height: units.gu(2)
                width: dialog.contentWidth

                Label{
                    id: lbTimeIntervals
                    text: i18n.tr("Time intervals")
                }

                Button{
                    anchors{
                        verticalCenter: lbTimeIntervals.verticalCenter
                        right: parent.right
                    }
                    width: units.gu(12)
                    color: theme.palette.normal.positive
                    Icon{
                        anchors.centerIn: parent
                        height: parent.height-units.gu(0.5)
                        name: "add"
                        color: theme.palette.normal.positiveText
                    }
                    onClicked: reapIntervals.model.insertInterval(settings.newSessionDefaultDuration,settings.newSessionDefaultPause)
                }

            }

            Divider{}

            Column{
                width: dialog.contentWidth
                Repeater{
                    id: reapIntervals
                    model: Backend.sessions.sessionIntervalEditModel
                    delegate: SubsessionListItem{
                        onRemove: reapIntervals.model.removeIntervalAt(index)
                        onDayChanged: reapIntervals.model.updateDayAt(index,start,newDay)
                        onHourChanged: reapIntervals.model.updateHourAt(index,start,newHour)
                        onMinuteChanged: reapIntervals.model.updateMinuteAt(index,start,newMinute)
                    }
                }
            }

            Divider{}

            // -------------------------------------
            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
            Button{
                text: i18n.tr("Add session")
                color: UbuntuColors.orange
                enabled: reapIntervals.model.count > 0 && inputProject.selectedProject > 0
                onClicked: {
                    Backend.sessions.add(inputDescription.text,inputProject.selectedProject)
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
