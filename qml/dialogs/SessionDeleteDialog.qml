/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../components"

import TTCore 1.0

Item {
    id: root

    property int uid

    signal didDelete()

    function open(uid){
        root.uid   = uid
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent
        Dialog{
            id: dialog
            title: i18n.tr("Delete session")

            text: i18n.tr("Do you want to delete this session?") + "<br><br>"
                  + "(" + i18n.tr("This action can't be undone") + ")"

            // -------------------------------------
            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
            Button{
                text: i18n.tr("Delete session")
                color: theme.palette.normal.negative
                onClicked: {
                    Backend.sessions.remove(root.uid)
                    didDelete()
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
