/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../components"

import TTCore 1.0

Item {
    id: root

    property int    uid
    property string title
    property int    colorHue
    property int    colorLightness
    property int    topic

    function open(uid,title,colorHue,colorLightness,topic){
        root.uid   = uid
        root.title = title
        root.colorHue = colorHue
        root.colorLightness = colorLightness
        root.topic = topic
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent
        Dialog{
            id: dialog
            title: i18n.tr("Edit project")

            Label{
               text: i18n.tr("Topic")
            }
            Item{
                height: rectTopicColor.height
                width: dialog.contentWidth
                Rectangle{
                    id: rectTopicColor
                    height: units.gu(3)
                    width: units.gu(2)
                    radius: units.gu(1)
                    color: Backend.topics.getColor(root.topic)
                }
                Label{
                    anchors{
                        verticalCenter: rectTopicColor.verticalCenter
                        left: rectTopicColor.right
                        right: parent.right
                        leftMargin: units.gu(2)
                    }
                    text: Backend.topics.getTitle(root.topic)
                }
            }

            Label{
                text: i18n.tr("Title")
            }
            TextField{
                id: inputTitle
                placeholderText: i18n.tr("Insert a title") + " ..."
                Component.onCompleted: text = root.title
            }

            Label{
                text: i18n.tr("Color")
            }
            ProjectColorSelector{
                id: inputColor
                baseColor: Backend.topics.getColor(root.topic)
                Component.onCompleted: {
                    selectedHue = root.colorHue
                    selectedLightness = root.colorLightness
                }
            }

            // -------------------------------------
            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
            Button{
                text: i18n.tr("Save changes")
                color: UbuntuColors.orange
                enabled: inputTitle.text.length > 0 && inputColor.hasSelection
                onClicked: {
                    Backend.projects.update(root.uid,inputTitle.text,inputColor.selectedHue,inputColor.selectedLightness)
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
