/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

import TTCore 1.0

Item {
    id: root

    function open(){
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent

        Dialog{
            id: dialog
            title: i18n.tr("Edit session's date")

            DatePicker{
                id: datePicker
                date: Backend.sessions.selected.startDate
                mode: "Years|Months|Days"
                minimum: new Date(new Date().getFullYear()-25,1,1)
                maximum: new Date(new Date().getFullYear()+25,12,31)
            }


            Item{
                width: dialog.contentWidth
                height: childrenRect.height

                Row{
                    id: buttonRow
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(2)

                    Button{
                        text: i18n.tr("Cancel")
                        onClicked: PopupUtils.close(dialog)
                    }
                    Button{
                        text: i18n.tr("Save")
                        color: UbuntuColors.orange
                        onClicked:{
                            Backend.sessions.setSelectedStartDate(datePicker.date)
                            PopupUtils.close(dialog)
                        }
                    }
                }
            }
        }
    }
}
