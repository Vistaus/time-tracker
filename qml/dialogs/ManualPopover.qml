/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../components"

Item {
    id: root

    function show(){
        PopupUtils.open(popoverComponent,root)
    }

    Component{
        id: popoverComponent
        Popover{
            id: popover

            Label{
                id: lbTitle
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: units.gu(2)
                }
                textSize: Label.Large
                font.bold: true
                text: i18n.tr("Time Tracker")
            }

            Rectangle{
                anchors{
                    fill: scrollView
                    topMargin: units.gu(-1)
                    bottomMargin: units.gu(-1)
                    margins: units.gu(-2)
                }
                radius: units.gu(1)
                color: theme.palette.normal.base
                opacity: 0.2
            }

            /* ---- manual content ---- */
            ScrollView{
                    id: scrollView
                    anchors{
                        top: lbTitle.bottom
                        left: parent.left
                        right: parent.right
                        margins: units.gu(3)
                    }
                    height: root.parent.height - lbTitle.height - popoverFooter.height - units.gu(14)

                    Column{
                        id: contentCol
                        width: scrollView.width
                        spacing: units.gu(1)

                        ManualText{
                            text: "<b>" + i18n.tr("Welcome to Time Tracker!") + "</b>"
                        }
                        ManualText{
                            text: i18n.tr("This app is designed to easily track the time spend on various topics.")
                        }

                        ManualCaption{
                            title: i18n.tr("Sessions")
                        }
                        ManualText{
                            text: i18n.tr("The basic unit of time spend on something are sessions. Swiping from the bottom edge opens the Live Session panel. On this page you can easily start, pause, continue and stop a session. "+
                                          "If you missed to track a session in real time, in the top left menu, there is also a feature to add a session manually.")
                        }

                        ManualCaption{
                            title: i18n.tr("Projects")
                        }
                        ManualText{
                            text: i18n.tr("Each session is linked to a project. Projects themself are grouped into topics, that allows to better organise your projects. "+
                                          "For instance, if you like to measure the time spend on reading books, you can define a topic 'Books' and create a project for each book you read. "+
                                          "This way, you can easily measure both the time spend per book as well as for reading books in general.")
                                + "<br><br>"
                                + i18n.tr("To keep the list of topics and projects clear, both of them can be set to inactive in order to hide them temporarily or permanently. "+
                                          "When you finished a book for instance, you won't need to add new sessions to this project anymore so you can set it to inactive.")
                        }

                        ManualCaption{
                            title: i18n.tr("Data export")
                        }
                        ManualText{
                            text: i18n.tr("The focus of this app is to ease the tracking of your time usage. It provides only basic analysis features. "+
                                          "If you like to perform more advanced analysis of your data, you can simply export your data from Time Tracker as a CSV file to pass them "+
                                          "into your preferred data analysis tool.")
                        }

                        ManualCaption{
                            title: i18n.tr("Source Code & Feedback")
                        }
                        ManualText{
                            text: i18n.tr("The source code of this app is available on %1.").arg("<a href=\"https://gitlab.com/matdahl/time-tracker\">GitLab</a>")
                            onLinkActivated: Qt.openUrlExternally("https://gitlab.com/matdahl/time-tracker")
                        }
                        ManualText{
                            text: i18n.tr("If you have any feedback, found a bug or have an idea for new features, feel free to open an issue on %1.").arg("<a href=\"https://gitlab.com/matdahl/time-tracker/-/issues\">GitLab</a>")
                            onLinkActivated: Qt.openUrlExternally("https://gitlab.com/matdahl/time-tracker/-/issues")
                        }
                    }
                }


            /* ---- footer part of the manual ---- */
            Item{
                id: popoverFooter
                anchors{
                    top: scrollView.bottom
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                height: units.gu(4)

                CheckBox{
                    id: cbShowOnStart
                    anchors{
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                    }
                    Component.onCompleted: checked = settings.showManualOnStart
                    onCheckedChanged: settings.showManualOnStart = checked
                }

                Label{
                    anchors{
                        verticalCenter: parent.verticalCenter
                        left: cbShowOnStart.right
                        right: btOK.left
                        leftMargin: units.gu(1)
                        rightMargin: units.gu(2)
                    }
                    text: i18n.tr("Show on start")
                }

                Button{
                    id: btOK
                    anchors{
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                    }
                    color: UbuntuColors.orange
                    text: i18n.tr("OK")
                    onClicked: PopupUtils.close(popover)
                }
            }

            // insert empty item to ensure there is a bottom margin in the popover
            Item{
                anchors.top: popoverFooter.bottom
                width: scrollView.width
                height: units.gu(4)
            }
        }
    }
}
