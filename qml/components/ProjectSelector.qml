/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import TTCore 1.0

Item {
    id: root

    width: parent.width
    height: childrenRect.height

    property int selectedTopic:   0
    property int selectedProject: 0

    property real containerHeight: 3.5
    readonly property int colSpacing: units.gu(2)
    readonly property int colWidth: (width-colSpacing)/2

    onSelectedTopicChanged: {
        Backend.projects.setSelectorModelTopic(selectedTopic)
    }

    Component.onCompleted: {
        topicSelect.selectedIndex   = Backend.topics.activeModel.indexOf(root.selectedTopic)
        projectSelect.selectedIndex = Backend.projects.selectorModel.indexOf(root.selectedProject)
    }

    function checkValues(){
        if (Backend.topics.activeModel.indexOf(selectedTopic) < 0){
            selectedTopic   = 0
            selectedProject = 0
        }
        if (Backend.projects.selectorModel.indexOf(selectedProject) < 0){
            selectedProject = 0
        }
    }


    Column{
        spacing: units.gu(1)
        width: root.width

        Row{
            id: titleRow
            spacing: root.colSpacing
            height: units.gu(2)
            Label{
                width: root.colWidth
                horizontalAlignment: Label.AlignHCenter
                text: i18n.tr("Topic")
            }
            Label{
                width: root.colWidth
                horizontalAlignment: Label.AlignHCenter
                text: i18n.tr("Project")
            }
        }

        Row{
            id: selectorRow
            spacing: root.colSpacing
            height: root.containerHeight*topicSelect.itemHeight

            OptionSelector{
                id: topicSelect
                anchors.verticalCenter: selectorRow.verticalCenter
                width: root.colWidth
                expanded: true
                height: (model.count>root.containerHeight ? root.containerHeight : model.count)*itemHeight
                model: Backend.topics.activeModel
                delegate: OptionSelectorDelegate{
                    onClicked: {
                        if (root.selectedTopic === topicUid)
                            return

                        root.selectedTopic = topicUid

                        // select first project of topic, if projectSelect was already initialised
                        if (projectSelect.selectedIndex > -1)
                            projectSelect.selectedIndex = 0
                    }
                    Rectangle{
                        width: units.gu(1.5)
                        height: parent.height
                        color: topicColor
                    }
                    text: topicTitle
                }
            }

            OptionSelector{
                id: projectSelect
                anchors.verticalCenter: selectorRow.verticalCenter
                width: root.colWidth
                expanded: true
                height: (model.count>root.containerHeight ? root.containerHeight : model.count)*itemHeight
                selectedIndex: -1
                model: Backend.projects.selectorModel
                delegate: OptionSelectorDelegate{
                    onSelectedChanged: {
                        if (selected)
                            root.selectedProject = projectUid
                    }
                    Rectangle{
                        width: units.gu(1.5)
                        height: parent.height
                        color: projectColor
                    }
                    text: projectTitle
                }
            }
        }
    }
}
