/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

Button{
    id: root

    // this must be linked to a SessionIntervalModel that provides a "startDate" property
    property var model

    text: model.startDate.toLocaleDateString()
    onClicked: PopupUtils.open(popoverComponent)

    Component{
        id: popoverComponent
        Popover{
            id: popover
            contentHeight: picker.height + 2*picker.y
            DatePicker{
                id: picker
                y: units.gu(2)
                mode: "Days|Months|Years"
                date: root.model.startDate
                onDateChanged: root.model.startDate = date
                minimum: {
                    var d = Date.prototype.midnight.call(new Date())
                    d.setFullYear(d.getFullYear() - 25);
                    return d
                }
                maximum: {
                    var d = Date.prototype.midnight.call(new Date())
                    d.setFullYear(d.getFullYear() + 25);
                    return d
                }
            }
        }
    }
}
