/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.ListItems 1.3

import "listitems"
import "../dialogs"

import TTCore 1.0

Item {
    id: root

    readonly property int project: Backend.sessions.selected.project

    function open(uid){
        Backend.sessions.selectSession(uid)
        PopupUtils.open(popopverComponent)
    }

    Component{
        id: popopverComponent

        Popover{
            id: popover

            Column{
                id: col
                width: popover.contentWidth
                padding: units.gu(2)
                spacing: units.gu(2)

                Label{
                    text: i18n.tr("Session details")
                    font.bold: true
                    textSize: Label.Large
                }

                Rectangle{
                    id: projectItem
                    width: col.width - 2*col.padding
                    height: units.gu(7)
                    radius: units.gu(1)
                    color: theme.palette.normal.base

                    ListItemLayout{
                        id: projectDetails
                        padding{
                            top: units.gu(1.5)
                            leading: units.gu(1)
                        }

                        Rectangle{
                            SlotsLayout.position: SlotsLayout.First
                            height: units.gu(4)
                            width:  units.gu(4)
                            radius: units.gu(1)
                            color:  Backend.projects.getColor(root.project)
                        }

                        title.text: Backend.projects.getTopicTitle(root.project) + "  -  " + Backend.projects.getTitle(root.project)
                        subtitle.text: Backend.sessions.selected.description

                        Icon{
                            SlotsLayout.position: SlotsLayout.Last
                            height: units.gu(3)
                            name: "edit"
                            MouseArea{
                                anchors{
                                    fill: parent
                                    margins: -units.gu(1)
                                }
                                onClicked: projectEditDialog.open()
                            }
                        }
                    }
                }

                Item{
                    id: startDateItem
                    height: units.gu(3)
                    width: col.width - 2*col.padding

                    Icon{
                        id: icStartDate
                        anchors.left: parent.left
                        name: "calendar"
                        height: units.gu(3)
                    }

                    Label{
                        anchors{
                            verticalCenter: icStartDate.verticalCenter
                            left: icStartDate.right
                            right: mouseEditStartDate.left
                            margins: units.gu(2)
                        }
                        text: Backend.sessions.selected.startDate.toLocaleDateString()
                    }

                    MouseArea{
                        id: mouseEditStartDate
                        anchors.right: parent.right
                        height: units.gu(3)
                        width: units.gu(3)
                        Icon{
                            anchors.centerIn: parent
                            height: units.gu(2)
                            name: "edit"
                        }
                        onClicked: startDateEditDialog.open()
                    }
                }

                Column{
                    id: subsessionsCol
                    width: col.width - 2*col.padding

                    Repeater{
                        id: reapSubsessions
                        model: Backend.sessions.selected.intervalModel
                        delegate: SubsessionListItem{
                            animated: false
                            onDayChanged: Backend.sessions.updateSelectedDayStamp(index,start,newDay)
                            onHourChanged: Backend.sessions.updateSelectedHourStamp(index,start,newHour)
                            onMinuteChanged: Backend.sessions.updateSelectedMinuteStamp(index,start,newMinute)
                        }
                    }
                }
            }
        }
    }

    SessionProjectEditDialog{
        id: projectEditDialog
    }

    SessionStartDateEditDialog{
        id: startDateEditDialog
    }
}
