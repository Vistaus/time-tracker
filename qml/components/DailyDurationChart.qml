/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12

Item {
    id: root

    property var model

    property int spacing: units.gu(0.125) > 1 ? units.gu(0.125) : 1
    property int baselineHeight: units.gu(0.125) > 1 ? units.gu(0.125) : 1
    property int barPadding: baselineHeight

    readonly property int numBars: model && model.count > 0 ? model.count : 0
    readonly property int barWidth: (width - (numBars-1)*spacing - 2*barPadding)/numBars
    readonly property int barHeight: height - baselineHeight - 2*spacing

    readonly property int baselineWidth: 2*barPadding + numBars*barWidth + (numBars-1)*spacing

    property color color: "red"

    property real maxValue: 1

    Repeater{
        id: repBars
        model: root.model
        delegate: Item{
            id: barDelegate
            x: barPadding + index*(barWidth+spacing)
            y: root.spacing
            height: root.barHeight
            width: root.barWidth

            readonly property real value: modelValue
            onValueChanged: {
                if (modelValue > root.maxValue)
                    root.maxValue = modelValue
            }

            Rectangle{
                id: rectWeekSeparator
                anchors{
                    top: parent.top
                    bottom: parent.bottom
                    margins: -root.spacing
                    horizontalCenter: parent.left
                }
                width: root.baselineHeight
                color: theme.palette.normal.base
                visible: modelDate.getDay() === 1 && index > 0
            }

            Rectangle{
                id: rectBar
                anchors.bottom: parent.bottom
                width: parent.width
                height: (barDelegate.value/root.maxValue)*root.barHeight
                color: root.color
                radius: units.gu(0.25)
            }
        }
    }

    Rectangle{
        id: baseline
        anchors{
            left: parent.left
            bottom: parent.bottom
        }
        height: root.baselineHeight
        width:  root.baselineWidth
        radius: height/2
        color: theme.palette.normal.base
    }
}
