/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import "listitems"
Item {
    id: root
    width: parent.width
    height: childrenRect.height

    Column{
        id: col
        width: root.width

        Item{
            id: header
            width: root.width
            height: units.gu(5)

            Rectangle{
                anchors.fill: parent
                color: theme.palette.normal.base
                opacity: 0.2
            }

            Rectangle{
                anchors{
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                height: units.gu(0.125)
                color: theme.palette.normal.base
            }

            Label{
                id: lbDate
                anchors{
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: units.gu(2)
                }

                text: dsmDate.getTime() === (new Date()).setHours(0,0,0,0)   ? i18n.tr("Today")
                    : dsmDate.getTime() === (new Date()).setHours(-24,0,0,0) ? i18n.tr("Yesterday")
                                                                             : dsmDate.toLocaleDateString()
            }
        }

        Repeater{
            model: dsmModel
            delegate: DailySessionsListItem{}
        }
    }
}
