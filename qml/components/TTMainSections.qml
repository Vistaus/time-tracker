/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root

    width: parent.width
    height: units.gu(5)

    property alias selectedIndex: internal.selectedIndex

    readonly property int numberOfSections: internal.titles.length
    readonly property int sectionWidth: width/numberOfSections

    Item{
        id: internal
        property int selectedIndex: 0
        property var titles: [
            i18n.tr("Calendar"),
            i18n.tr("Sessions"),
            i18n.tr("Projects")
        ]
    }

    Rectangle{
        id: background
        anchors.fill: parent
        color: colors.currentHeader
        opacity: 0.1
    }

    Row{
        height: root.height
        Repeater{
            id: reap
            model: internal.titles
            delegate: Item{
                height: root.height
                width:  root.sectionWidth
                Rectangle{
                    anchors.fill: parent
                    visible: index === root.selectedIndex
                    color: theme.palette.normal.base
                    opacity: 0.2
                }

                Label{
                    id: lbTitle
                    anchors.centerIn: parent
                    text: modelData
                    font.bold: true
                    color: index === root.selectedIndex ? theme.palette.selected.selection
                                                        : theme.palette.normal.base
                }
                Rectangle{
                    anchors{
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    height: units.gu(0.25)
                    color: lbTitle.color
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: internal.selectedIndex = index
                }
            }
        }
    }
}
