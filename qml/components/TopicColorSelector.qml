/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: root
    width: parent.width
    height: units.gu(5)

    property int selectedIndex: -1

    ScrollView{
        id: scroll
        anchors.fill: parent

        Row{
            height: scroll.height
            spacing: units.gu(1)
            Repeater{
                model: 12
                delegate: Button{
                    height: scroll.height
                    width: height
                    color: root.selectedIndex === index ? theme.palette.normal.selection : theme.palette.normal.base
                    Rectangle{
                        anchors{
                            fill: parent
                            margins: units.gu(0.5)
                        }
                        radius: units.gu(0.5)
                        color: Backend.topics.hueToColor(index)
                    }
                    onClicked: root.selectedIndex = index
                }
            }
        }
    }
}
