/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Item{
    id: itemZoom

    Button{
        id: btZoom
        anchors{
            fill: parent
            leftMargin: units.gu(0.5)
            rightMargin: units.gu(0.5)
        }
        onClicked: PopupUtils.open(zoomPopoverComponent,this)

        Icon{
            anchors.centerIn: parent
            height: parent.height - units.gu(2)
            name: "find"
            color: theme.palette.normal.baseText
        }

        Component{
            id: zoomPopoverComponent
            Popover{
                id: zoomPopover
                contentWidth: zoomRow.width
                contentHeight: zoomRow.height + 2*zoomRow.padding

                Row{
                    id: zoomRow
                    padding: units.gu(1)
                    spacing: units.gu(2)
                    height: zoomSlider.height

                    Icon{
                        id: zoomIconOut
                        anchors.verticalCenter: zoomSlider.verticalCenter
                        height: units.gu(3)
                        name: "zoom-out"
                    }

                    Slider{
                        id: zoomSlider
                        width: units.gu(20)
                        minimumValue: 0
                        maximumValue: 1
                        value: calendarView.zoom
                        live: true
                        onValueChanged: {
                            const centerTime = (calFlickable.contentY + 0.5*calFlickable.height) / calendarView.pxlPerDay
                            calendarView.zoom = value
                            calFlickable.setTimeAtY(centerTime,0.5)
                            calFlickable.returnToBounds()
                        }
                    }

                    Icon{
                        id: zoomIconIn
                        anchors.verticalCenter: zoomSlider.verticalCenter
                        height: units.gu(3)
                        name: "zoom-in"
                    }
                }
            }
        }

    }
}
