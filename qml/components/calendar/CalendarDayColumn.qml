/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: root
    width: calendarView.columnWidth
    height: 24*calendarView.pxlPerHour

    property date date: calEntryDate

    readonly property real pxlPerSec: height / 86400.
    property int selectionWidth: units.gu(1)

    Item{
        id: background
        anchors.fill: parent

        Rectangle{
            anchors.fill: parent
            color: theme.palette.normal.base
            opacity: 0.2
            visible: date.getDay() === 0 || date.getDay() === 6
        }

        Rectangle{
            anchors{
                top: parent.top
                left: parent.left
                bottom: parent.bottom
            }
            width: calendarView.dividerWidth
            color: theme.palette.normal.base
        }

        Rectangle{
            anchors{
                top: parent.top
                right: parent.right
                bottom: parent.bottom
            }
            width: calendarView.dividerWidth
            visible: index === Backend.calendar.displayedDays-1
            color: theme.palette.normal.base
        }
    }

    Item{
        id: subsessions
        anchors.fill: parent

        Repeater{
            id: repeaterSubsessions
            model: calEntryModel
            delegate: Rectangle{
                id: subsessionItem
                anchors{
                    left: parent.left
                    right: parent.right
                    margins: units.gu(0.125)
                }
                radius: Math.min(width/3,units.gu(1))
                y: entryStartSecs * root.pxlPerSec
                height: (entryEndSecs-entryStartSecs)*root.pxlPerSec
                color: Backend.projects.getColor(entryProject)

                readonly property bool isSelected: entrySession === calendarView.selectedSession

                Rectangle{
                    anchors.fill: parent
                    radius: parent.radius
                    color: theme.palette.normal.selection
                    opacity: 0.2
                    visible: subsessionItem.isSelected
                }
                Rectangle{
                    anchors{
                        fill: parent
                        margins: -root.selectionWidth/2
                    }
                    radius: parent.radius - anchors.margins
                    color: "#00000000"
                    visible: subsessionItem.isSelected
                    border{
                        color: theme.palette.normal.selection
                        width: root.selectionWidth
                    }
                }
            }
        }
    }
}
