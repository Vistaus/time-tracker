/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: root
    height: parent.height
    width: calendarView.columnWidth

    property date date: calEntryDate

    states: [
        State{
            name: "weekly"
            when: Backend.calendar.mode === CalendarViewManager.WEEKLY
        },
        State{
            name: "monthly"
            when: Backend.calendar.mode === CalendarViewManager.MONTHLY
        }
    ]

    Item{
        id: background
        anchors.fill: parent

        Rectangle{
            anchors.fill: parent
            color: theme.palette.normal.base
            opacity: 0.3
        }

        Rectangle{
            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: calendarView.dividerWidth
            color: theme.palette.normal.base
        }

        Rectangle{
            anchors{
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            height: calendarView.dividerWidth
            color: theme.palette.normal.base
        }

        Rectangle{
            anchors{
                top: parent.top
                bottom: parent.bottom
                left: parent.left
            }
            visible: root.state === "weekly" || index % 3 === 0
            width: calendarView.dividerWidth
            color: theme.palette.normal.base
        }

        Rectangle{
            anchors{
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }
            visible: index === Backend.calendar.displayedDays-1
            width: calendarView.dividerWidth
            color: theme.palette.normal.base
        }
    }

    Item{
        id: labelsWeekly
        anchors.fill: parent
        visible: root.state === "weekly"

        Label{
            anchors{
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                bottom: parent.verticalCenter
            }
            verticalAlignment: Label.AlignVCenter
            text: Qt.formatDate(root.date,"ddd")
            textSize: Label.Small
        }

        Label{
            anchors{
                horizontalCenter: parent.horizontalCenter
                top: parent.verticalCenter
                bottom: parent.bottom
            }
            verticalAlignment: Label.AlignVCenter
            text: Qt.formatDate(root.date,"d MMM")
            textSize: Label.Small
        }
    }

    Item{
        id: labelsMonthly
        anchors.fill: parent
        visible: root.state === "monthly"

        Label{
            id: lbMonthly
            anchors.centerIn: parent
            visible: index % 3 === 1
            text: Qt.formatDate(root.date,"d")
        }
        Rectangle{
            anchors{
                horizontalCenter: parent.horizontalCenter
                top: lbMonthly.bottom
                topMargin: units.gu(0.25)
                bottom: parent.bottom
            }
            width: calendarView.dividerWidth
            color: theme.palette.normal.base
            visible: lbMonthly.visible
        }
    }
}
