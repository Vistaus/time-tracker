/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: root
    width: parent.width
    height: contentHeight + 2*verticalPadding

    property int verticalPadding: units.gu(1)
    property int horizontalPadding: units.gu(1)

    property int contentHeight: units.gu(5)

    Button{
        id: btBack
        anchors{
            verticalCenter: parent.verticalCenter
            left: parent.left
            margins: root.horizontalPadding
        }
        height: root.contentHeight
        width: height
        Icon{
            anchors.centerIn: parent
            height: parent.height - units.gu(3)
            name: "back"
            color: theme.palette.normal.baseText
        }
        onClicked: Backend.calendar.showPreviousPage()
    }

    Column{
        id: timeRangeItem
        anchors{
            verticalCenter: parent.verticalCenter
            left: btBack.right
            right: icToday.left
            margins: units.gu(1)
        }
        Label{
            id: lbTimeScope
            width: parent.width
            height: units.gu(3)
            horizontalAlignment: Label.AlignHCenter
            verticalAlignment:   Label.AlignVCenter
            text: (Backend.calendar.mode === CalendarViewManager.WEEKLY)  ? Backend.calendar.firstDay.getFullYear() + ", " + i18n.tr("week") + " " + Backend.calendar.firstDay.getWeek()
                : (Backend.calendar.mode === CalendarViewManager.MONTHLY) ? Qt.formatDate(Backend.calendar.firstDay,"MMMM yyyy")
                                                                          : "UNKNOWN CALENDAR MODE"
        }

        Item{
            id: rowTimeBounds
            height: units.gu(2)
            width: parent.width

            Label{
                id: lbFirstDay
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: canvasArrow.left
                    margins: units.gu(1)
                }
                textSize: Label.Small
                color: theme.palette.normal.base
                text: Qt.formatDate(Backend.calendar.firstDay,"d MMM")
            }

            Canvas{
                id: canvasArrow
                anchors.centerIn: parent
                height: units.gu(1)
                width: units.gu(2)

                property int lineWidth: units.gu(0.25)
                property int headWidth: units.gu(0.75)

                readonly property int barTop:    (height - lineWidth)/2
                readonly property int barBottom: (height + lineWidth)/2

                onPaint: {
                    var context = getContext("2d")

                    context.fillStyle = theme.palette.normal.base

                    // draw bar
                    context.beginPath();
                    context.moveTo(0, barTop);
                    context.lineTo(width-headWidth, barTop);
                    context.lineTo(width-headWidth, barBottom);
                    context.lineTo(0, barBottom);
                    context.closePath();
                    context.fill()

                    // draw head
                    context.beginPath();
                    context.moveTo(width-headWidth, 0);
                    context.lineTo(width, height/2);
                    context.lineTo(width-headWidth, height);
                    context.closePath();
                    context.fill()
                }
            }

            Label{
                id: lbLastDay
                anchors{
                    verticalCenter: parent.verticalCenter
                    left: canvasArrow.right
                    margins: units.gu(1)
                }
                textSize: Label.Small
                color: theme.palette.normal.base
                text: Qt.formatDate(new Date(Backend.calendar.firstDay.setDate(Backend.calendar.firstDay.getDate()+Backend.calendar.displayedDays-1)),"d MMM")
            }
        }
    }


    Icon{
        id: icToday
        anchors{
            verticalCenter: parent.verticalCenter
            right: btNext.left
            margins: units.gu(2)
        }
        height: root.contentHeight - units.gu(2)
        name: "calendar-today"
        MouseArea{
            anchors{
                fill: parent
                margins: - units.gu(1)
            }
            onClicked: Backend.calendar.showToday()
        }
    }

    Button{
        id: btNext
        anchors{
            verticalCenter: parent.verticalCenter
            right: parent.right
            margins: root.horizontalPadding
        }
        height: root.contentHeight
        width: height
        Icon{
            anchors.centerIn: parent
            height: parent.height - units.gu(3)
            name: "next"
            color: theme.palette.normal.baseText
        }
        onClicked: Backend.calendar.showNextPage()
    }
}
