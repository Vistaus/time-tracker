/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import ".."
import "../../dialogs"

import TTCore 1.0

Item {
    id: root
    width: parent.width
    height: bottomEdge.hint.height + content.height + 2*content.anchors.margins

    Item{
        id: content
        anchors{
            top: parent.top
            left: parent.left
            right: parent.right
            margins: units.gu(1)
        }
        height: units.gu(7)

        Icon{
            id: icDelete
            anchors{
                verticalCenter: parent.verticalCenter
                left: parent.left
                right: sessionDetailsItem.left
                rightMargin: content.anchors.margins
            }
            name: "delete"
            color: theme.palette.normal.negative
            visible: calendarView.selectedSession > 0
            MouseArea{
                anchors{
                    fill: icDelete
                    margins: -units.gu(1)
                }
                onClicked: deleteDialog.open(calendarView.selectedSession)
            }
        }

        Rectangle{
            id: sessionDetailsItem
            anchors{
                fill: parent
                leftMargin: calendarView.timeColumnWidth - content.anchors.margins
            }
            radius: units.gu(1)
            color: theme.palette.normal.base
            visible: calendarView.selectedSession > 0

            ListItemLayout{
                id: sessionDetailsItemContent
                padding{
                    top: units.gu(1.5)
                    leading: units.gu(1)
                }

                Rectangle{
                    SlotsLayout.position: SlotsLayout.First
                    height: units.gu(4)
                    width:  units.gu(4)
                    radius: units.gu(1)
                    color:  Backend.projects.getColor(Backend.sessions.selected.project)
                }

                title.text: Backend.projects.getTopicTitle(Backend.sessions.selected.project) + "  -  " + Backend.projects.getTitle(Backend.sessions.selected.project)
                subtitle.text: Backend.sessions.selected.description

                Column{
                    SlotsLayout.position: SlotsLayout.Trailing
                    width: units.gu(3)
                    spacing: units.gu(0.5)

                    Icon{
                        id: icDuration
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: units.gu(2)
                        name: "stopwatch"
                    }

                    Label{
                        id: lbDuration
                        anchors.horizontalCenter: parent.horizontalCenter
                        readonly property int hours: Math.floor(Backend.sessions.selected.duration/3600)
                        readonly property int mins:  Math.floor(Backend.sessions.selected.duration/60) - 60*hours
                        text: hours + (mins<10 ? ":0" : ":") + mins
                    }
                }

                Icon{
                    SlotsLayout.position: SlotsLayout.Last
                    height: units.gu(3)
                    name: "edit"
                    MouseArea{
                        anchors{
                            fill: parent
                            margins: -units.gu(1)
                        }
                        onClicked: sessionDetailPopover.open(calendarView.selectedSession)
                    }
                }
            }
        }

        Label{
            anchors.centerIn: parent
            text: i18n.tr("No session selected")
            visible: calendarView.selectedSession === 0
        }
    }

    SessionDetailPopover{
        id: sessionDetailPopover
    }

    SessionDeleteDialog{
        id: deleteDialog
        onDidDelete: calendarView.selectedSession = 0
    }
}
