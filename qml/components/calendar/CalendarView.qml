/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Qt.labs.settings 1.1
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: calendarView
    width: parent.width

    Settings{
        category: "CalendarView"
        property alias zoom: calendarView.zoom
        property alias flickableY: calFlickable.contentY
    }

    property int selectedSession: 0
    onSelectedSessionChanged: Backend.sessions.selectSession(selectedSession)

    property real minPxlPerHour: calFlickable.height/24
    property real maxPxlPerHour: units.gu(8)

    property real zoom: 1.0
    onZoomChanged: {
        if (zoom<0)
            zoom = 0
        else if (zoom>1)
            zoom = 1
    }

    readonly property real pxlPerHour: minPxlPerHour + zoom * (maxPxlPerHour-minPxlPerHour)
    readonly property real pxlPerDay: 24*pxlPerHour

    property int timeColumnWidth: units.gu(4)
    property int headerRowHeight: units.gu(4)
    property int dividerWidth: units.gu(0.125)

    readonly property real columnWidth: (width - timeColumnWidth) / Backend.calendar.displayedDays

    Item{
        id: headerRow
        width: calendarView.width
        height: calendarView.headerRowHeight

        CalendarZoomButton{
            id: zoomButton
            anchors{
                top: parent.top
                left: parent.left
                right: headerCellsRow.left
                bottom: parent.bottom
            }
        }

        Row{
            id: headerCellsRow
            x: calendarView.timeColumnWidth
            height: parent.height

            Repeater{
                id: repeaterHeaderCells
                model: Backend.calendar.model
                delegate: CalendarDayHeaderCell{}
            }
        }
    }

    Flickable{
        id: calFlickable
        anchors{
            top: headerRow.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: calContent.height
        contentWidth:  width
        clip: true

        function setTimeAtY(time,yRel){
            if (time < 0)
                time = 0
            else if (time > 1)
                time = 1

            contentY = calendarView.pxlPerDay * time - yRel*calFlickable.height
        }

        Item{
            id: calContent
            width: calFlickable.width
            height: calendarView.pxlPerDay

            Item{
                id: timeLabelColumn
                width: calendarView.timeColumnWidth
                height: calContent.height

                Repeater{
                    id: reapTimeLabels
                    model: 23
                    delegate: Label{
                        anchors{
                            right: parent.right
                            margins: units.gu(0.5)
                        }
                        y: (modelData+1) * calendarView.pxlPerHour - height/2
                        textSize: Label.Small
                        text: (modelData+1) + ":00"
                        onWidthChanged: {
                            const w = width + 2*anchors.margins
                            if (w > calendarView.timeColumnWidth)
                                calendarView.timeColumnWidth = w
                        }
                    }
                }
            }

            Row{
                id: daysRow
                anchors.left: timeLabelColumn.right
                height: calContent.height

                Repeater{
                    id: repeaterDayColumns
                    model: Backend.calendar.model
                    delegate: CalendarDayColumn{}
                }
            }

            Item{
                id: hourSeparators
                anchors.fill: parent
                Repeater{
                    model: 49
                    delegate: Rectangle{
                        anchors{
                            left: parent.left
                            leftMargin: calendarView.timeColumnWidth
                            right: parent.right
                        }
                        y: modelData * calendarView.pxlPerHour/2
                        readonly property bool hightlighted: modelData > 0 && modelData < 48 && modelData % 12 === 0
                        height: (hightlighted ? 2 : 1) * calendarView.dividerWidth
                        color: theme.palette.normal.base
                        opacity: modelData % 2 === 0 ? 1 : 0.5
                    }
                }
            }
        }

        PinchArea{
            id: pinchArea
            anchors.fill: parent
            pinch.dragAxis: Pinch.NoDrag

            property real zoomMax: 4
            property real zoomMin: 0.1

            property real baseZoom: 0
            property real fixedTime: 0
            property real fixedTimeYRel: 0

            onPinchStarted: {
                baseZoom      = calendarView.zoom
                fixedTimeYRel = pinch.center.y/height
                fixedTime     = (calFlickable.contentY + fixedTimeYRel*calFlickable.height) / calendarView.pxlPerDay
            }
            onPinchUpdated:  {
                if (Math.abs(pinch.angle) < 45 || Math.abs(pinch.angle) > 135)
                    return

                const yPinch = pinch.scale * Math.abs(Math.sin(pinch.angle*Math.PI/180))
                // apply zoom
                if (yPinch > 1)
                    calendarView.zoom = baseZoom + (yPinch-1) / zoomMax
                else
                    calendarView.zoom = baseZoom + ((yPinch-zoomMin)/(1-zoomMin) - 1)
                // ensure that the zoom center stays in position
                calFlickable.setTimeAtY(fixedTime,fixedTimeYRel)
            }
            onPinchFinished: calFlickable.returnToBounds()

            MouseArea{
                id: calendarMouse
                anchors.fill: parent
                onClicked: {
                    const day = Math.floor((mouse.x - calendarView.timeColumnWidth) / calendarView.columnWidth)
                    const time = mouse.y/height // in units of days
                    const sessionId = Backend.calendar.model.getSessionIdAt(day,time)
                    calendarView.selectedSession = (sessionId === calendarView.selectedSession ? 0 : sessionId)
                }
            }
        }
    }

    Rectangle{
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            leftMargin: calendarView.timeColumnWidth
        }
        height: calendarView.dividerWidth
        color: theme.palette.normal.base
    }
}
