/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

Item {
    id: root

    property int day
    property int hour
    property int minute

    property int minDay
    property int minHour
    property int minMinute

    property int maxDay
    property int maxHour
    property int maxMinute

    signal dayWasChanged(int newDay)
    signal hourWasChanged(int newHour)
    signal minuteWasChanged(int newMinute)

    QtObject{
        id: internal
        readonly property int day:    root.day
        readonly property int hour:   24*root.day + root.hour
        readonly property int minute: root.minute

        property int minHour: -24
        property int maxHour: 23
        property var hoursModel: []
    }

    function open(caller){
        // define bounds, that are fixed for the live time of the popover
        internal.minHour = 24*root.minDay + root.minHour
        internal.maxHour = 24*root.maxDay + root.maxHour
        internal.hoursModel = []
        for (var h=internal.minHour; h<=internal.maxHour; h++)
            internal.hoursModel.push(h)

        PopupUtils.open(popoverComponent,caller)
    }

    Component{
        id: popoverComponent
        Popover{
            id: popover
            contentWidth: content.width + units.gu(4)

            function updateHour(newHour){
                var newDay  = 0
                while (newHour<0){
                    newDay  -= 1
                    newHour += 24
                }
                while (newHour>23){
                    newDay  += 1
                    newHour -= 24
                }

                if (newDay !== internal.day)
                    root.dayWasChanged(newDay)

                root.hourWasChanged(newHour)

                validateMinute()
            }

            function updateMinute(newMinute){
                root.minuteWasChanged(newMinute)
                validateMinute()
            }

            function validateMinute(){
                if (internal.hour === internal.minHour){
                    if (internal.minute < root.minMinute){
                        root.minuteWasChanged(root.minMinute)
                        minutesPicker.selectedIndex = root.minMinute
                    }
                } else if (internal.hour === internal.maxHour){
                    if (internal.minute > root.maxMinute){
                        root.minuteWasChanged(root.maxMinute)
                        minutesPicker.selectedIndex = root.maxMinute
                    }
                }
            }

            Row{
                 id: content
                 x: units.gu(2)
                 y: units.gu(2)
                 height: childrenRect.height + units.gu(4)
                 anchors.horizontalCenter: parent.horizontalCenter
                 Picker{
                     id: hoursPicker
                     width: units.gu(10)
                     model: internal.hoursModel
                     circular: false
                     selectedIndex: internal.hour - internal.minHour
                     onSelectedIndexChanged: popover.updateHour(selectedIndex + internal.minHour)
                     delegate: PickerDelegate{
                         id: hoursDelegate
                         readonly property int dayOffset: modelData>=0 ? modelData/24 : -1 + (modelData+1)/24
                         Rectangle{
                             id: rectDayOffset
                             anchors{
                                 verticalCenter: parent.verticalCenter
                                 left: parent.left
                                 margins: units.gu(1)
                             }
                             height: units.gu(2)
                             width: lbDayOffset.width + units.gu(1)
                             radius: units.gu(0.5)
                             color: UbuntuColors.orange
                             visible: dayOffset !== 0

                             Label{
                                 id: lbDayOffset
                                 anchors.centerIn: parent
                                 textSize: Label.Small
                                 text: (dayOffset>0 ? "+" : "-") + dayOffset
                             }
                         }

                         Label{
                             anchors{
                                 verticalCenter: parent.verticalCenter
                                 right: parent.right
                             }
                             width: units.gu(6)
                             horizontalAlignment: Label.AlignHCenter
                             text: modelData - 24*dayOffset
                         }
                     }
                 }
                 Picker{
                     id: minutesPicker
                     width: units.gu(6)

                     model: 60
                     //selectedIndex: internal.minute
                     Component.onCompleted: selectedIndex = internal.minute
                     onSelectedIndexChanged: popover.updateMinute(selectedIndex)

                     delegate: PickerDelegate{
                         Label{
                             anchors.centerIn: parent
                             text: (modelData < 10 ? "0":"") + modelData
                         }
                     }
                 }
             }
        }
    }
}
