/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: root
    anchors{
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        margins: units.gu(2)
    }
    property int buttonHeight: units.gu(7)
    property int topPadding:   units.gu(1)
    property int spacing:      units.gu(2)
    height: buttonHeight + topPadding

    readonly property int buttonWidth: (width - 2*spacing)/3

    states: [
        State{
            name: "session"
            when: Backend.sessions.hasLiveSession
        },
        State{
            name: "nosession"
            when: !Backend.sessions.hasLiveSession
        }
    ]

    Item{
        id: sessionContent
        anchors{
            fill: parent
            topMargin: root.topPadding
        }
        visible: root.state == "session"

        Row{
            height: parent.height
            spacing: root.spacing

            Button{
                id: btPause
                height: root.buttonHeight
                width:  root.buttonWidth
                readonly property bool showPause: Backend.sessions.liveSessionIsRunning
                color: showPause ? theme.palette.normal.base
                                 : theme.palette.normal.positive
                onClicked:{
                    if (showPause)
                        Backend.sessions.pauseLiveSession()
                    else
                        Backend.sessions.continueLiveSession()
                }

                Icon{
                    anchors.centerIn: parent
                    height: parent.height - units.gu(2)
                    name: btPause.showPause ? "media-playback-pause"
                                            : "media-playback-start"
                    color: btPause.showPause ? theme.palette.normal.baseText
                                             : theme.palette.normal.positiveText
                }
            }

            Button{
                id: btDiscard
                height: root.buttonHeight
                width:  root.buttonWidth
                color:  theme.palette.normal.negative
                onClicked: Backend.sessions.discardLiveSession()

                Icon{
                    anchors.centerIn: parent
                    height: parent.height - units.gu(2)
                    name:  "delete"
                    color: theme.palette.normal.negativeText
                }
            }

            Button{
                id: btStop
                height: root.buttonHeight
                width:  root.buttonWidth
                enabled: inputProject.selectedProject > 0
                color: UbuntuColors.orange
                onClicked: {
                    Backend.sessions.finishLiveSession(inputProject.selectedProject,inputDescription.text)
                    inputDescription.text = ""
                }

                Icon{
                    anchors.centerIn: parent
                    height: parent.height - units.gu(2)
                    name: "media-playback-stop"
                    color: theme.palette.normal.negativeText
                }

            }
        }
    }

    Item{
        id: noSessionContent
        anchors{
            fill: parent
            topMargin: root.topPadding
        }
        visible: root.state == "nosession"

        Button{
            id: btStart
            anchors.fill: parent
            color: theme.palette.normal.positive
            Icon{
                anchors.centerIn: parent
                height: parent.height - units.gu(2)
                name:  "media-playback-start"
                color: theme.palette.normal.positiveText
            }
            onClicked: Backend.sessions.startLiveSession()
        }
    }

}
