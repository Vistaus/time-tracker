/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

Button {
    id: root
    property bool start: true

    signal dayChanged(int newDay)
    signal hourChanged(int newHour)
    signal minuteChanged(int newMinute)

    anchors.verticalCenter: parent.verticalCenter
    height: parent.height - units.gu(2)
    width:  lbTime.width + units.gu(3)
    enabled: internal.valid

    QtObject{
        id: internal

        property bool valid: root.start ? siStartValid  : siEndValid
        property int day:    root.start ? siStartDay    : siEndDay
        property int hour:   root.start ? siStartHour   : siEndHour
        property int minute: root.start ? siStartMinute : siEndMinute
        readonly property string time: ((hour<10)?"0":"") + hour + ":" + ((minute<10)?"0":"")+minute

        property bool minValid: root.start ? siMinStartValid  : siStartValid
        property int minDay:    minValid ? root.start ? siMinStartDay : siStartDay
                                         : -1
        property int minHour:   minValid ? root.start ? siMinStartHour : siStartHour
                                         : 0
        property int minMinute: minValid ? root.start ? siMinStartMinute : siStartMinute
                                         : 0

        property bool maxValid: root.start ? siEndValid  : siMaxEndValid
        property int maxDay:    maxValid ? root.start ? siEndDay    : siMaxEndDay
                                         : day + 1
        property int maxHour:   maxValid ? root.start ? siEndHour   : siMaxEndHour
                                         : 23
        property int maxMinute: maxValid ? root.start ? siEndMinute : siMaxEndMinute
                                         : 59
    }

    Label{
        id: lbTime
        anchors.centerIn: parent
        text: root.enabled ? internal.time : "   ...   "
    }

    Rectangle{
        id: rectDayOffset

        anchors{
            top: parent.top
            left:  root.start ? parent.left : undefined
            right: root.start ? undefined   : parent.right
            margins: -units.gu(1)
        }
        height: units.gu(2)
        width: lbDayOffset.width + units.gu(1)
        radius: units.gu(0.5)
        color: UbuntuColors.orange
        visible: internal.day !== 0

        Label{
            id: lbDayOffset
            anchors.centerIn: parent
            textSize: Label.Small
            text: (internal.day>0 ? "+": "") + internal.day
        }
    }

    SubsessionIntervalBoundEditPopover{
        id: boundEditPopover
        day:     internal.day
        hour:    internal.hour
        minute:  internal.minute
        minDay:    internal.minDay
        minHour:   internal.minHour
        minMinute: internal.minMinute
        maxDay:    internal.maxDay
        maxHour:   internal.maxHour
        maxMinute: internal.maxMinute

        onDayWasChanged: root.dayChanged(newDay)
        onHourWasChanged: root.hourChanged(newHour)
        onMinuteWasChanged: root.minuteChanged(newMinute)
    }

    onClicked: boundEditPopover.open(root)
}
