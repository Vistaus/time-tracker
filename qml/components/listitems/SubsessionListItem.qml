/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import ".."

Item{
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    implicitHeight: listItem.height + listItem.y
    clip: true

    signal remove()
    signal dayChanged(bool start, int newDay)
    signal hourChanged(bool start, int newHour)
    signal minuteChanged(bool start, int newMinute)

    property bool animated: true

    state: animated ? "initial" : "finished"
    states: [
        State {
            name: "initial"
            PropertyChanges {
                target: root
                height: 0
            }
        },
        State {
            name: "finished"
            PropertyChanges {
                target: root
                height: implicitHeight
            }
        }
    ]
    transitions: Transition {
        from: "initial"
        to: "finished"
        PropertyAnimation{
            duration: 400
            property: "height"
        }
    }
    Component.onCompleted: state = "finished"

    /* -------------------- *
     * ---- Pause Item ---- *
     * -------------------- */
    Item{
        id: pauseItem
        height: visible ? units.gu(3) : 0
        width: parent.width
        visible: index > 0
        property color color: theme.palette.normal.base

        Row{
            anchors.centerIn: parent
            spacing: units.gu(1)
            Icon{
                id: icPauseDuration
                name: "stopwatch"
                height: units.gu(2)
                color: pauseItem.color
            }
            Label{
                anchors.verticalCenter: icPauseDuration.verticalCenter
                readonly property int minsTotal: Math.round(siPause/60.)
                readonly property int hours: minsTotal / 60
                readonly property int mins:  minsTotal % 60
                text: hours + ":" + (mins<10 ? "0":"")+mins
                color: pauseItem.color
            }
        }
    }


    /* ------------------- *
     * ---- List Item ---- *
     * ------------------- */
    ListItem {
        id: listItem
        anchors.top: pauseItem.bottom
        height: units.gu(6)

        leadingActions: ListItemActions{
            actions: [
                Action{
                    iconName: "delete"
                    name: i18n.tr("delete")
                    onTriggered: root.remove()
                }
            ]
        }

        divider.visible: false
        Rectangle{
            anchors.fill: parent
            color: theme.palette.normal.base
            opacity: 0.3
        }

        SubsessionBoundButton{
            id: btStart
            anchors{
                left: parent.left
                margins: units.gu(1)
            }
            start: true
            onDayChanged: root.dayChanged(start,newDay)
            onHourChanged: root.hourChanged(start,newHour)
            onMinuteChanged: root.minuteChanged(start,newMinute)
        }

        Item{
            id: itemMiddle
            anchors{
                top: parent.top
                left: btStart.right
                right: btEnd.left
                bottom: parent.bottom
                margins: units.gu(1)
            }

            readonly property int topMargin: units.gu(1)
            readonly property int leftMargin: units.gu(0)
            readonly property int rightMargin: units.gu(0)
            readonly property int bottomMargin: units.gu(1)
            readonly property int lineWidth: units.gu(0.25)

            readonly property int xMin: leftMargin
            readonly property int xMax: width - rightMargin
            readonly property int yMin: topMargin
            readonly property int yMax: height - bottomMargin
            readonly property int yMiddle: (yMax+yMin)/2
            readonly property int arrowBarTop: yMiddle - lineWidth/2
            readonly property int arrowHeadHeight: (yMax-yMin)
            readonly property int arrowHeadWidth:  2./3 * arrowHeadHeight

            Canvas{
                anchors.fill: parent
                onPaint: {
                    var context = getContext("2d")

                    const xMin        = itemMiddle.xMin
                    const xMax        = itemMiddle.xMax
                    const yMin        = itemMiddle.yMin
                    const yMax        = itemMiddle.yMax
                    const yMiddle     = itemMiddle.yMiddle
                    const lw          = itemMiddle.lineWidth
                    const arrowTop    = itemMiddle.arrowBarTop
                    const arrowHeight = itemMiddle.arrowHeadHeight
                    const arrowWidth  = itemMiddle.arrowHeadWidth

                    context.fillStyle = theme.palette.normal.base

                    // draw left bar
                    context.beginPath();
                    context.moveTo(xMin, yMin);
                    context.lineTo(xMin+lw, yMin);
                    context.lineTo(xMin+lw, yMax);
                    context.lineTo(xMin, yMax);
                    context.closePath();
                    context.fill()

                    // draw arrow bar
                    context.beginPath();
                    context.moveTo(xMin+lw, arrowTop);
                    context.lineTo(xMax-arrowWidth, arrowTop);
                    context.lineTo(xMax-arrowWidth, arrowTop+lw);
                    context.lineTo(xMin+lw, arrowTop+lw);
                    context.closePath();
                    context.fill()

                    // draw arrow head
                    context.beginPath();
                    context.moveTo(xMax-arrowWidth, yMin);
                    context.lineTo(xMax, yMiddle);
                    context.lineTo(xMax-arrowWidth, yMax);
                    context.closePath();
                    context.fill()
                }
            }

            Item{
                id: durationItem
                readonly property int hCenter: (itemMiddle.xMax-itemMiddle.arrowHeadWidth + itemMiddle.xMin+itemMiddle.lineWidth)/2
                x: hCenter - width/2
                height: itemMiddle.arrowBarTop - units.gu(0.5)
                width: childrenRect.width

                Icon{
                    id: icDuration
                    name: "stopwatch"
                    height: durationItem.height
                    color: theme.palette.normal.base
                }

                Label{
                    id: lbDuration
                    anchors{
                        verticalCenter: parent.verticalCenter
                        left: icDuration.right
                        margins: units.gu(1)
                    }
                    readonly property int minsTotal: Math.round(siDuration/60.)
                    readonly property int hours: minsTotal / 60
                    readonly property int mins:  minsTotal % 60
                    text: siStartValid && siEndValid ? hours + ":" + (mins<10 ? "0":"")+mins : "  -  "
                    color: icDuration.color
                }
            }
        }

        SubsessionBoundButton{
            id: btEnd
            anchors{
                right: parent.right
                margins: units.gu(1)
            }
            start: false
            onDayChanged: root.dayChanged(start,newDay)
            onHourChanged: root.hourChanged(start,newHour)
            onMinuteChanged: root.minuteChanged(start,newMinute)
        }
    }
}
