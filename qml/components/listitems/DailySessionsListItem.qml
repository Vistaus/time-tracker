/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import ".."
import "../../dialogs"

import TTCore 1.0

ListItem {
    id: root

    Rectangle{
        id: rectTopic
        anchors{
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: units.gu(0.5)
        color: Backend.projects.getTopicColor(sessionProject)
    }

    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "delete"
                name: i18n.tr("delete")
                onTriggered: deleteDialog.open(sessionUid)
            }
        ]
    }

    ListItemLayout{
        id: layout

        padding.trailing: units.gu(3)

        Rectangle{
            id: projectColorItem
            SlotsLayout.position: SlotsLayout.First
            height: units.gu(3)
            width: units.gu(1.5)
            radius: units.gu(0.5)
            color:  Backend.projects.getColor(sessionProject)
        }

        title.text: Backend.projects.getTopicTitle(sessionProject) + "  -  " + Backend.projects.getTitle(sessionProject)
        subtitle.text: sessionDescription

        Item{
            id: durationItem
            SlotsLayout.position: SlotsLayout.Trailing
            height: units.gu(3)
            width: units.gu(4)

            Column{
                anchors.verticalCenter: parent.verticalCenter
                width: durationItem.width
                spacing: units.gu(0.5)

                Icon{
                    id: icDuration
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: units.gu(2)
                    name: "stopwatch"
                }

                Label{
                    id: lbDuration
                    anchors.horizontalCenter: icDuration.horizontalCenter
                    readonly property int hours: Math.floor(sessionDuration/3600)
                    readonly property int mins:  Math.floor(sessionDuration/60) - 60*hours
                    text: hours + (mins<10 ? ":0" : ":") + mins
                }
            }
        }

        Item{
            id: startEndItem
            SlotsLayout.position: SlotsLayout.Trailing
            height: units.gu(3)
            width:  units.gu(4)

            Column{
                anchors.verticalCenter: startEndItem.verticalCenter
                width: startEndItem.width

                Label{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: Qt.formatTime(sessionEnd,"h:mm")
                    textSize: Label.Small
                }
                Icon{
                    anchors.horizontalCenter: parent.horizontalCenter
                    name: "up"
                    height: units.gu(2)
                }
                Label{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: Qt.formatTime(sessionStart,"h:mm")
                    textSize: Label.Small
                }
            }
        }
    }

    Rectangle{
        id: startDayOffsetItem
        anchors{
            bottom: parent.bottom
            right: parent.right
            margins: units.gu(0.5)
        }
        height: units.gu(2.75)
        width:  units.gu(3)
        radius: units.gu(0.75)
        color: UbuntuColors.orange
        readonly property int offset: sessionStartDayOffset
        visible: offset !== 0
        Label{
            id: lbStartDayOffset
            anchors.centerIn: parent
            textSize: Label.Small
            text: (startDayOffsetItem.offset > 0 ? "+":"") + startDayOffsetItem.offset
        }
    }

    Rectangle{
        id: endDayOffsetItem
        anchors{
            top: parent.top
            right: parent.right
            margins: units.gu(0.5)
        }
        height: units.gu(2.75)
        width:  units.gu(3)
        radius: units.gu(0.75)
        color: UbuntuColors.orange
        readonly property int offset: sessionEndDayOffset
        visible: offset !== 0
        Label{
            id: lbEndDayOffset
            anchors.centerIn: parent
            textSize: Label.Small
            text: (endDayOffsetItem.offset > 0 ? "+":"") + endDayOffsetItem.offset
        }
    }

    onClicked: detailPopover.open(sessionUid)

    SessionDeleteDialog{
        id: deleteDialog
    }

    SessionDetailPopover{
        id: detailPopover
    }
}
