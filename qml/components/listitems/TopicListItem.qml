/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import "../../dialogs"
import ".."

import TTCore 1.0

ListItem{
    id: root
    width: parent.width
    height: units.gu(9)

    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "delete"
                name: i18n.tr("delete")
                onTriggered: deleteDialog.open(topicUid,topicTitle)
            }
        ]
    }

    trailingActions: ListItemActions{
        actions: [
            Action{
                iconName: topicStatus===0 ? "view-off" : "view-on"
                onTriggered: Backend.topics.setStatus(topicUid,1-topicStatus)
            },
            Action{
                iconName: "edit"
                name: i18n.tr("edit")
                onTriggered: editDialog.open(topicUid,topicTitle,topicColorHue)
            }
        ]
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            Backend.topics.select(topicUid)
            projectsStack.showTopicDetail()
        }
    }

    Item{
        id: content
        width: root.width
        height: root.height

        Rectangle{
            id: rectColor
            anchors{
                top: parent.top
                left: parent.left
                topMargin: units.gu(1.5)
                leftMargin: units.gu(2)
            }
            width:  units.gu(3)
            height: units.gu(4)
            radius: units.gu(1)
            color: topicColor
        }

        Label{
            id: lbTitle
            anchors{
                left: rectColor.right
                right: icDrag.left
                bottom: rectColor.verticalCenter
                leftMargin: units.gu(2)
                rightMargin: units.gu(1)
            }
            elide: Label.ElideRight
            text: topicTitle
        }

        DailyDurationChart{
            id: chart
            anchors{
                top: rectColor.verticalCenter
                left: lbTitle.left
                right: lbTitle.right
                bottom: rectColor.bottom
                topMargin: units.gu(0.25)
            }
            model: topicDailyDurations30DaysModel
            color: topicColor
        }

        Item{
            id: durationsItem
            anchors{
                top: chart.bottom
                left: chart.left
                right: chart.right
                bottom: parent.bottom
            }

            Row{
                anchors{
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                }
                spacing: units.gu(0.5)

                Icon{
                    anchors.verticalCenter: parent.verticalCenter
                    height: units.gu(2)
                    source: "/assets/calendar-week.svg"
                }

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    width: units.gu(5.5)
                    readonly property real hours: topicDailyDurations7DaysModel.sum/60
                    text: hours.toLocaleString(Qt.locale(),'f',hours<1000 ? 1 : 0) + " h"
                    textSize: Label.Small
                }
            }

            Row{
                anchors{
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
                spacing: units.gu(0.5)
                Icon{
                    anchors.verticalCenter: parent.verticalCenter
                    height: units.gu(2)
                    source: "/assets/calendar-30days.svg"
                }

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    width: units.gu(5.5)
                    readonly property real hours: topicDailyDurations30DaysModel.sum/60
                    text: hours.toLocaleString(Qt.locale(),'f',hours<1000 ? 1 : 0) + " h"
                    textSize: Label.Small
                }
            }

            Row{
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                }
                spacing: units.gu(0.5)
                Icon{
                    anchors.verticalCenter: parent.verticalCenter
                    height: units.gu(2)
                    source: "/assets/calendar-ever.svg"
                }

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    width: units.gu(5.5)
                    readonly property real hours: topicDailyDurationsModel.sum/60
                    text: hours.toLocaleString(Qt.locale(),'f',hours<1000 ? 1 : 0) + " h"
                    textSize: Label.Small
                }
            }
        }

        Icon{
            id: icDrag
            anchors{
                verticalCenter: parent.verticalCenter
                right: icNext.left
            }
            height: units.gu(3)
            name: "sort-listitem"
            MouseArea{
                id: dragMouse
                anchors{
                    fill: parent
                    margins: -units.gu(1)
                }
                drag.target: content
            }
        }


        Icon{
            id: icNext
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
            }
            width: units.gu(3)
            name: "next"
        }

        property int dragItemIndex: index

        states: [
            State {
                when: content.Drag.active
                ParentChange {
                    target: content
                    parent: reapTopics
                }
            },
            State {
                when: !content.Drag.active
                AnchorChanges {
                    target: content
                    anchors.horizontalCenter: content.parent.horizontalCenter
                    anchors.verticalCenter: content.parent.verticalCenter
                }
            }
        ]
        Drag.active: dragMouse.drag.active
        Drag.hotSpot.x: content.width / 2
        Drag.hotSpot.y: content.height / 2
    }

    DropArea{
        anchors.fill: parent
        onEntered: {
            if (drag.source.dragItemIndex > index){
                Backend.topics.swap(drag.source.dragItemIndex,index,sections.selectedIndex)
            } else if (drag.source.dragItemIndex < index){
                Backend.topics.swap(index,drag.source.dragItemIndex,sections.selectedIndex)
            }
        }
    }

    TopicEditDialog{
        id: editDialog
    }

    TopicDeleteDialog{
        id: deleteDialog
    }
}
