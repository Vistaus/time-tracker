/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Item {
    id: root

    width: parent.width
    height: rows*rowHeight + (rows-1)*spacing

    property color baseColor

    property int hueSteps: 1
    property int lightnessSteps: 2

    property int selectedHue: -1000
    property int selectedLightness: -1000
    property bool hasSelection: selectedHue !== -1000 && selectedLightness !== -1000

    property int spacing: units.gu(0.5)

    property int rowHeight: units.gu(5)
    readonly property int colWidth: (root.width - (cols-1)*spacing)/cols

    readonly property int rows: 1 + 2*hueSteps
    readonly property int cols: 1 + 2*lightnessSteps


    Column{
        width: root.width
        spacing: root.spacing
        Repeater{
            id: rowsRepeater
            model: rows
            delegate: Row{
                height: root.rowHeight
                spacing: root.spacing
                property int hueValue: index - hueSteps
                Repeater{
                    id: buttonRepeater
                    model: cols
                    delegate: Button{
                        height: root.rowHeight
                        width: root.colWidth
                        property int lightnessValue: index - lightnessSteps
                        property bool isSelected: root.selectedHue === hueValue && root.selectedLightness === lightnessValue

                        Rectangle{
                            anchors{
                                fill: parent
                                margins: -units.gu(0.5)
                            }
                            radius: units.gu(1.5)
                            visible: isSelected
                            color: theme.palette.normal.selection
                        }

                        Rectangle{
                            anchors{
                                fill: parent
                                margins: units.gu(0.5)
                            }
                            radius: units.gu(0.5)
                            color: Backend.projects.getColor(root.baseColor,hueValue,lightnessValue)
                        }
                        onClicked:{
                            root.selectedHue = hueValue
                            root.selectedLightness = lightnessValue
                        }
                    }
                }
            }
        }
    }
}
