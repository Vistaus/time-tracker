/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

/*
 * An item for the settings list which allows to manipulate a time parameter
 */
ListItem{
    id: root

    signal timeChanged(int newSecsSinceMidnight)

    // the text to display on the item
    property string text

    // states whether the switcher was checked
    property int secsSinceMidnight: 0

    readonly property int hours: Math.floor(secsSinceMidnight/3600)
    readonly property int mins:  Math.floor(secsSinceMidnight/60) - 60*hours

    QtObject{
        id: internal
        property date time: new Date(new Date().setHours(hours,mins,0,0))
    }

    ListItemLayout {
        id: layout
        anchors.fill: parent

        title.text: root.text

        Button {
            SlotsLayout.position: SlotsLayout.Last
            text: Qt.formatTime(internal.time,"hh:mm")
            onClicked: PopupUtils.open(popoverComponent,this)
        }
    }

    Component{
        id: popoverComponent
        Popover{
            id: popover
            contentHeight: picker.height + 2*picker.y
            DatePicker{
                id: picker
                x: units.gu(2)
                y: units.gu(2)
                mode: "Hours|Minutes"
                date: internal.time
                onDateChanged: root.timeChanged(date.getSeconds() + 60*date.getMinutes() + 3600*date.getHours())
            }
        }
    }
}
