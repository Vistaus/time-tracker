/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

Button{
    id: bt
    width:  units.gu(8)
    height: units.gu(3)
    y: (parent.height - height)/2

    property int padding: units.gu(0.5)
    property int spacing: units.gu(0)

    property color color1: "green"
    property color color2: "blue"

    property int index: -1

    signal selected(int index)

    onClicked: selected(bt.index)
    Rectangle{
        width:  0.5*(parent.width-2*parent.padding-parent.spacing)
        height: (parent.height-2*parent.padding)
        x: parent.padding
        y: parent.padding
        radius: parent.padding
        color: bt.color1
        Rectangle{
            color: parent.color
            anchors.right:  parent.right
            anchors.top:    parent.top
            anchors.bottom: parent.bottom
            width: parent.radius
        }
    }
    Rectangle{
        width:  0.5*(parent.width-2*parent.padding-parent.spacing)
        height: (parent.height-2*parent.padding)
        x: 0.5*(parent.width + parent.spacing)
        y: parent.padding
        radius: parent.padding
        color: bt.color2
        Rectangle{
            color: parent.color
            anchors.left:   parent.left
            anchors.top:    parent.top
            anchors.bottom: parent.bottom
            width: parent.radius
        }
    }
}
