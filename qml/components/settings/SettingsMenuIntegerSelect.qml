/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

/*
 * An item for the settings list which allows to manipulate an integer parameter
 */
ListItem{
    id: root

    // the text to display on the item
    property string text

    // the current value of the item
    property int value: 0

    // the bound of valid values
    property int minValue: 0
    property int maxValue: 10

    ListItemLayout {
        id: layout
        anchors.fill: parent

        title.text: root.text

        Row{
            SlotsLayout.position: SlotsLayout.Trailing
            id: selectorItem
            height: units.gu(4)

            Button{
                id: btDec
                height: selectorItem.height
                width: units.gu(5)
                color: theme.palette.normal.negative
                enabled: root.value > root.minValue
                Label{
                    text: "-"
                    anchors.centerIn: parent
                    font.bold: true
                    color: theme.palette.normal.negativeText
                }
                onClicked: root.value -= 1
                Timer{
                    id: decTimer
                    running: btDec.pressed
                    repeat: true
                    interval: 200
                    onTriggered: {
                        if (root.value > root.minValue)
                            root.value -= 1
                    }
                }
            }

            Label{
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(5)
                horizontalAlignment: Label.AlignHCenter
                text: root.value
            }

            Button{
                id: btInc
                height: selectorItem.height
                width: units.gu(5)
                color: theme.palette.normal.positive
                enabled: root.value < root.maxValue
                Label{
                    text: "+"
                    anchors.centerIn: parent
                    font.bold: true
                    color: theme.palette.normal.positiveText
                }
                onClicked: root.value += 1
                Timer{
                    id: incTimer
                    running: btInc.pressed
                    repeat: true
                    interval: 200
                    onTriggered: {
                        if (root.value < root.maxValue)
                            root.value += 1
                    }
                }
            }
        }
    }

}
