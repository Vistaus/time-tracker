/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3

/*
 * An item for the settings list which pushs a new subpage to the stack if clicked on
 */
ListItem{
    id: root

    // the text to display on the item
    property string text

    // the subpage which should be pushed on stack
    property string subpage: ""

    // if set, the corresponding icon is shown on the left
    property string iconName

    ListItemLayout {
        id: layout
        anchors.fill: parent

        title.text: root.text

        Icon{
            SlotsLayout.position: SlotsLayout.First
            height: units.gu(3)
            name: iconName
        }

        Icon {
            SlotsLayout.position: SlotsLayout.Last
            height: units.gu(2)
            visible: subpage !== ""
            name: "next"
        }
    }

    onClicked:{
        if (subpage !== "")
            pages.push(Qt.resolvedUrl(subpage))
    }
}
