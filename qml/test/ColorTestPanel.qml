/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root
    ScrollView{
        anchors.fill: parent
        Column{
            Repeater{
                model: 4
                delegate: Row{
                    property int row: index
                    Repeater{
                        model: 3
                        delegate: ColorTest{
                            width: 0.333*root.width
                            baseHue: ((index + 3*row) / 12) % 1.
                        }
                    }
                }
            }
        }
    }
}
