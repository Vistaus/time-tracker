/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root

    height: rows*cellSize

    property int cols: 3
    property int rows: 5

    readonly property int cellSize: width/cols

    property real baseHue:        0./12.
    property real baseLightness:  0.5
    property real baseSaturation: 1

    property real deltaHue: 5./360.
    property real deltaLightness: 0.08
    property real deltaSaturation: 0.2

    Column{
        Repeater{
            model: rows
            delegate: Row{
                property real lightness: baseLightness + (index-2)*deltaLightness
                Repeater{
                    model: cols
                    delegate: Rectangle{
                        width: root.cellSize
                        height: width
                        property real hue: baseHue + (index-1)*deltaHue
                        property real saturation: baseSaturation - Math.abs(index-1)*deltaSaturation
                        color: Qt.hsla(hue<0 ? hue+1 : hue,saturation,lightness)
                    }
                }
            }
        }
    }
}
