/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import "../../components/listitems"
import "../../dialogs"

import TTCore 1.0

Item {
    id: root

    ListItem{
        id: header
        width: parent.width
        height: units.gu(7)

        leadingActions: ListItemActions{
            actions: [
                Action{
                    iconName: "delete"
                    name: i18n.tr("delete")
                    onTriggered: topicDeleteDialog.open(
                                     Backend.topics.selected.uid,
                                     Backend.topics.selected.title
                                 )
                }
            ]
        }

        trailingActions: ListItemActions{
            actions: [
                Action{
                    iconName: "edit"
                    name: i18n.tr("edit")
                    onTriggered: topicEditDialog.open(
                                     Backend.topics.selected.uid,
                                     Backend.topics.selected.title,
                                     Backend.topics.selected.colorHue
                                 )
                }
            ]
        }

        Rectangle{
            anchors.fill: parent
            color: colors.currentHeader
            opacity: 0.3
        }

        Icon {
            anchors{
                top: parent.top
                left: parent.left
                topMargin: units.gu(1.5)
                leftMargin: units.gu(1)
            }
            height: units.gu(3)
            name: "back"
            MouseArea{
                anchors{
                    fill: parent
                    margins: -units.gu(1)
                }
                onClicked: projectsStack.showTopicList()
            }
        }

        ListItemLayout{
            id: headerLayout
            padding{
                top: units.gu(1)
                leading: units.gu(3)
            }

            Rectangle{
                SlotsLayout.position: SlotsLayout.First
                width:  units.gu(3)
                height: units.gu(4)
                radius: units.gu(1)
                color: Backend.topics.selected.color
            }

            title.text: Backend.topics.selected.title
            subtitle.text: Backend.topics.selected.status === 1 ? "("+i18n.tr("inactive")+")" : ""

            MouseArea{
                SlotsLayout.position: SlotsLayout.Last
                height: units.gu(4)
                width: units.gu(4)
                Icon{
                    anchors.centerIn: parent
                    height: units.gu(3)
                    name: "add"
                }
                onClicked: projectAddDialog.open(Backend.topics.selected.uid)
            }
        }
    }


    Sections{
        id: sections
        anchors{
            top: header.bottom
            left: parent.left
            right: parent.right
        }
        visible: Backend.projects.selectedTopicModel.inactiveCount > 0

        model: [
            i18n.tr("Active projects") + " ("+Backend.projects.selectedTopicModel.activeCount+")",
            i18n.tr("Inactive projects") + " ("+Backend.projects.selectedTopicModel.inactiveCount+")"
        ]
    }

    UbuntuListView{
        id: projectsListView
        anchors{
            fill: parent
            topMargin: header.height + (sections.visible ? sections.height : 0)
        }
        currentIndex: -1

        model: SortFilterModel{
            model: Backend.projects.selectedTopicModel
            filter{
                property: "projectStatus"
                pattern: sections.selectedIndex === 0 ? /0/ : /1/
            }
        }
        delegate: ProjectListItem{}
    }

    TopicEditDialog{
        id: topicEditDialog
    }

    TopicDeleteDialog{
        id: topicDeleteDialog
    }

    ProjectAddDialog{
        id: projectAddDialog
    }
}
