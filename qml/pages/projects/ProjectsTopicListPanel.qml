/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import "../../components/listitems"
import "../../dialogs"

import TTCore 1.0

Item{
    id: root

    Sections{
        id: sections
        anchors{
            top: parent.top
            left: parent.left
            right: parent.right
        }
        visible: Backend.topics.inactiveModel.count > 0

        model: [
            i18n.tr("Active topics") + " ("+Backend.topics.activeModel.count+")",
            i18n.tr("Inactive topics") + " ("+Backend.topics.inactiveModel.count+")"
        ]
    }

    ScrollView{
        id: scroll
        anchors{
            fill: parent
            topMargin: sections.visible ? sections.height : 0
        }
        Column{
            id: col
            width: scroll.width

            Repeater{
                id: reapTopics
                model: sections.selectedIndex === 0 ? Backend.topics.activeModel
                                                    : Backend.topics.inactiveModel
                delegate: TopicListItem{}
            }

            Item{
                id: emptyListItem
                width: col.width
                height: units.gu(8)
                visible: Backend.topics.activeModel.count === 0 && Backend.topics.inactiveModel.count === 0
                Label{
                    anchors.centerIn: parent
                    text: i18n.tr("No projects available.")
                }
            }

            Item{
                id: itemNewTopic
                width: col.width
                height: units.gu(6)
                Button{
                    anchors.centerIn: parent
                    color: theme.palette.normal.positive
                    Icon{
                        anchors.centerIn: parent
                        height: parent.height - units.gu(1)
                        color: theme.palette.normal.positiveText
                        name: "add"
                    }
                    onClicked: addDialog.open()
                }
            }
        }
    }

    TopicAddDialog{
        id: addDialog
    }
}
