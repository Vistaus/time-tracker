/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Qt.labs.settings 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3

import "../components"
import "../components/listitems"

import TTCore 1.0

BottomEdge {
    height: parent.height

    /* ---- hint ---- */
    hint.status: BottomEdgeHint.Active
    hint.text: i18n.tr("Live Session")
    onCollapseStarted: hint.status = BottomEdgeHint.Active
    onCommitStarted: {
        if (contentItem)
            contentItem.checkValues()
    }

    contentComponent: Rectangle{
        height: bottomEdge.height
        width:  bottomEdge.width
        color: colors.currentBackground

        function checkValues(){
            inputProject.checkValues()
        }

        Settings{
            id: settings
            category: "LiveSession"
            property alias selectedTopic: inputProject.selectedTopic
            property alias selectedProject: inputProject.selectedProject
            property alias description: inputDescription.text
        }

        PageHeader{
            id: header
            title: i18n.tr("Live Session")
            StyleHints{backgroundColor: colors.currentHeader}
            MouseArea{
                anchors.fill: parent
                onClicked: bottomEdge.collapse()
            }
        }

        ScrollView{
            id: scroll
            anchors{
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: divBottom.top
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }

            Column{
                id: col
                width: scroll.width
                spacing: units.gu(2)
                topPadding: units.gu(2)

                Label{
                    text: i18n.tr("Description")
                }
                TextField{
                    id: inputDescription
                    x: units.gu(2)
                    width: col.width - 2*x
                    placeholderText: i18n.tr("insert a description") + " ..."
                }

                ProjectSelector{
                    id: inputProject
                    x: units.gu(2)
                    width: col.width - 2*x
                }

                Label{
                    text: i18n.tr("Start date")
                }
                SessionStartDateSelectButton{
                    x: units.gu(2)
                    width: col.width - 2*x
                    model: Backend.sessions.liveSessionIntervalModel
                    enabled: false
                }

                Divider{}

                Label{
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: units.gu(10)
                    verticalAlignment: Label.AlignVCenter
                    text: i18n.tr("No live session")
                    visible: !Backend.sessions.hasLiveSession
                }

                Column{
                    id: subsessionsCol
                    width: col.width

                    Repeater{
                        model: Backend.sessions.liveSessionIntervalModel
                        delegate: SubsessionListItem{
                            onDayChanged: Backend.sessions.updateLiveSessionDayStamp(index,start,newDay)
                            onHourChanged: Backend.sessions.updateLiveSessionHourStamp(index,start,newHour)
                            onMinuteChanged: Backend.sessions.updateLiveSessionMinuteStamp(index,start,newMinute)
                        }
                    }
                }
            }
        }

        Divider{
            id: divBottom
            anchors{
                left: parent.left
                right: parent.right
                bottom: buttons.top
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
        }

        LiveSessionButtons{
            id: buttons
        }
    }
}
