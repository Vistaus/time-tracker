/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components/settings"

Page {
    id: root

    header: PageHeader{
        StyleHints{backgroundColor: colors.currentHeader}
        title: i18n.tr("Time Tracker") + " - " + i18n.tr("Settings")
    }

    ScrollView{
        id: scroll
        anchors{
            fill: parent
            topMargin: header.height
        }
        Column{
            width: scroll.width

            SettingsMenuItem{
                id: stManual
                text: i18n.tr("Show manual")
                iconName: "info"
                onClicked: manual.show()
            }

            SettingsMenuItem{
                text: i18n.tr("Default values")
                subpage: "qrc:/pages/SettingsParameterPage.qml"
            }


            SettingsCaption{title: i18n.tr("Appearance")}
            SettingsMenuSwitch{
                text: i18n.tr("Use default theme")
                Component.onCompleted: checked = colors.useDefaultTheme
                onCheckedChanged: colors.useDefaultTheme = checked
            }
            SettingsMenuSwitch{
                enabled: !colors.useDefaultTheme
                text: i18n.tr("Dark Mode")
                onCheckedChanged: colors.darkMode = checked
                Component.onCompleted: checked = colors.darkMode
            }
            SettingsMenuDoubleColorSelect{
                enabled: !colors.useDefaultTheme
                text: i18n.tr("Color")
                model: colors.headerColors
                Component.onCompleted: currentSelectedColor =  colors.currentIndex
                onCurrentSelectedColorChanged: colors.currentIndex = currentSelectedColor
            }
        }
    }
}
