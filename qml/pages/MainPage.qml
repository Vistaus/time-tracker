/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Qt.labs.settings 1.1
import QtQuick.Layouts 1.12 as QQL
import Ubuntu.Components 1.3

import "../components"
import "../dialogs"

Page {
    id: root

    Settings{
        id: settingsMainPage
        category: "MainPage"
        property alias selectedSection: sections.selectedIndex
    }

    header: PageHeader{
        leadingActionBar.actions: [
            Action{
                iconName: "add"
                text: i18n.tr("Add session")
                onTriggered: sessionAddDialog.open()
            },
            Action{
                iconName: "share"
                text: i18n.tr("Export")
                onTriggered: pages.push(Qt.resolvedUrl("ExportPage.qml"))
            },
            Action{
                iconName: "settings"
                text: i18n.tr("Settings")
                onTriggered: pages.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        ]
        StyleHints{backgroundColor: colors.currentHeader}
        title: i18n.tr("Time Tracker")
    }

    TTMainSections{
        id: sections
        anchors{
            top: header.bottom
            left: parent.left
            right: parent.right
        }
    }

    QQL.StackLayout{
        id: stack
        anchors{
            top: sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        currentIndex: sections.selectedIndex

        CalendarPanel{
            id: calendarPanel
        }

        SessionsPanel{
            id: sessionsPanel
        }

        ProjectsPanel{
            id: projectsPanel
        }
    }

    LiveSessionBottomEdge{
        id: bottomEdge
    }

    SessionAddDialog{
        id: sessionAddDialog
    }
}
