/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Qt.labs.settings 1.1
import Ubuntu.Components 1.3

import "../components/calendar"

import TTCore 1.0

Item {
    id: root

    Settings{
        id: calSettings
        category: "Calendar"
        property alias calendarMode: calendarModeSelection.selectedIndex
    }

    Sections{
        id: calendarModeSelection
        anchors{
            top: parent.top
            left: parent.left
            right: parent.right
        }
        model: [
            i18n.tr("Week"),
            i18n.tr("Month")
        ]
        onSelectedIndexChanged: {
            switch (selectedIndex){
            case 0:
                Backend.calendar.mode = CalendarViewManager.WEEKLY
                break
            case 1:
                Backend.calendar.mode = CalendarViewManager.MONTHLY
                break
            }
        }
    }

    CalendarNavigationPanel{
        id: navigationPanel
        anchors.top: calendarModeSelection.bottom
    }

    CalendarView{
        id: calendarView
        anchors{
            top: navigationPanel.bottom
            left: parent.left
            right: parent.right
            bottom: selectionDetailPanel.top
            rightMargin: units.gu(1)
        }
    }

    CalendarSelectionDetailPanel{
        id: selectionDetailPanel
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }
}
