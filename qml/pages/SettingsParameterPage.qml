/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components/settings"

Page {
    id: root

    header: PageHeader{
        StyleHints{backgroundColor: colors.currentHeader}
        title: i18n.tr("Time Tracker") + " - " + i18n.tr("Default values")
    }

    ScrollView{
        id: scroll
        anchors{
            fill: parent
            topMargin: header.height
        }
        Column{
            width: scroll.width

            SettingsCaption{title: i18n.tr("New Session")}
            SettingsMenuTimeSelect{
                text: i18n.tr("Start time")
                secsSinceMidnight: settings.newSessionDefaultStartSecond
                onTimeChanged: settings.newSessionDefaultStartSecond = newSecsSinceMidnight
            }
            SettingsMenuIntegerSelect{
                text: i18n.tr("Duration (minutes)")
                value: settings.newSessionDefaultDuration/60
                onValueChanged: settings.newSessionDefaultDuration = 60*value
                minValue: 1
                maxValue: 1440
            }
            SettingsMenuIntegerSelect{
                text: i18n.tr("Pause length (minutes)")
                value: settings.newSessionDefaultPause/60
                onValueChanged: settings.newSessionDefaultPause = 60*value
                minValue: 1
                maxValue: 1440
            }
        }
    }
}
