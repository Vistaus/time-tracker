/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3

import TTCore 1.0

Page {
    id: root

    header: PageHeader{
        StyleHints{backgroundColor: colors.currentHeader}
        title: i18n.tr("Time Tracker") + " - " + i18n.tr("Export Data")
    }

    ScrollView{
        id: scroll
        anchors{
            fill: parent
            topMargin: header.height
        }

        Column{
            id: col
            width: scroll.width

            padding: units.gu(2)
            spacing: units.gu(2)
            readonly property int contentWidth: width - 2*padding

            Label{
                text: i18n.tr("Filename")
            }

            Row{
                height: inputFilename.height
                spacing: units.gu(1)

                TextField{
                    id: inputFilename
                    width: col.contentWidth - lbCSV.width - units.gu(1)
                    readonly property date now: new Date()
                    placeholderText: Qt.formatDate(now,"yyyy-MM-dd") + "-timetracker"
                    function getFilename(){
                        var name = text.length > 0 ? text : placeholderText

                        return name.trim().replace(/ /g,"_") + ".csv"
                    }
                }
                Label {
                    id: lbCSV
                    anchors.verticalCenter: inputFilename.verticalCenter
                    text: ".csv"
                }
            }

            Label{
                text: i18n.tr("Separator")
            }
            OptionSelector{
                width: col.contentWidth
                model: ExportManager.NUMBER_OF_SEPARATORS
                selectedIndex: Backend.exports.separator
                onSelectedIndexChanged: Backend.exports.separator = selectedIndex
                delegate: OptionSelectorDelegate{
                    width: col.contentWidth
                    text: modelData === ExportManager.SEPARATOR_TAB   ? "<TAB>" :
                          modelData === ExportManager.SEPARATOR_BLANK ? i18n.tr("\" \" (blank)")
                                                                      : Backend.exports.getSeparator(modelData)
                }
            }

            Button{
                anchors.horizontalCenter: parent.horizontalCenter
                width: col.contentWidth - units.gu(8)
                color: UbuntuColors.orange
                text: i18n.tr("Export")
                onClicked: {
                    Backend.exports.filename = inputFilename.getFilename()
                    pageStack.push(Qt.resolvedUrl("ExportSelectPage.qml"))
                }
            }
        }
    }
}
