/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "backend.h"

Backend::Backend(QObject *parent)
    : QObject(parent)
    , m_sessions(new SessionManager)
    , m_topics(new TopicManager)
    , m_projects(new ProjectManager(m_topics,m_sessions))
    , m_calendar(new CalendarViewManager(m_sessions,m_projects))
    , m_exports(new ExportManager(m_sessions,m_projects)){

    connect(m_topics,SIGNAL(topicAdded(uint)),m_projects,SLOT(onTopicAdded(uint)));
    connect(m_topics,SIGNAL(topicRemoved(uint)),m_projects,SLOT(onTopicRemoved(uint)));
    connect(m_topics,SIGNAL(topicSelected(uint)),m_projects,SLOT(onTopicSelected(uint)));
    connect(m_topics,SIGNAL(topicColorChanged(uint)),m_projects,SLOT(onTopicColorChanged(uint)));

    connect(m_projects,SIGNAL(projectRemoved(uint)),m_sessions,SLOT(onProjectRemoved(uint)));

    connect(m_sessions,SIGNAL(sessionAdded(uint)),m_projects,SLOT(onSessionAdded(uint)));
    connect(m_sessions,SIGNAL(sessionRemoved(uint)),m_projects,SLOT(onSessionRemoved(uint)));
    connect(m_sessions,SIGNAL(sessionUpdated(uint)),m_projects,SLOT(onSessionUpdated(uint)));

    qInfo() << "[INIT] Backend created.";
}
