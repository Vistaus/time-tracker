/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "dailyvaluemodel.h"

int DailyValueModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant DailyValueModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case DATEROLE:
        return QVariant(m_list.at(index.row()).date);
    case VALUEROLE:
        return QVariant(m_list.at(index.row()).value);
    }
    return QVariant();
}

QHash<int, QByteArray> DailyValueModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[DATEROLE]  = "modelDate";
    names[VALUEROLE] = "modelValue";
    return names;
}

void DailyValueModel::setTimeRange(const QDate &first, const QDate &last){
    QVector<DailyValueModelNode> newList;
    const int offsetToLast = first.daysTo(last);
    for (int i=0; i<=offsetToLast; i++)
        newList.append({first.addDays(i),0});

    for (const DailyValueModelNode &node : m_list){
        const int idx = newList.indexOf(node);
        if (idx > -1)
            newList[idx].value = node.value;
    }

    beginResetModel();
    m_list.swap(newList);
    endResetModel();

    if (m_list.size() != newList.size())
        emit countChanged();

    m_sum = 0;
    for (const DailyValueModelNode &node : m_list)
        m_sum += node.value;
    emit sumChanged();
}

void DailyValueModel::add(const QDate &date, const qreal value){
    const DailyValueModelNode node = {date,value};
    const int idx = m_list.indexOf(node);
    if (idx < 0){
        int insertIdx = 0;
        while (insertIdx<m_list.size() && m_list.at(insertIdx).date<date)
            insertIdx++;
        beginInsertRows(QModelIndex(),insertIdx,insertIdx);
        m_list.insert(insertIdx,node);
        endInsertRows();
    } else {
        m_list[idx].value += value;
        emit dataChanged(index(idx),index(idx),{VALUEROLE});
    }

    m_sum += value;
    emit sumChanged();
}

void DailyValueModel::set(const QDate &date, const qreal value){
    const DailyValueModelNode node = {date,value};
    const int idx = m_list.indexOf(node);
    if (idx < 0){
        int insertIdx = 0;
        while (insertIdx<m_list.size() && m_list.at(insertIdx).date<date)
            insertIdx++;
        beginInsertRows(QModelIndex(),insertIdx,insertIdx);
        m_list.insert(insertIdx,node);
        endInsertRows();
    } else {
        m_sum -= m_list[idx].value;
        m_list[idx].value = value;
        emit dataChanged(index(idx),index(idx),{VALUEROLE});
    }

    m_sum += value;
    emit sumChanged();
}

bool DailyValueModel::isInRange(const QDate &date) const{
    if (m_list.size() == 0)
        return false;

    return date >= m_list.front().date && date <= m_list.back().date;
}
