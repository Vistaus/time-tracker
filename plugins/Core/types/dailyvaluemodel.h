/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef DAILYVALUEMODEL_H
#define DAILYVALUEMODEL_H

#include <QAbstractListModel>
#include <QDate>
#include <QVector>


struct DailyValueModelNode{
    QDate date;
    qreal value;
};

inline bool operator==(const DailyValueModelNode &n1, const DailyValueModelNode &n2){
    return n1.date == n2.date;
}

class DailyValueModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
    Q_PROPERTY(int  sum   READ sum   NOTIFY sumChanged)
public:
    enum Roles{
        DATEROLE = Qt::UserRole,
        VALUEROLE
    };

    explicit DailyValueModel(QObject *parent = nullptr) : QAbstractListModel(parent){}

    /* --------------------- *
     * --- QML interface --- */
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    inline uint count() const {return m_list.size();}
    inline int  sum()   const {return m_sum;}

    /* --------------------- *
     * --- C++ interface --- */
    void setTimeRange(const QDate &first, const QDate &last);
    void add(const QDate &date, const qreal value);
    void set(const QDate &date, const qreal value);

    bool isInRange(const QDate &date) const;

signals:
    void countChanged();
    void sumChanged();

private:
    QVector<DailyValueModelNode> m_list;

    int m_sum = 0;
};

#endif // DAILYVALUEMODEL_H
