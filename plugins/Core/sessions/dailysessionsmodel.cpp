/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "dailysessionsmodel.h"

DailySessionsModel::DailySessionsModel(QObject *parent)
    : QAbstractListModel(parent){
}


int DailySessionsModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant DailySessionsModel::data(const QModelIndex &index, int role) const{
    if (index.row() >= m_list.size())
        return QVariant();

    switch (role) {
    case DATEROLE:
        return QVariant(m_list.at(index.row()).date);
    case MODELROLE:
        return QVariant::fromValue<SessionModel*>(m_list.at(index.row()).model);
    }
    return QVariant();
}

QHash<int, QByteArray> DailySessionsModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[DATEROLE]  = "dsmDate";
    names[MODELROLE] = "dsmModel";
    return names;
}

void DailySessionsModel::addSession(const Session &s){

    const int idx = m_list.indexOf(s.startDate);
    if (idx<0){
        int insertIdx = 0;
        while (insertIdx < m_list.size() && m_list.at(insertIdx).date > s.startDate)
            insertIdx++;

        beginInsertRows(QModelIndex(),insertIdx,insertIdx);
        m_list.insert(insertIdx,DailySessionsModelElement(s.startDate,new SessionModel));
        endInsertRows();
        emit countChanged();

        m_list[insertIdx].model->add(s);

    } else {
        m_list[idx].model->add(s);
    }
}

void DailySessionsModel::removeSession(const uint uid){
    for (int i=0; i<m_list.size(); i++){
        SessionModel *model = m_list.at(i).model;
        if (model->remove(uid)){
            if (model->count() == 0){
                beginRemoveRows(QModelIndex(),i,i);
                m_list.removeAt(i);
                endRemoveRows();
                delete model;
            }
            return;
        }
    }
}

void DailySessionsModel::updateSession(const Session &s){
    bool added = false;
    bool removed = false;
    for (int i=0; i<m_list.size(); i++){
        SessionModel *model = m_list.at(i).model;
        const int idx = model->find(s.uid);
        if (idx < 0){
            if (m_list.at(i).date == s.startDate){
                model->add(s);
                added = true;
            }
        } else {
            if (m_list.at(i).date == s.startDate) {
                model->update(s);
                removed = true;
                added = true;
            } else {
                model->remove(s.uid);
                if (model->count() == 0){
                    beginRemoveRows(QModelIndex(),i,i);
                    m_list.removeAt(i);
                    endRemoveRows();
                    delete model;
                    i--;
                }
                removed = true;
            }
        }
        if (added && removed)
            return;
    }
    if (!added && removed)
        addSession(s);
}
