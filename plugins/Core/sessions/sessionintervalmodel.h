/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SESSIONINTERVALMODEL_H
#define SESSIONINTERVALMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QDate>
#include <QTime>

#include "session.h"

class SessionIntervalModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
    Q_PROPERTY(QDate startDate READ startDate WRITE setStartDate NOTIFY startDateChanged)

public:
    enum Roles{
        UIDROLE = Qt::UserRole,
        STARTVALIDROLE,
        STARTDAYROLE,
        STARTHOURROLE,
        STARTMINUTEROLE,
        ENDVALIDROLE,
        ENDDAYROLE,
        ENDHOURROLE,
        ENDMINUTEROLE,
        DURATIONROLE,
        MINSTARTVALIDROLE,
        MINSTARTDAYROLE,
        MINSTARTHOURROLE,
        MINSTARTMINUTEROLE,
        MAXENDVALIDROLE,
        MAXENDDAYROLE,
        MAXENDHOURROLE,
        MAXENDMINUTEROLE,
        PAUSEROLE
    };

    inline SessionIntervalModel(QObject *parent = nullptr) : QAbstractListModel(parent), m_startDate(QDate::currentDate()) {}

    /* ----------------------- *
     * ---- QML interface ---- */
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    inline uint count() const {return m_list.size();}
    inline QDate startDate() const {return m_startDate;}

    Q_INVOKABLE void insertInterval(const int duration = 3600, const int pause = 43200);
    Q_INVOKABLE void updateDayAt(const uint idx, const bool start, const int day);
    Q_INVOKABLE void updateHourAt(const uint idx, const bool start, const int hour);
    Q_INVOKABLE void updateMinuteAt(const uint idx, const bool start, const int minute);
    Q_INVOKABLE void removeIntervalAt(const uint idx);
    Q_INVOKABLE void clear();

    /* ----------------------- *
     * ---- C++ interface ---- */
    inline const SessionIntervalList &list() const {return m_list;}

    bool isFinished() const;

    void insertInterval(const int startDay, const QTime &startTime, const int endDay, const QTime &endTime, const uint uid = 0);
    SessionInterval &startInterval();
    const SessionInterval &stopInterval();

    void setList(const SessionIntervalList &list);

public slots:
    void setStartDate(const QDate &startDate);

signals:
    void countChanged();
    void startDateChanged();

private:
    SessionIntervalList m_list;
    QDate m_startDate;
};

#endif // SESSIONINTERVALMODEL_H
