/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SESSION_H
#define SESSION_H

#include <QString>
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QVector>
#include <QList>

struct SessionInterval{
    uint uid = 0;

    int   startDay = 0;
    QTime startTime;
    int   endDay = 0;
    QTime endTime;

    inline int startSecs() const{
        return 86400*startDay + startTime.msecsSinceStartOfDay()/1000;
    }
    inline int endSecs() const{
        return 86400*endDay + endTime.msecsSinceStartOfDay()/1000;
    }
    inline int duration() const{
        return startTime.isValid() && endTime.isValid() ? endSecs() - startSecs()
                                                        : 0;
    }
};

inline bool operator==(const SessionInterval &i1, const SessionInterval &i2){
    return i1.uid == i2.uid;
}


typedef QVector<SessionInterval> SessionIntervalList;





// key = day offset to startDate
// value = minutes
typedef QHash<int,int> SessionDailyDuration;


struct Session{
    uint uid = 0;

    uint project = 0;
    QString description;

    QDate startDate;

    SessionIntervalList intervals;

    inline QDateTime start() const{
        QDateTime t(startDate);
        if (intervals.size()>0)
            t = t.addSecs(intervals.front().startSecs());
        return t;
    }
    inline QDateTime end() const{
        QDateTime t(startDate);
        if (intervals.size()>0)
            t = t.addSecs(intervals.back().endSecs());
        return t;
    }
    inline int duration() const{
        int dur = 0;
        for (const SessionInterval &si : intervals)
            dur += si.duration();
        return dur;
    }

    inline bool isValid() const{
        return uid>0 && project>0 && intervals.size()>0;
    }

    inline void addInterval(const SessionInterval &si){
        int i=0;
        const int startSecs = si.startSecs();
        while (i<intervals.size() && startSecs > intervals.at(i).startSecs())
            i++;
        intervals.insert(i,si);
    }

    inline SessionDailyDuration dailyDurations() const{
        SessionDailyDuration dailyDurations;
        for (const SessionInterval &si : intervals){
            for (int i=si.startDay; i<=si.endDay; i++){
                const int startMinutes = (i==si.startDay) ? si.startTime.minute() + 60*si.startTime.hour() : 0;
                const int endMinutes   = (i==si.endDay)   ? si.endTime.minute() + 60*si.endTime.hour() : 1440;
                const int dur = endMinutes - startMinutes;
                if (dailyDurations.contains(i))
                    dailyDurations[i] += dur;
                else
                    dailyDurations[i] = dur;
            }
        }
        return dailyDurations;
    }
};

inline bool operator==(const Session &s1, const Session &s2){
    return s1.uid == s2.uid;
}


class SessionList : public QVector<Session>
{
public:
    inline int find(const uint uid) const{
        for (int i=0; i<size(); i++)
            if (at(i).uid == uid)
                return i;
        return -1;
    }
};

#endif // SESSION_H
