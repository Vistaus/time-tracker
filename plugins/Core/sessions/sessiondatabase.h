/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SESSIONDATABASE_H
#define SESSIONDATABASE_H

#include <QDebug>
#include <QString>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>

#include "session.h"

class SessionDatabase
{
public:
    SessionDatabase();

    bool read(SessionList &list);
    bool insert(Session &s);
    bool remove(const uint uid);
    bool updateSession(const Session &s);
    bool updateInterval(const SessionInterval &si);

private:
    const QString m_db_name = "sessions";
};

#endif // SESSIONDATABASE_H
