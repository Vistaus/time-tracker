/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "livesessiondatabase.h"

LiveSessionDatabase::LiveSessionDatabase(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",m_db_name);
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + m_db_name + ".sqlite");
    if (!db.open()){
        qCritical() << "[ERROR] LiveSessionDatabase::LiveSessionDatabase() - could not open database: " << db.lastError().text();
        return;
    }

    QSqlQuery query(db);
    query.prepare("CREATE TABLE intervals ("
                  "uid INTEGER PRIMARY KEY,"
                  "startDay INTEGER,"
                  "startTime INTEGER,"
                  "endDay INTEGER,"
                  "endTime INTEGER)"
                  );

    if (query.exec())
        qInfo() << "[INFO] LiveSessionDatabase: table for session intervals created";

    db.close();
}

bool LiveSessionDatabase::read(SessionIntervalModel *model){
    if (!model)
        return false;

    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] LiveSessionDatabase::read() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("SELECT * FROM intervals");

    if (!query.exec()){
        qCritical() << "[ERROR] LiveSessionDatabase::read() - could not select intervals: " << query.lastError().text();
        db.close();
        return false;
    }

    model->clear();
    while (query.next()){
        try{
            const int endTime = query.value("endTime").toInt();
            model->insertInterval(
                        query.value("startDay").toInt(),
                        QTime::fromMSecsSinceStartOfDay(query.value("startTime").toInt()),
                        query.value("endDay").toInt(),
                        (endTime >= 0 ? QTime::fromMSecsSinceStartOfDay(query.value("endTime").toInt()) : QTime()),
                        query.value("uid").toUInt()
                        );
        } catch (std::exception e) {
            qWarning() << "[ERROR] LiveSessionDatabase::read() - could not parse a session: " << e.what() << " - Ignore and continue with next.";
        }
    }

    db.close();
    return true;
}

bool LiveSessionDatabase::insert(SessionInterval &si){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] LiveSessionDatabase::insert() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("INSERT INTO intervals (startDay,startTime,endDay,endTime) VALUES "
                  "("+QString::number(si.startDay)+
                  ","+QString::number(si.startTime.msecsSinceStartOfDay())+
                  ","+QString::number(si.endDay)+
                  ","+(si.endTime.isValid() ? QString::number(si.endTime.msecsSinceStartOfDay()) : "-1")+
                  ")");

    if (!query.exec()){
        qCritical() << "[ERROR] LiveSessionDatabase::insert() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    si.uid = query.lastInsertId().toUInt();
    db.close();
    return true;
}

bool LiveSessionDatabase::update(const SessionInterval &si){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] LiveSessionDatabase::update() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("UPDATE intervals SET"
                  " startDay="+QString::number(si.startDay)+
                  ",startTime="+QString::number(si.startTime.isValid() ? si.startTime.msecsSinceStartOfDay() : -1)+
                  ",endDay="+QString::number(si.endDay)+
                  ",endTime="+QString::number(si.endTime.isValid() ? si.endTime.msecsSinceStartOfDay() : -1)+
                  " WHERE uid="+QString::number(si.uid));

    if (!query.exec()){
        qCritical() << "[ERROR] LiveSessionDatabase::update() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}

bool LiveSessionDatabase::clear(){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] LiveSessionDatabase::clear() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("DELETE FROM intervals");

    if (!query.exec()){
        qCritical() << "[ERROR] LiveSessionDatabase::clear() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}
