/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "sessionmodel.h"

SessionModel::SessionModel(QObject *parent)
    : QAbstractListModel(parent){
}

int SessionModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant SessionModel::data(const QModelIndex &index, int role) const{
    if (index.row() >= m_list.size())
        return QVariant();

    const Session &s = m_list.at(index.row());
    switch (role) {
    case UIDROLE:
        return QVariant(s.uid);
    case PROJECTROLE:
        return QVariant(s.project);
    case DESCRIPTIONROLE:
        return QVariant(s.description);
    case DURATIONROLE:
        return QVariant(s.duration());
    case STARTROLE:
        return QVariant(s.start());
    case ENDROLE:
        return QVariant(s.end());
    case STARTDAYOFFSETROLE:
        return QVariant(s.startDate.daysTo(s.start().date()));
    case ENDDAYOFFSETROLE:
        return QVariant(s.startDate.daysTo(s.end().date()));
    }
    return QVariant();
}

QHash<int, QByteArray> SessionModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[UIDROLE]         = "sessionUid";
    names[PROJECTROLE]     = "sessionProject";
    names[DESCRIPTIONROLE] = "sessionDescription";
    names[DURATIONROLE]    = "sessionDuration";
    names[STARTROLE]       = "sessionStart";
    names[ENDROLE]         = "sessionEnd";
    names[STARTDAYOFFSETROLE] = "sessionStartDayOffset";
    names[ENDDAYOFFSETROLE]   = "sessionEndDayOffset";
    return names;
}

int SessionModel::find(const uint uid) const{
    for (int i=0; i< m_list.size(); i++)
        if (m_list.at(i).uid == uid)
            return i;
    return -1;
}

void SessionModel::add(const Session &s){
    int insertIdx = 0;
    while (insertIdx < m_list.size() && m_list.at(insertIdx).start() > s.start())
        insertIdx++;

    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.insert(insertIdx,s);
    endInsertRows();

    emit countChanged();
}

void SessionModel::update(const Session &s){
    for (int i=0; i<m_list.size(); i++){
        if (m_list.at(i).uid == s.uid){
            m_list.replace(i,s);
            emit dataChanged(index(i),index(i));
        }
    }
}

bool SessionModel::remove(const uint uid){
    for (int i=0; i<m_list.size(); i++){
        if (m_list.at(i).uid == uid){
            beginRemoveRows(QModelIndex(),i,i);
            m_list.removeAt(i);
            endRemoveRows();
            emit countChanged();
            return true;
        }
    }
    return false;
}
