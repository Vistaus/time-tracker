/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SESSIONSTATSRECORD_H
#define SESSIONSTATSRECORD_H

#include <QDate>
#include <QVector>
#include <QList>

struct SessionStatsRecord{
    SessionStatsRecord(const uint session = 0) : session(session) {}

    uint  session;
    QDate date;
    int   duration = 0;
};

class SessionStatsRecordList : public QVector<SessionStatsRecord>
{
public:
    QList<uint> find(const uint session) const{
        QList<uint> idxs;
        for (int i=0; i<size(); i++)
            if (at(i).session == session)
                idxs.push_back(i);
        return idxs;
    }
};

#endif // SESSIONSTATSRECORD_H
