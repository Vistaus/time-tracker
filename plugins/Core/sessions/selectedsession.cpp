/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "selectedsession.h"

void SelectedSession::setSession(const Session &s){
    if (m_session.uid == s.uid)
        return;

    m_session = s;

    m_intervalModel->setList(m_session.intervals);

    emit uidChanged();
    emit projectChanged();
    emit descriptionChanged();
    emit startDateChanged();
    emit durationChanged();
}

void SelectedSession::setProject(const uint project){
    m_session.project = project;
    emit projectChanged();
}

void SelectedSession::setDescription(const QString &description){
    m_session.description = description;
    emit descriptionChanged();
}

void SelectedSession::setStartDate(const QDate &date){
    m_session.startDate = date;
    emit startDateChanged();
}


void SelectedSession::updateDayAt(const uint idx, const bool start, const int day){
    if (idx >= m_session.intervals.size())
        return;

    SessionInterval si = m_session.intervals.at(idx);
    if (start)
        si.startDay = day;
    else
        si.endDay = day;
    m_session.intervals.replace(idx,si);

    m_intervalModel->updateDayAt(idx,start,day);

    emit durationChanged();
}

void SelectedSession::updateHourAt(const uint idx, const bool start, const int hour){
    if (idx >= m_session.intervals.size())
        return;

    SessionInterval si = m_session.intervals.at(idx);
    if (start)
        si.startTime.setHMS(hour,si.startTime.minute(),0);
    else
        si.endTime.setHMS(hour,si.endTime.minute(),0);
    m_session.intervals.replace(idx,si);

    m_intervalModel->updateHourAt(idx,start,hour);

    emit durationChanged();
}

void SelectedSession::updateMinuteAt(const uint idx, const bool start, const int minute){
    if (idx >= m_session.intervals.size())
        return;

    SessionInterval si = m_session.intervals.at(idx);
    if (start)
        si.startTime.setHMS(si.startTime.hour(),minute,0);
    else
        si.endTime.setHMS(si.endTime.hour(),minute,0);
    m_session.intervals.replace(idx,si);

    m_intervalModel->updateMinuteAt(idx,start,minute);

    emit durationChanged();
}
