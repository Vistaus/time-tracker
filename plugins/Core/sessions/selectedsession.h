/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SELECTEDSESSION_H
#define SELECTEDSESSION_H

#include <QObject>
#include <QString>
#include <QDate>

#include "session.h"
#include "sessionintervalmodel.h"

class SelectedSession : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint uid READ uid NOTIFY uidChanged)
    Q_PROPERTY(uint project READ project NOTIFY projectChanged)
    Q_PROPERTY(QString description READ description NOTIFY descriptionChanged)
    Q_PROPERTY(QDate startDate READ startDate NOTIFY startDateChanged)
    Q_PROPERTY(int duration READ duration NOTIFY durationChanged)

    Q_PROPERTY(SessionIntervalModel *intervalModel READ intervalModel CONSTANT)
public:
    explicit SelectedSession(QObject *parent = nullptr)
        : QObject(parent), m_intervalModel(new SessionIntervalModel){}


    /* ----------------------- *
     * ---- QML interface ---- */
    inline uint uid() const {return m_session.uid;}
    inline uint project() const {return m_session.project;}
    inline QString description() const {return m_session.description;}
    inline QDate startDate() const {return m_session.startDate;}
    inline int duration() const {return m_session.duration();}

    inline SessionIntervalModel *intervalModel() const {return m_intervalModel;}


    /* ----------------------- *
     * ---- C++ interface ---- */
    inline const Session &session() const {return m_session;}

    void setSession(const Session &s);
    void setProject(const uint project);
    void setDescription(const QString &description);
    void setStartDate(const QDate &date);

    void updateDayAt(const uint idx, const bool start, const int day);
    void updateHourAt(const uint idx, const bool start, const int hour);
    void updateMinuteAt(const uint idx, const bool start, const int minute);

signals:
    void uidChanged();
    void projectChanged();
    void descriptionChanged();
    void startDateChanged();
    void durationChanged();

private:
    Session m_session;

    SessionIntervalModel *m_intervalModel;
};

#endif // SELECTEDSESSION_H
