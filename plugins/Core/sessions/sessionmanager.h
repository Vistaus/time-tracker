/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H

#include <QObject>
#include <QDebug>

#include "session.h"
#include "sessiondatabase.h"
#include "livesessiondatabase.h"
#include "dailysessionsmodel.h"
#include "sessionintervalmodel.h"
#include "selectedsession.h"

class SessionManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(DailySessionsModel *dailyModel READ dailyModel CONSTANT)

    Q_PROPERTY(SessionIntervalModel *sessionIntervalEditModel READ sessionIntervalEditModel CONSTANT)

    // Live session
    Q_PROPERTY(SessionIntervalModel *liveSessionIntervalModel READ liveSessionIntervalModel CONSTANT)
    Q_PROPERTY(bool hasLiveSession READ hasLiveSession NOTIFY hasLiveSessionChanged)
    Q_PROPERTY(bool liveSessionIsRunning READ liveSessionIsRunning NOTIFY liveSessionIsRunningChanged)

    // Selected Session
    Q_PROPERTY(SelectedSession *selected READ selected CONSTANT)

public:
    explicit SessionManager(QObject *parent = nullptr);

    /* ----------------------- *
     * ---- QML interface ---- */
    inline DailySessionsModel   *dailyModel()               const {return m_dailyModel;}
    inline SessionIntervalModel *sessionIntervalEditModel() const {return m_sessionIntervalEditModel;}
    inline SessionIntervalModel *liveSessionIntervalModel() const {return m_liveSessionIntervalModel;}
    inline bool hasLiveSession()       const {return m_liveSessionIntervalModel->count() > 0;}
    inline bool liveSessionIsRunning() const {return m_liveSessionIsRunning;}
    inline SelectedSession *selected() const {return m_selected;}

    Q_INVOKABLE void startLiveSession();
    Q_INVOKABLE void discardLiveSession();
    Q_INVOKABLE void pauseLiveSession();
    Q_INVOKABLE void continueLiveSession();
    Q_INVOKABLE void finishLiveSession(const uint project, const QString &description);
    Q_INVOKABLE void updateLiveSessionDayStamp(const uint idx, const bool start, const int newDay);
    Q_INVOKABLE void updateLiveSessionHourStamp(const uint idx, const bool start, const int newHour);
    Q_INVOKABLE void updateLiveSessionMinuteStamp(const uint idx, const bool start, const int newMinute);

    Q_INVOKABLE void add(const QString &description, const uint project);
    Q_INVOKABLE void remove(const uint uid);

    Q_INVOKABLE void selectSession(const uint uid);
    Q_INVOKABLE void setSelectedProject(const uint project, const QString &description);
    Q_INVOKABLE void setSelectedStartDate(const QDate &date);
    Q_INVOKABLE void updateSelectedDayStamp(const uint idx, const bool start, const int newDay);
    Q_INVOKABLE void updateSelectedHourStamp(const uint idx, const bool start, const int newHour);
    Q_INVOKABLE void updateSelectedMinuteStamp(const uint idx, const bool start, const int newMinute);

    /* ----------------------- *
     * ---- C++ interface ---- */
    inline const SessionList &allSessions() const {return m_allSessions;}

    Session getSession(const uint uid) const;

public slots:
    void onProjectRemoved(const uint uid);

signals:
    void hasLiveSessionChanged();
    void liveSessionIsRunningChanged();

    void sessionAdded(const uint uid);
    void sessionRemoved(const uint uid);
    void sessionUpdated(const uint uid);

private:
    SessionList m_allSessions;
    DailySessionsModel *m_dailyModel;
    SessionIntervalModel *m_sessionIntervalEditModel;

    SessionIntervalModel *m_liveSessionIntervalModel;
    bool m_liveSessionIsRunning = false;

    SelectedSession *m_selected;

    SessionDatabase m_db;
    LiveSessionDatabase m_dbLive;
};

#endif // SESSIONMANAGER_H
