/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "sessionintervalmodel.h"

int SessionIntervalModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant SessionIntervalModel::data(const QModelIndex &index, int role) const{
    const int row = index.row();
    if (row >= m_list.size())
        return QVariant();

    switch (role) {
    case UIDROLE:
        return QVariant(m_list.at(row).uid);

    case STARTVALIDROLE:
        return QVariant(m_list.at(row).startTime.isValid());
    case STARTDAYROLE:
        return QVariant(m_list.at(row).startDay);
    case STARTHOURROLE:
        return QVariant(m_list.at(row).startTime.hour());
    case STARTMINUTEROLE:
        return QVariant(m_list.at(row).startTime.minute());

    case ENDVALIDROLE:
        return QVariant(m_list.at(row).endTime.isValid());
    case ENDDAYROLE:
        return QVariant(m_list.at(row).endDay);
    case ENDHOURROLE:
        return QVariant(m_list.at(row).endTime.hour());
    case ENDMINUTEROLE:
        return QVariant(m_list.at(row).endTime.minute());

    case DURATIONROLE:
        return QVariant(m_list.at(row).duration());

    case MINSTARTVALIDROLE:
        return (row > 0) ? QVariant(m_list.at(row-1).endTime.isValid())
                         : QVariant(false);
    case MINSTARTDAYROLE:
        return (row > 0) ? QVariant(m_list.at(row-1).endDay)
                         : QVariant(-1);
    case MINSTARTHOURROLE:
        return (row > 0) ? QVariant(m_list.at(row-1).endTime.hour())
                         : QVariant(0);
    case MINSTARTMINUTEROLE:
        return (row > 0) ? QVariant(m_list.at(row-1).endTime.minute())
                         : QVariant(0);

    case MAXENDVALIDROLE:
        return (row < m_list.size()-1) ? QVariant(m_list.at(row+1).startTime.isValid())
                                       : QVariant(false);
    case MAXENDDAYROLE:
        return (row < m_list.size()-1) ? QVariant(m_list.at(row+1).startDay)
                                       : QVariant(m_list.at(row+1).startDay + 1);
    case MAXENDHOURROLE:
        return (row < m_list.size()-1) ? QVariant(m_list.at(row+1).startTime.hour())
                                       : QVariant(23);
    case MAXENDMINUTEROLE:
        return (row < m_list.size()-1) ? QVariant(m_list.at(row+1).startTime.minute())
                                       : QVariant(59);

    case PAUSEROLE:
        return (row > 0) ? QVariant(m_list.at(row).startSecs() - m_list.at(row-1).endSecs())
                         : QVariant(0);
    }
    return QVariant();
}

QHash<int, QByteArray> SessionIntervalModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[UIDROLE]      = "siUid";
    names[STARTVALIDROLE]  = "siStartValid";
    names[STARTDAYROLE]    = "siStartDay";
    names[STARTHOURROLE]   = "siStartHour";
    names[STARTMINUTEROLE] = "siStartMinute";
    names[ENDVALIDROLE]  = "siEndValid";
    names[ENDDAYROLE]    = "siEndDay";
    names[ENDHOURROLE]   = "siEndHour";
    names[ENDMINUTEROLE] = "siEndMinute";
    names[DURATIONROLE] = "siDuration";
    names[MINSTARTVALIDROLE]  = "siMinStartValid";
    names[MINSTARTDAYROLE]    = "siMinStartDay";
    names[MINSTARTHOURROLE]   = "siMinStartHour";
    names[MINSTARTMINUTEROLE] = "siMinStartMinute";
    names[MAXENDVALIDROLE]  = "siMaxEndValid";
    names[MAXENDDAYROLE]    = "siMaxEndDay";
    names[MAXENDHOURROLE]   = "siMaxEndHour";
    names[MAXENDMINUTEROLE] = "siMaxEndMinute";
    names[PAUSEROLE] = "siPause";
    return names;
}


void SessionIntervalModel::insertInterval(const int duration, const int pause){
    SessionInterval si;
    int startSecs = count() > 0 ? m_list.back().endSecs() + pause
                                : pause;
    int endSecs   = startSecs + duration;


    // round to closest full minute
    const int secsFromFullMinute = startSecs % 60;
    startSecs -= secsFromFullMinute;
    if (secsFromFullMinute > 29)
        startSecs += 60;


    // set start time stamp
    si.startDay = (startSecs >= 0) ? startSecs/86400
                                   : startSecs/86400 - 1;
    startSecs -= si.startDay*86400;

    const int startHours = startSecs/3600;
    startSecs -= startHours*3600;
    const int startMinutes = startSecs/60;
    si.startTime = QTime(startHours,startMinutes);


    // set end time stamp
    si.endDay = endSecs/86400;
    endSecs -= si.endDay*86400;

    const int endHours = endSecs/3600;
    endSecs -= endHours*3600;
    const int endMinutes = endSecs/60;
    si.endTime = QTime(endHours,endMinutes);


    // add interval to model
    const int idx = m_list.size();
    beginInsertRows(QModelIndex(),idx,idx);
    m_list.insert(idx,si);
    endInsertRows();
    emit countChanged();
}

void SessionIntervalModel::updateDayAt(const uint idx, const bool start, const int day){
    if (idx >= count())
        return;

    if (start){
        m_list[idx].startDay = day;
        emit dataChanged(index(idx),index(idx),{STARTDAYROLE,DURATIONROLE,PAUSEROLE});
        if (idx > 0)
            emit dataChanged(index(idx-1),index(idx-1),{MAXENDDAYROLE});
    } else {
        m_list[idx].endDay = day;
        emit dataChanged(index(idx),index(idx),{ENDDAYROLE,DURATIONROLE});
        if (idx < m_list.size()-1)
            emit dataChanged(index(idx+1),index(idx+1),{MINSTARTDAYROLE,PAUSEROLE});
    }
}

void SessionIntervalModel::updateHourAt(const uint idx, const bool start, const int hour){
    if (idx >= count())
        return;

    if (start){
        m_list[idx].startTime.setHMS(hour,m_list.at(idx).startTime.minute(),0);
        emit dataChanged(index(idx),index(idx),{STARTHOURROLE,DURATIONROLE,PAUSEROLE});
        if (idx > 0)
            emit dataChanged(index(idx-1),index(idx-1),{MAXENDHOURROLE});
    } else {
        m_list[idx].endTime.setHMS(hour,m_list.at(idx).endTime.minute(),0);
        emit dataChanged(index(idx),index(idx),{ENDHOURROLE,DURATIONROLE});
        if (idx < m_list.size()-1)
            emit dataChanged(index(idx+1),index(idx+1),{MINSTARTHOURROLE,PAUSEROLE});
    }
}

void SessionIntervalModel::updateMinuteAt(const uint idx, const bool start, const int minute){
    if (idx >= count())
        return;

    if (start){
        m_list[idx].startTime.setHMS(m_list.at(idx).startTime.hour(),minute,0);
        emit dataChanged(index(idx),index(idx),{STARTMINUTEROLE,DURATIONROLE,PAUSEROLE});
        if (idx > 0)
            emit dataChanged(index(idx-1),index(idx-1),{MAXENDMINUTEROLE});
    } else {
        m_list[idx].endTime.setHMS(m_list.at(idx).endTime.hour(),minute,0);
        emit dataChanged(index(idx),index(idx),{ENDMINUTEROLE,DURATIONROLE});
        if (idx < m_list.size()-1)
            emit dataChanged(index(idx+1),index(idx+1),{MINSTARTMINUTEROLE,PAUSEROLE});
    }
}

void SessionIntervalModel::removeIntervalAt(const uint idx){
    if (idx >= count())
        return;

    beginRemoveRows(QModelIndex(),idx,idx);
    m_list.removeAt(idx);
    endRemoveRows();

    emit countChanged();

    if (idx > 0)
        emit dataChanged(index(idx-1),index(idx-1),{MAXENDDAYROLE,MAXENDHOURROLE,MAXENDMINUTEROLE});

    if (idx < count())
        emit dataChanged(index(idx),index(idx),{MINSTARTDAYROLE,MINSTARTHOURROLE,MINSTARTMINUTEROLE,PAUSEROLE});
}

void SessionIntervalModel::clear(){
    if (m_list.size() == 0)
        return;

    beginResetModel();
    m_list.clear();
    endResetModel();

    emit countChanged();

    setStartDate(QDate::currentDate());
}

bool SessionIntervalModel::isFinished() const{
    return m_list.size() == 0 || m_list.back().endTime.isValid();
}

void SessionIntervalModel::insertInterval(const int startDay, const QTime &startTime, const int endDay, const QTime &endTime, const uint uid){
    SessionInterval si;
    si.startDay  = startDay;
    si.startTime = startTime;
    si.endDay    = endDay;
    si.endTime   = endTime;
    si.uid = uid;

    const int startSecs = si.startSecs();
    int idx = 0;
    while (idx < m_list.size() && startSecs > m_list.at(idx).startSecs())
        idx++;

    beginInsertRows(QModelIndex(),idx,idx);
    m_list.insert(idx,si);
    endInsertRows();

    emit countChanged();

    if (idx > 0)
        emit dataChanged(index(idx-1),index(idx-1),{MAXENDDAYROLE,MAXENDHOURROLE,MAXENDMINUTEROLE});

    if (idx < count()-1)
        emit dataChanged(index(idx+1),index(idx+1),{MINSTARTDAYROLE,MINSTARTHOURROLE,MINSTARTMINUTEROLE,PAUSEROLE});
}

SessionInterval &SessionIntervalModel::startInterval(){
    SessionInterval si;

    qint64 startSecs = QDateTime(m_startDate).msecsTo(QDateTime::currentDateTime()) / 1000;

    // round to closest full minute
    const int secsFromFullMinute = startSecs % 60;
    startSecs -= secsFromFullMinute;
    if (secsFromFullMinute > 29)
        startSecs += 60;

    si.startDay = (startSecs >= 0) ? startSecs/86400
                                   : startSecs/86400 - 1;
    startSecs -= si.startDay*86400;

    const int startHours = startSecs/3600;
    startSecs -= startHours*3600;
    const int startMinutes = startSecs/60;
    si.startTime = QTime(startHours,startMinutes);

    si.endTime = QTime();

    const int idx = m_list.size();
    beginInsertRows(QModelIndex(),idx,idx);
    m_list.insert(idx,si);
    endInsertRows();

    emit countChanged();

    if (idx > 0)
        emit dataChanged(index(idx-1),index(idx-1),{MAXENDDAYROLE,MAXENDHOURROLE,MAXENDMINUTEROLE});

    return m_list[idx];
}

const SessionInterval &SessionIntervalModel::stopInterval(){
    const int idx = m_list.size()-1;

    qint64 endSecs = QDateTime(m_startDate).msecsTo(QDateTime::currentDateTime()) / 1000;

    // round to closest full minute
    const int secsFromFullMinute = endSecs % 60;
    endSecs -= secsFromFullMinute;
    if (secsFromFullMinute > 29)
        endSecs += 60;

    m_list[idx].endDay = (endSecs >= 0) ? endSecs/86400
                                        : endSecs/86400 - 1;
    endSecs -= m_list[idx].endDay*86400;

    const int endHours = endSecs/3600;
    endSecs -= endHours*3600;
    const int endMinutes = endSecs/60;
    m_list[idx].endTime = QTime(endHours,endMinutes);

    emit dataChanged(index(idx),index(idx));

    return m_list.at(idx);
}

void SessionIntervalModel::setList(const SessionIntervalList &list){
    beginResetModel();
    m_list = list;
    endResetModel();
    emit countChanged();
}



void SessionIntervalModel::setStartDate(const QDate &startDate){
    if (m_startDate == startDate)
        return;

    m_startDate = startDate;
    emit startDateChanged();
}
