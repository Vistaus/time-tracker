/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "sessionmanager.h"

SessionManager::SessionManager(QObject *parent)
    : QObject(parent)
    , m_dailyModel(new DailySessionsModel)
    , m_sessionIntervalEditModel(new SessionIntervalModel)
    , m_liveSessionIntervalModel(new SessionIntervalModel)
    , m_selected(new SelectedSession){

    m_db.read(m_allSessions);

    for (const Session &s : m_allSessions)
        m_dailyModel->addSession(s);

    m_dbLive.read(m_liveSessionIntervalModel);
    m_liveSessionIsRunning = !m_liveSessionIntervalModel->isFinished();

    qInfo() << "[INIT] SessionManager created.";
}

void SessionManager::startLiveSession(){
    m_liveSessionIntervalModel->setStartDate(QDate::currentDate());

    m_dbLive.insert(m_liveSessionIntervalModel->startInterval());
    emit hasLiveSessionChanged();

    m_liveSessionIsRunning = true;
    emit liveSessionIsRunningChanged();
}

void SessionManager::discardLiveSession(){
    m_liveSessionIntervalModel->clear();
    m_dbLive.clear();
    emit hasLiveSessionChanged();

    m_liveSessionIsRunning = false;
    emit liveSessionIsRunningChanged();
}

void SessionManager::pauseLiveSession(){
    // ensure, the last start time stamp is < now()
    const QDateTime now = QDateTime::currentDateTime();
    while (m_liveSessionIntervalModel->count()>0){
        const QDateTime startTime = QDateTime(m_liveSessionIntervalModel->startDate())
                                      .addSecs(m_liveSessionIntervalModel->list().back().startSecs());
        if (startTime > now)
            m_liveSessionIntervalModel->removeIntervalAt(m_liveSessionIntervalModel->count()-1);
        else
            break;
    }
    // if all live sessions where removed, insert a new one
    if (m_liveSessionIntervalModel->count() == 0)
        m_dbLive.insert(m_liveSessionIntervalModel->startInterval());

    m_dbLive.update(m_liveSessionIntervalModel->stopInterval());

    m_liveSessionIsRunning = false;
    emit liveSessionIsRunningChanged();
}

void SessionManager::continueLiveSession(){
    m_dbLive.insert(m_liveSessionIntervalModel->startInterval());

    m_liveSessionIsRunning = true;
    emit liveSessionIsRunningChanged();
}

void SessionManager::finishLiveSession(const uint project, const QString &description){
    if (project == 0){
        qWarning() << "SessionManager::finishLiveSession() - got project uid=0. Discard this session.";
        return;
    }

    if (m_liveSessionIsRunning)
        pauseLiveSession();

    Session s;
    s.project = project;
    s.description = description;
    s.startDate = m_liveSessionIntervalModel->startDate();

    for (const SessionInterval &si : m_liveSessionIntervalModel->list())
        s.addInterval(si);

    if (m_db.insert(s)){
        m_allSessions.append(s);
        m_dailyModel->addSession(s);

        emit sessionAdded(s.uid);
    }

    m_liveSessionIntervalModel->clear();
    m_dbLive.clear();
    emit hasLiveSessionChanged();
}

void SessionManager::updateLiveSessionDayStamp(const uint idx, const bool start, const int newDay){
    if (idx >= m_liveSessionIntervalModel->count())
        return;

    m_liveSessionIntervalModel->updateDayAt(idx,start,newDay);
    m_dbLive.update(m_liveSessionIntervalModel->list().at(idx));
}

void SessionManager::updateLiveSessionHourStamp(const uint idx, const bool start, const int newHour){
    if (idx >= m_liveSessionIntervalModel->count())
        return;

    m_liveSessionIntervalModel->updateHourAt(idx,start,newHour);
    m_dbLive.update(m_liveSessionIntervalModel->list().at(idx));
}

void SessionManager::updateLiveSessionMinuteStamp(const uint idx, const bool start, const int newMinute){
    if (idx >= m_liveSessionIntervalModel->count())
        return;

    m_liveSessionIntervalModel->updateMinuteAt(idx,start,newMinute);
    m_dbLive.update(m_liveSessionIntervalModel->list().at(idx));
}

void SessionManager::add(const QString &description, const uint project){
    if (project == 0){
        qWarning() << "SessionManager::add() - got project uid=0. Discard this session.";
        return;
    }

    Session s;
    s.project = project;
    s.description = description;
    s.startDate = m_sessionIntervalEditModel->startDate();

    for (const SessionInterval &si : m_sessionIntervalEditModel->list())
        s.addInterval(si);

    if (m_db.insert(s)){
        m_allSessions.append(s);
        m_dailyModel->addSession(s);

        emit sessionAdded(s.uid);
    }
    m_sessionIntervalEditModel->clear();
}

void SessionManager::remove(const uint uid){
    const uint idx = m_allSessions.find(uid);
    if (idx < 0)
        return;

    if (m_db.remove(uid)){
        m_allSessions.removeAt(idx);
        m_dailyModel->removeSession(uid);

        emit sessionRemoved(uid);
    }
}

void SessionManager::selectSession(const uint uid){
    const int idx = m_allSessions.find(uid);
    if (idx < 0)
        return;

    m_selected->setSession(m_allSessions.at(idx));
}

void SessionManager::setSelectedProject(const uint project, const QString &description){
    const int idx = m_allSessions.find(m_selected->uid());
    if (idx < 0)
        return;

    Session session = m_allSessions.at(idx);
    session.project = project;
    session.description = description;

    if (m_db.updateSession(session)){
        m_allSessions.replace(idx,session);
        m_dailyModel->updateSession(session);
        m_selected->setProject(project);
        m_selected->setDescription(description);

        emit sessionUpdated(m_selected->uid());
    }
}

void SessionManager::setSelectedStartDate(const QDate &date){
    const int idx = m_allSessions.find(m_selected->uid());
    if (idx < 0)
        return;

    Session session = m_allSessions.at(idx);
    session.startDate = date;

    if (m_db.updateSession(session)){
        m_allSessions.replace(idx,session);
        m_dailyModel->updateSession(session);
        m_selected->setStartDate(date);

        emit sessionUpdated(m_selected->uid());
    }
}

void SessionManager::updateSelectedDayStamp(const uint idx, const bool start, const int newDay){
    if (idx >= m_selected->intervalModel()->count())
        return;

    m_selected->updateDayAt(idx,start,newDay);
    if (m_db.updateInterval(m_selected->intervalModel()->list().at(idx))){
        const Session &s = m_selected->session();
        m_allSessions.replace(idx,s);
        m_dailyModel->updateSession(s);

        emit sessionUpdated(m_selected->uid());
    }
}

void SessionManager::updateSelectedHourStamp(const uint idx, const bool start, const int newHour){
    if (idx >= m_selected->intervalModel()->count())
        return;

    m_selected->updateHourAt(idx,start,newHour);
    if (m_db.updateInterval(m_selected->intervalModel()->list().at(idx))){
        const Session &s = m_selected->session();
        m_allSessions.replace(idx,s);
        m_dailyModel->updateSession(s);

        emit sessionUpdated(m_selected->uid());
    }
}

void SessionManager::updateSelectedMinuteStamp(const uint idx, const bool start, const int newMinute){
    if (idx >= m_selected->intervalModel()->count())
        return;

    m_selected->updateMinuteAt(idx,start,newMinute);
    if (m_db.updateInterval(m_selected->intervalModel()->list().at(idx))){
        const Session &s = m_selected->session();
        m_allSessions.replace(idx,s);
        m_dailyModel->updateSession(s);

        emit sessionUpdated(m_selected->uid());
    }
}

Session SessionManager::getSession(const uint uid) const{
    const int idx = m_allSessions.find(uid);
    return idx >= 0 ? m_allSessions.at(idx) : Session();
}

void SessionManager::onProjectRemoved(const uint uid){
    QList<uint> removeIds;
    for (const Session &s : m_allSessions)
        if (s.project == uid)
            removeIds.push_back(s.uid);

    for (uint sessionUid : removeIds)
        remove(sessionUid);
}
