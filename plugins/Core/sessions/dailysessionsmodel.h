/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef DAILYSESSIONSMODEL_H
#define DAILYSESSIONSMODEL_H

#include <QAbstractListModel>
#include <QDate>
#include <QVector>

#include "session.h"
#include "sessionmodel.h"

struct DailySessionsModelElement{
    DailySessionsModelElement(const QDate &date = QDate(), SessionModel *model = nullptr)
        : date(date), model(model) {}

    QDate date;
    SessionModel *model;
};

inline bool operator==(const DailySessionsModelElement &e1, const DailySessionsModelElement &e2){
    return e1.date == e2.date;
}

class DailySessionsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
public:
    enum Roles{
        DATEROLE = Qt::UserRole,
        MODELROLE
    };

    explicit DailySessionsModel(QObject *parent = nullptr);

    /* ----------------------- *
     * ---- QML interface ---- */
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    inline uint count() const {return m_list.size();}

    /* ----------------------- *
     * ---- C++ interface ---- */
    void addSession(const Session &s);
    void removeSession(const uint uid);
    void updateSession(const Session &s);

signals:
    void countChanged();

private:
    QVector<DailySessionsModelElement> m_list;
};

#endif // DAILYSESSIONSMODEL_H
