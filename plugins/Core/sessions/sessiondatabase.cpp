/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "sessiondatabase.h"

SessionDatabase::SessionDatabase(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",m_db_name);
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + m_db_name + ".sqlite");
    if (!db.open()){
        qCritical() << "[ERROR] SessionDatabase::SessionDatabase() - could not open database: " << db.lastError().text();
        return;
    }

    QSqlQuery querySessionsTable(db);
    querySessionsTable.prepare("CREATE TABLE sessions ("
                               "uid INTEGER PRIMARY KEY,"
                               "project INTEGER,"
                               "startDate TEXT,"
                               "description TEXT)"
                               );

    if (querySessionsTable.exec())
        qInfo() << "[INFO] SessionDatabase: table for sessions created";


    QSqlQuery querySessionIntervalsTable(db);
    querySessionIntervalsTable.prepare("CREATE TABLE intervals ("
                                       "uid INTEGER PRIMARY KEY,"
                                       "session INTEGER,"
                                       "startDay INTEGER,"
                                       "startTime INTEGER,"
                                       "endDay INTEGER,"
                                       "endTime INTEGER)"
                                       );

    if (querySessionIntervalsTable.exec())
        qInfo() << "[INFO] SessionDatabase: table for session intervals created";

    db.close();
}

bool SessionDatabase::read(SessionList &list){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] SessionDatabase::read() - could not open database: " << db.lastError().text();
        return false;
    }

    // read sessions
    QSqlQuery query(db);
    query.prepare("SELECT * FROM sessions");

    if (!query.exec()){
        qCritical() << "[ERROR] SessionDatabase::read() - could not select sessions: " << query.lastError().text();
        db.close();
        return false;
    }

    list.clear();
    while (query.next()){
        try{
            Session s;
            s.uid         = query.value("uid").toUInt();
            s.project     = query.value("project").toUInt();
            s.description = query.value("description").toString();
            s.startDate   = QDate::fromString(query.value("startDate").toString(),"yyyy/MM/dd");

            list.append(s);
        } catch (std::exception e) {
            qWarning() << "[ERROR] SessionDatabase::read() - could not parse a session: " << e.what() << " - Ignore and continue with next.";
        }
    }

    // read intervals
    QSqlQuery queryIntervals(db);
    queryIntervals.prepare("SELECT * FROM intervals");


    if (!queryIntervals.exec()){
        qCritical() << "[ERROR] SessionDatabase::read() - could not select intervals: " << query.lastError().text();
        db.close();
        return false;
    }

    while (queryIntervals.next()){
        try{
            SessionInterval si;
            si.uid       = queryIntervals.value("uid").toUInt();
            si.startDay  = queryIntervals.value("startDay").toInt();
            const int startSecs = queryIntervals.value("startTime").toInt();
            si.startTime = startSecs >= 0 ? QTime::fromMSecsSinceStartOfDay(startSecs) : QTime();
            si.endDay    = queryIntervals.value("endDay").toInt();
            const int endSecs = queryIntervals.value("endTime").toInt();
            si.endTime   = endSecs >= 0 ? QTime::fromMSecsSinceStartOfDay(endSecs) : QTime();

            const uint sessionId = queryIntervals.value("session").toUInt();

            const int sessionIdx = list.find(sessionId);
            if (sessionIdx > -1){
                list[sessionIdx].addInterval(si);
            } else {
                qWarning() << "[WARNING] SessionDatabase::read() - found an interval (uid="+QString::number(si.uid)+") linked to not existing session with uid="+QString::number(sessionId)+". This one will be ignored.";
            }

        } catch (std::exception e) {
            qWarning() << "[ERROR] SessionDatabase::read() - could not parse an interval: " << e.what() << " - Ignore and continue with next.";
        }
    }

    // remove invalid sessions
    for (int i=list.size()-1; i>-1; i--){
        if (!list.at(i).isValid()){
            const Session &s = list.at(i);
            qWarning() << "[WARNING] SessionDatabase::read() - remove invalid session (uid="+QString::number(s.uid)+"): project="+QString::number(s.project)+", number of intervals="+QString::number(s.intervals.size());
            list.removeAt(i);
        }
    }

    db.close();
    return true;
}

bool SessionDatabase::insert(Session &s){
    if (s.intervals.size() == 0)
        return false;

    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] SessionDatabase::insert() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("INSERT INTO sessions (project,startDate,description) VALUES"
                  "("+QString::number(s.project)+
                  ",'"+s.startDate.toString("yyyy/MM/dd")+"'"
                  ",'"+s.description+"'"
                  ")"
                  );

    if (!query.exec()){
        qCritical() << "[ERROR] SessionDatabase::insert() - could not insert session: " << query.lastError().text();
        db.close();
        return false;
    }
    s.uid = query.lastInsertId().toUInt();

    for (const SessionInterval &si : s.intervals){
        QSqlQuery queryInterval(db);
        queryInterval.prepare("INSERT INTO intervals (session,startDay,startTime,endDay,endTime) VALUES"
                              "("+QString::number(s.uid)+
                              ","+QString::number(si.startDay)+
                              ","+QString::number(si.startTime.isValid() ? si.startTime.msecsSinceStartOfDay() : -1)+
                              ","+QString::number(si.endDay)+
                              ","+QString::number(si.endTime.isValid() ? si.endTime.msecsSinceStartOfDay() : -1)+
                              ")"
                              );
        if (!queryInterval.exec())
            qCritical() << "[ERROR] SessionDatabase::insert() - could not insert an interval: " << query.lastError().text();
    }

    db.close();
    return true;
}

bool SessionDatabase::remove(const uint uid){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] SessionDatabase::remove() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("DELETE FROM sessions WHERE uid="+QString::number(uid));

    if (!query.exec()){
        qCritical() << "[ERROR] SessionDatabase::remove() - could not delete session: " << query.lastError().text();
        db.close();
        return false;
    }

    QSqlQuery query1(db);
    query1.prepare("DELETE FROM intervals WHERE session="+QString::number(uid));

    if (!query1.exec())
        qWarning() << "[ERROR] SessionDatabase::remove() - could not delete intervals: " << query1.lastError().text();

    db.close();
    return true;
}

bool SessionDatabase::updateSession(const Session &s){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] SessionDatabase::updateStartDate() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("UPDATE sessions SET"
                  " project="+QString::number(s.project)+
                  ",description='"+s.description+"'"
                  ",startDate='"+s.startDate.toString("yyyy/MM/dd")+"'"
                  " WHERE uid="+QString::number(s.uid));

    if (!query.exec()){
        qCritical() << "[ERROR] SessionDatabase::updateStartDate() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}

bool SessionDatabase::updateInterval(const SessionInterval &si){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] SessionDatabase::updateInterval() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("UPDATE intervals SET"
                  " startDay="+QString::number(si.startDay)+
                  ",startTime="+QString::number(si.startTime.isValid() ? si.startTime.msecsSinceStartOfDay() : -1)+
                  ",endDay="+QString::number(si.endDay)+
                  ",endTime="+QString::number(si.endTime.isValid() ? si.endTime.msecsSinceStartOfDay() : -1)+
                  " WHERE uid="+QString::number(si.uid));

    if (!query.exec()){
        qCritical() << "[ERROR] SessionDatabase::updateInterval() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}


