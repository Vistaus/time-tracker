/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SESSIONMODEL_H
#define SESSIONMODEL_H

#include <QAbstractListModel>
#include <QVector>

#include "session.h"

class SessionModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
public:
    enum Roles{
        UIDROLE = Qt::UserRole,
        PROJECTROLE,
        DESCRIPTIONROLE,
        DURATIONROLE,
        STARTROLE,
        ENDROLE,
        STARTDAYOFFSETROLE,
        ENDDAYOFFSETROLE
    };

    explicit SessionModel(QObject *parent = nullptr);

    /* ----------------------- *
     * ---- QML interface ---- */
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    inline uint count() const {return m_list.size();}

    /* ----------------------- *
     * ---- C++ interface ---- */
    int find(const uint uid) const;

    void add(const Session &s);
    void update(const Session &s);
    bool remove(const uint uid);

signals:
    void countChanged();


private:
    QVector<Session> m_list;
};

#endif // SESSIONMODEL_H
