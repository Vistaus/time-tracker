/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef LIVESESSIONDATABASE_H
#define LIVESESSIONDATABASE_H

#include <QDebug>
#include <QString>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>

#include "session.h"
#include "sessionintervalmodel.h"

class LiveSessionDatabase
{
public:
    LiveSessionDatabase();

    bool read(SessionIntervalModel *model);
    bool insert(SessionInterval &si);
    bool update(const SessionInterval &si);
    bool clear();

private:
    const QString m_db_name = "livesession";
};

#endif // LIVESESSIONDATABASE_H
