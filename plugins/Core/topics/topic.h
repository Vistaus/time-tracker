/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef TOPIC_H
#define TOPIC_H

#include <QString>
#include <QColor>
#include <QDate>
#include <QVector>

#include "types/dailyvaluemodel.h"

const qreal topicColorNumber = 12.;
const qreal topicColorSaturation = 1.0;
const qreal topicColorLightness  = 0.5;

struct Topic{
    enum Status{
        ACTIVE,
        INACTIVE
    };

    Topic() : dailyDurations(new DailyValueModel), dailyDurations30days(new DailyValueModel), dailyDurations7days(new DailyValueModel){
        dailyDurations30days->setTimeRange(QDate::currentDate().addDays(-29),QDate::currentDate());
        dailyDurations7days->setTimeRange(QDate::currentDate().addDays(-6),QDate::currentDate());
    }

    uint    uid = 0;
    QString title;
    uint    colorHue = 0;
    QColor  color;
    uint    rank = 0;
    QDate   creationDate;
    Status  status = ACTIVE;

    DailyValueModel *dailyDurations;
    DailyValueModel *dailyDurations30days;
    DailyValueModel *dailyDurations7days;


    static QColor hueToColor(const uint hue){
        QColor c;
        c.setHslF(hue/topicColorNumber,topicColorSaturation,topicColorLightness);
        return c;
    }
};

inline bool operator==(const Topic &t1, const Topic &t2){
    return t1.uid == t2.uid;
}

class TopicList : public QVector<Topic>
{
public:
    int find(const uint uid) const{
        for (int i=0; i<size(); i++)
            if (at(i).uid == uid)
                return i;
        return -1;
    }
};

#endif // TOPIC_H
