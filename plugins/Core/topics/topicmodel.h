/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef TOPICMODEL_H
#define TOPICMODEL_H

#include <QAbstractListModel>

#include "topic.h"

class TopicModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
public:
    enum Roles{
        UIDROLE = Qt::UserRole,
        TITLEROLE,
        COLORROLE,
        COLORHUEROLE,
        RANKROLE,
        STATUSROLE,
        DAILYDURATIONROLE,
        DAILYDURATION30DAYSROLE,
        DAILYDURATION7DAYSROLE,
        CREATIONDATEROLE
    };

    explicit TopicModel(QObject *parent = nullptr);


    /* --------------------- *
     * --- QML interface --- */

    inline uint count() const {return m_list.size();}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE int indexOf(const uint uid) const;

    /* --------------------- *
     * --- C++ interface --- */
    Topic get(const uint idx) const;
    const Topic &at(const uint idx) const;

    void insert(const Topic &t);
    void update(const Topic &t);
    void removeTopic(const uint uid);
    void swap(const uint idx1, const uint idx2);

signals:
    void countChanged();

private:
    TopicList m_list;

};

#endif // TOPICMODEL_H
