/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef SELECTEDTOPIC_H
#define SELECTEDTOPIC_H

#include <QObject>
#include <QString>
#include <QColor>

#include <QDebug>

#include "topic.h"

class SelectedTopic : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint    uid   READ uid   NOTIFY uidChanged)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
    Q_PROPERTY(QColor  color READ color NOTIFY colorChanged)
    Q_PROPERTY(uint    colorHue READ colorHue NOTIFY colorHueChanged)
    Q_PROPERTY(uint    status READ status NOTIFY statusChanged)
public:
    explicit SelectedTopic(QObject *parent = nullptr);


    /* --------------------- *
     * --- QML interface --- */
    inline uint    uid()      const {return m_topic.uid;}
    inline QString title()    const {return m_topic.title;}
    inline QColor  color()    const {return m_topic.color;}
    inline uint    colorHue() const {return m_topic.colorHue;}
    inline uint    status()   const {return m_topic.status;}


    /* --------------------- *
     * --- C++ interface --- */
    void setTopic(const Topic &t);

signals:
    void uidChanged();
    void titleChanged();
    void colorChanged();
    void colorHueChanged();
    void statusChanged();

private:
    Topic m_topic;

};

#endif // SELECTEDTOPIC_H
