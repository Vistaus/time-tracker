/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef TOPICMANAGER_H
#define TOPICMANAGER_H

#include <QObject>
#include <QDebug>

#include "topicdatabase.h"
#include "topicmodel.h"
#include "selectedtopic.h"

class TopicManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TopicModel *activeModel READ activeModel CONSTANT)
    Q_PROPERTY(TopicModel *inactiveModel READ inactiveModel CONSTANT)
    Q_PROPERTY(SelectedTopic *selected READ selected CONSTANT)
public:
    explicit TopicManager(QObject *parent = nullptr);

    /* --------------------- *
     * --- QML interface --- */
    inline TopicModel *activeModel() const {return m_activeModel;}
    inline TopicModel *inactiveModel() const {return m_inactiveModel;}
    inline SelectedTopic *selected() const {return m_selected;}

    Q_INVOKABLE QString getTitle(const uint uid) const;
    Q_INVOKABLE QColor  getColor(const uint uid) const;

    Q_INVOKABLE QColor hueToColor(const uint hue) const;

    Q_INVOKABLE void add(const QString &title, const uint colorHue);
    Q_INVOKABLE void update(const uint uid, const QString &title, const uint colorHue);
    Q_INVOKABLE void remove(const uint uid);
    Q_INVOKABLE void swap(const uint idx1, const uint idx2, const uint modelNumber);
    Q_INVOKABLE void setStatus(const uint uid, const uint status);

    Q_INVOKABLE void select(const uint uid);

    /* --------------------- *
     * --- C++ interface --- */
    void addDailyDuration(const uint topic, const QDate &date, const qreal value);

signals:
    void topicAdded(const uint uid);
    void topicRemoved(const uint uid);
    void topicSelected(const uint uid);
    void topicColorChanged(const uint uid);

private:
    TopicDatabase m_db;

    TopicList m_list;
    TopicModel *m_activeModel;
    TopicModel *m_inactiveModel;

    SelectedTopic *m_selected;
};

#endif // TOPICMANAGER_H
