/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "topicmanager.h"

TopicManager::TopicManager(QObject *parent)
    : QObject(parent)
    , m_activeModel(new TopicModel), m_inactiveModel(new TopicModel){

    m_db.read(m_list);

    for (const Topic &t : m_list){
        if (t.status == Topic::ACTIVE)
            m_activeModel->insert(t);
        else if (t.status == Topic::INACTIVE)
            m_inactiveModel->insert(t);
    }

    m_selected = new SelectedTopic;

    qInfo() << "[INIT] TopicManager created.";
}

QString TopicManager::getTitle(const uint uid) const{
    const int idx = m_list.find(uid);
    return idx > -1 ? m_list.at(idx).title : QString();
}

QColor TopicManager::getColor(const uint uid) const{
    const int idx = m_list.find(uid);
    return idx > -1 ? m_list.at(idx).color : QColor();
}

QColor TopicManager::hueToColor(const uint hue) const{
    return Topic::hueToColor(hue);
}

void TopicManager::add(const QString &title, const uint colorHue){
    Topic t;
    t.title = title;
    t.colorHue = colorHue;
    t.color = Topic::hueToColor(t.colorHue);
    t.status = Topic::ACTIVE;

    if (m_db.insert(t)){
        m_list.append(t);
        m_activeModel->insert(t);
        emit topicAdded(t.uid);
    }
}

void TopicManager::update(const uint uid, const QString &title, uint colorHue){
    const int idx = m_list.find(uid);
    if (idx < 0)
        return;

    Topic t = m_list.at(idx);
    const uint oldColorHue = t.colorHue;

    t.title = title;
    t.colorHue = colorHue;
    t.color = Topic::hueToColor(t.colorHue);

    if (m_db.update(t)){
        m_list.replace(idx,t);
        m_activeModel->update(t);
        m_inactiveModel->update(t);

        if (m_selected->uid() == t.uid)
            m_selected->setTopic(t);

        if (oldColorHue != t.colorHue)
            emit topicColorChanged(t.uid);
    }
}

void TopicManager::remove(const uint uid){
    if (m_db.remove(uid)){
        m_activeModel->removeTopic(uid);
        m_inactiveModel->removeTopic(uid);

        const int idx = m_list.find(uid);
        if (idx > -1){
            Topic &t = m_list[idx];
            delete t.dailyDurations;
            delete t.dailyDurations30days;
            m_list.removeAt(idx);
        }
        emit topicRemoved(uid);
    }
}

void TopicManager::swap(const uint idx1, const uint idx2, const uint modelNumber){
    TopicModel *model = modelNumber == 0 ? m_activeModel
                                         : m_inactiveModel;
    if (idx1 >= model->count() || idx2 >= model->count())
        return;

    Topic topic1 = model->get(idx1);
    Topic topic2 = model->get(idx2);
    if (topic1.uid == 0 || topic2.uid == 0)
        return;

    // swap ranks
    uint rank1 = topic1.rank;
    topic1.rank = topic2.rank;
    topic2.rank = rank1;

    // update DB
    if (m_db.update(topic1) && m_db.update(topic2)){
        // update model
        model->update(topic1);
        model->update(topic2);
        model->swap(idx1,idx2);

        // update list
        const int idx1 = m_list.find(topic1.uid);
        if (idx1 >= 0)
            m_list[idx1].rank = topic1.rank;
        const int idx2 = m_list.find(topic2.uid);
        if (idx2 >= 0)
            m_list[idx2].rank = topic2.rank;
    }
}

void TopicManager::setStatus(const uint uid, const uint status){
    const int idx = m_list.find(uid);
    if (idx < 0)
        return;

    Topic topic = m_list.at(idx);
    const Topic::Status oldStatus = topic.status;

    switch (status) {
    case 0:
        topic.status = Topic::ACTIVE;
        break;
    case 1:
        topic.status = Topic::INACTIVE;
        break;
    }

    if (topic.status == oldStatus)
        return;

    if (m_db.update(topic)){
        // update list
        m_list.replace(idx,topic);

        // update models
        switch (topic.status) {
        case Topic::ACTIVE:
            m_inactiveModel->removeTopic(topic.uid);
            m_activeModel->insert(topic);
            break;
        case Topic::INACTIVE:
            m_activeModel->removeTopic(topic.uid);
            m_inactiveModel->insert(topic);
            break;
        }

        if (m_selected->uid() == topic.uid)
            m_selected->setTopic(topic);
    }
}

void TopicManager::select(const uint uid){
    const int idx = m_list.find(uid);
    if (idx <0)
        return;

    const uint oldSelectedUid = m_selected->uid();
    m_selected->setTopic(m_list.at(idx));

    if (oldSelectedUid != m_selected->uid())
        emit topicSelected(uid);
}

void TopicManager::addDailyDuration(const uint topic, const QDate &date, const qreal value){
    const int idx = m_list.find(topic);
    if (idx < 0)
        return;

    m_list.at(idx).dailyDurations->add(date,value);

    const int dayOffset = date.daysTo(QDate::currentDate());
    if (dayOffset < 0)
        return;

    if (dayOffset < 30){
        m_list.at(idx).dailyDurations30days->add(date,value);
        if (dayOffset < 7)
            m_list.at(idx).dailyDurations7days->add(date,value);
    }
}
