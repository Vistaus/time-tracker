/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "selectedtopic.h"

SelectedTopic::SelectedTopic(QObject *parent)
    : QObject(parent){

}

void SelectedTopic::setTopic(const Topic &t){
    m_topic = t;

    emit uidChanged();
    emit titleChanged();
    emit colorChanged();
    emit colorHueChanged();
    emit statusChanged();
}
