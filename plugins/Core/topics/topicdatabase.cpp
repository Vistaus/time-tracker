/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "topicdatabase.h"

TopicDatabase::TopicDatabase(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",m_db_name);
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + m_db_name + ".sqlite");
    if (!db.open()){
        qCritical() << "[ERROR] TopicDatabase::TopicDatabase() - could not open database: " << db.lastError().text();
        return;
    }

    QSqlQuery query(db);
    query.prepare("CREATE TABLE topics ("
                  "uid INTEGER PRIMARY KEY,"
                  "title TEXT DEFAULT '',"
                  "colorHue INTEGER DEFAULT 0," // values in [0,11]
                  "rank INTEGER DEFAULT 0,"
                  "status INTEGER DEFAULT 0,"
                  "creationDate TEXT DEFAULT '')"
                  );

    if (query.exec())
        qInfo() << "TopicDatabase: table for topics created";

    db.close();
}

bool TopicDatabase::read(TopicList &list){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] TopicDatabase::read() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("SELECT * FROM topics");

    if (!query.exec()){
        qCritical() << "[ERROR] TopicDatabase::read() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    list.clear();
    while (query.next()){
        try{
            Topic t;
            t.uid   = query.value("uid").toUInt();
            t.title = query.value("title").toString();
            t.colorHue = query.value("colorHue").toUInt();
            t.color = Topic::hueToColor(t.colorHue);
            t.rank  = query.value("rank").toUInt();
            t.status = (Topic::Status) query.value("status").toInt();
            t.creationDate  = QDate::fromString(query.value("creationDate").toString(),"yyyy/MM/dd");

            int i;
            for (i=0; i<list.size(); i++)
                if (list.at(i).rank > t.rank)
                    break;
            list.insert(i,t);
        } catch (std::exception e) {
            qWarning() << "[ERROR] TopicDatabase::read() - could not parse a row: " << e.what() << " - Ignore and continue with next.";
        }
    }

    db.close();
    return true;
}

bool TopicDatabase::insert(Topic &t){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] TopicDatabase::insert() - could not open database: " << db.lastError().text();
        return false;
    }

    t.creationDate = QDate::currentDate();

    QSqlQuery queryRankMax(db);
    queryRankMax.prepare("SELECT MAX(rank) FROM topics");

    if (!queryRankMax.exec()){
        qCritical() << "[ERROR] TopicDatabase::insert() - could not find max rank: " << queryRankMax.lastError().text();
        db.close();
        return false;
    }

    const uint maxRank = queryRankMax.next() ? queryRankMax.value(0).toUInt() : 0;
    t.rank = maxRank + 1;

    QSqlQuery query(db);
    query.prepare("INSERT INTO topics (title,colorHue,rank,status,creationDate) VALUES("
                  "'"+t.title+"',"
                  +QString::number(t.colorHue)+","
                  +QString::number(t.rank)+","
                  +QString::number(t.status)+","
                  "'"+t.creationDate.toString("yyyy/MM/dd")+"'"
                  ")");

    if (!query.exec()){
        qCritical() << "[ERROR] TopicDatabase::insert() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    t.uid = query.lastInsertId().toUInt();
    db.close();
    return true;
}

bool TopicDatabase::update(const Topic &t){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] TopicDatabase::update() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("UPDATE topics SET "
                  "title='"+t.title+"',"
                  "colorHue="+QString::number(t.colorHue)+","
                  "rank="+QString::number(t.rank)+","
                  "status="+QString::number(t.status)+
                  " WHERE uid="+QString::number(t.uid)
                  );

    if (!query.exec()){
        qCritical() << "[ERROR] TopicDatabase::update() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}

bool TopicDatabase::remove(const uint uid){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] TopicDatabase::remove() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("DELETE FROM topics WHERE uid="+QString::number(uid));

    if (!query.exec()){
        qCritical() << "[ERROR] TopicDatabase::remove() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}
