/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "topicmodel.h"

TopicModel::TopicModel(QObject *parent)
    : QAbstractListModel(parent){
}


int TopicModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant TopicModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case UIDROLE:
        return QVariant(m_list.at(index.row()).uid);
    case TITLEROLE:
        return QVariant(m_list.at(index.row()).title);
    case COLORROLE:
        return QVariant(m_list.at(index.row()).color);
    case COLORHUEROLE:
        return QVariant(m_list.at(index.row()).colorHue);
    case RANKROLE:
        return QVariant(m_list.at(index.row()).rank);
    case STATUSROLE:
        return QVariant(m_list.at(index.row()).status);
    case DAILYDURATIONROLE:
        return QVariant::fromValue<DailyValueModel*>(m_list.at(index.row()).dailyDurations);
    case DAILYDURATION30DAYSROLE:
        return QVariant::fromValue<DailyValueModel*>(m_list.at(index.row()).dailyDurations30days);
    case DAILYDURATION7DAYSROLE:
        return QVariant::fromValue<DailyValueModel*>(m_list.at(index.row()).dailyDurations7days);
    case CREATIONDATEROLE:
        return QVariant(m_list.at(index.row()).creationDate);
    }
    return QVariant();
}

QHash<int, QByteArray> TopicModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[UIDROLE]   = "topicUid";
    names[TITLEROLE] = "topicTitle";
    names[COLORROLE] = "topicColor";
    names[COLORHUEROLE] = "topicColorHue";
    names[RANKROLE]  = "topicRank";
    names[STATUSROLE]  = "topicStatus";
    names[DAILYDURATIONROLE] = "topicDailyDurationsModel";
    names[DAILYDURATION30DAYSROLE] = "topicDailyDurations30DaysModel";
    names[DAILYDURATION7DAYSROLE] = "topicDailyDurations7DaysModel";
    names[CREATIONDATEROLE]  = "topicCreationDate";
    return names;
}

int TopicModel::indexOf(const uint uid) const{
    for (int i=0; i<m_list.size(); i++)
        if (m_list.at(i).uid == uid)
            return i;
    return -1;
}

Topic TopicModel::get(const uint idx) const{
    return (idx < m_list.size()) ? m_list.at(idx) : Topic();
}

const Topic &TopicModel::at(const uint idx) const{
    return m_list.at(idx);
}

void TopicModel::insert(const Topic &t){
    const int L = m_list.size();
    int i;
    for (i=0; i<L; i++)
        if (t.rank < m_list.at(i).rank)
            break;

    beginInsertRows(QModelIndex(),i,i);
    m_list.insert(i,t);
    endInsertRows();

    emit countChanged();
}

void TopicModel::update(const Topic &t){
    const int idx = m_list.indexOf(t);
    if (idx < 0)
        return;

    m_list.replace(idx,t);
    emit dataChanged(index(idx),index(idx));
}

void TopicModel::removeTopic(const uint uid){
    for (int i=0; i< m_list.size(); i++){
        if (m_list.at(i).uid == uid){
            beginRemoveRows(QModelIndex(),i,i);
            m_list.removeAt(i);
            endRemoveRows();
            emit countChanged();
            return;
        }
    }
}

void TopicModel::swap(const uint idx1, const uint idx2){
    if (idx1 >= m_list.size() || idx2 >= m_list.size())
        return;

    beginMoveRows(QModelIndex(),idx1,idx1,QModelIndex(),idx2);
    m_list.move(idx1,idx2);
    endMoveRows();
}
