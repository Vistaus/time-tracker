/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QDebug>

#include "topics/topicmanager.h"
#include "projects/projectmanager.h"
#include "sessions/sessionmanager.h"
#include "calendar/calendarviewmanager.h"
#include "export/exportmanager.h"

class Backend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TopicManager *topics READ topics CONSTANT)
    Q_PROPERTY(ProjectManager *projects READ projects CONSTANT)
    Q_PROPERTY(SessionManager *sessions READ sessions CONSTANT)
    Q_PROPERTY(CalendarViewManager *calendar READ calendar CONSTANT)
    Q_PROPERTY(ExportManager *exports READ exports CONSTANT)

public:
    explicit Backend(QObject *parent = nullptr);

    inline TopicManager *topics() const {return m_topics;}
    inline ProjectManager *projects() const {return m_projects;}
    inline SessionManager *sessions() const {return m_sessions;}
    inline CalendarViewManager *calendar() const {return m_calendar;}
    inline ExportManager *exports() const {return m_exports;}

signals:

private:
    SessionManager *m_sessions;
    TopicManager *m_topics;
    ProjectManager *m_projects;
    CalendarViewManager *m_calendar;
    ExportManager *m_exports;
};

#endif // BACKEND_H
