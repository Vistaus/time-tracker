/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef EXPORTMANAGER_H
#define EXPORTMANAGER_H

#include <QObject>
#include <QDir>
#include <QStandardPaths>
#include <QDebug>

#include "sessions/sessionmanager.h"
#include "projects/projectmanager.h"

class ExportManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString filepath READ filepath CONSTANT)
    Q_PROPERTY(QString filename READ filename WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(SeparatorRole separator READ separator WRITE setSeparator NOTIFY separatorChanged)

public:
    enum ColumnRole{
        COLUMN_SESSIONID = 0,
        COLUMN_TOPICID,
        COLUMN_TOPICTITLE,
        COLUMN_PROJECTID,
        COLUMN_PROJECTTITLE,
        COLUMN_DATE,
        COLUMN_DURATION,
        COLUMN_START,
        COLUMN_END,
        COLUMN_INTERVALS,
        COLUMN_DESCRIPTION,
        NUMBER_OF_COLUMNS
    };

    enum SeparatorRole{
        SEPARATOR_TAB = 0,
        SEPARATOR_SEMICOLON,
        SEPARATOR_COMMA,
        SEPARATOR_BLANK,
        NUMBER_OF_SEPARATORS
    };
    Q_ENUMS(SeparatorRole)

    explicit ExportManager(SessionManager *sessions = nullptr, ProjectManager *projects = nullptr, QObject *parent = nullptr);

    inline QString filename() const {return m_filename;}
    inline QString filepath() const {return m_filepath;}
    inline SeparatorRole separator() const {return m_separator;}

    Q_INVOKABLE QString getFullFilename() const;
    Q_INVOKABLE QString columnTitle(const int role) const;
    Q_INVOKABLE QString getSeparator(const SeparatorRole role = NUMBER_OF_SEPARATORS) const;

    Q_INVOKABLE bool createExportFile();

public slots:
    void setFilename(const QString &filename);
    void setSeparator(const SeparatorRole separator);

signals:
    void filenameChanged();
    void separatorChanged();

private:
    QString m_filename;
    SeparatorRole m_separator = SEPARATOR_TAB;

    SessionManager *m_sessions;
    ProjectManager *m_projects;

    const QString m_filepath = QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/export";
};

#endif // EXPORTMANAGER_H
