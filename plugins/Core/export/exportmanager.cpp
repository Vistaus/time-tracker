/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "exportmanager.h"

ExportManager::ExportManager(SessionManager *sessions, ProjectManager *projects, QObject *parent)
    : QObject(parent), m_sessions(sessions), m_projects(projects) {

    QDir dir;
    dir.cd(filepath());
    dir.removeRecursively();

    qInfo() << "[INIT] ExportManager created.";
}

QString ExportManager::getFullFilename() const{
    return m_filepath + "/" + m_filename;
}

QString ExportManager::columnTitle(const int role) const{
    switch (role) {
    case COLUMN_SESSIONID:
        return "sessionId";
    case COLUMN_TOPICID:
        return "topicId";
    case COLUMN_TOPICTITLE:
        return "topicTitle";
    case COLUMN_PROJECTID:
        return "projectId";
    case COLUMN_PROJECTTITLE:
        return "projectTitle";
    case COLUMN_DATE:
        return "date";
    case COLUMN_DURATION:
        return "duration";
    case COLUMN_DESCRIPTION:
        return "description";
    case COLUMN_START:
        return "start";
    case COLUMN_END:
        return "end";
    case COLUMN_INTERVALS:
        return "intervals";
    }
    return "UNKNOWN";
}

QString ExportManager::getSeparator(const SeparatorRole role) const{
    switch (role == NUMBER_OF_SEPARATORS ? m_separator : role){
    case SEPARATOR_TAB:
        return "\t";
    case SEPARATOR_SEMICOLON:
        return ";";
    case SEPARATOR_COMMA:
        return ",";
    case SEPARATOR_BLANK:
        return " ";
    }
    return "INVALID_SEPARATOR";
}

bool ExportManager::createExportFile(){
    if (!m_sessions || !m_projects)
        return false;

    if (!QDir().mkpath(filepath()))
        qCritical() << "Could not create export data directory '" + filepath() + "'";

    QFile file(getFullFilename());
    if (file.open(QIODevice::WriteOnly)){
        QTextStream os((&file));

        const QString sep = getSeparator();

        // write headers
        for (int i=0; i<NUMBER_OF_COLUMNS; i++)
            os << columnTitle(i) << sep;
        os << "\n";

        // write sessions
        for (const Session &s : m_sessions->allSessions()){
            QVector<QString> data(NUMBER_OF_COLUMNS);
            data[COLUMN_SESSIONID]    = QString::number(s.uid);
            data[COLUMN_TOPICID]      = QString::number(m_projects->getTopicUid(s.project));
            data[COLUMN_TOPICTITLE]   = m_projects->getTopicTitle(s.project);
            data[COLUMN_PROJECTID]    = QString::number(s.project);
            data[COLUMN_PROJECTTITLE] = m_projects->getTitle(s.project);
            data[COLUMN_DATE]         = s.startDate.toString("yyyy/MM/dd");
            data[COLUMN_DURATION]     = QString::number(s.duration()/60);
            data[COLUMN_START]        = s.start().toString("yyyy/MM/dd hh:mm");
            data[COLUMN_END]          = s.end().toString("yyyy/MM/dd hh:mm");
            data[COLUMN_INTERVALS]    = QString::number(s.intervals.size());
            data[COLUMN_DESCRIPTION]  = s.description;

            for (int i=0; i<NUMBER_OF_COLUMNS; i++){
                // ensure the data does not contain any ", which would break the CSV format
                data[i].replace("\"","'");

                if (data.at(i).indexOf(sep) < 0)
                    os << data.at(i) << sep;
                else
                    os << "\"" << data.at(i) << "\"" << sep;
                }
            os << "\n";
        }

        file.close();
        return true;
    } else {
        qCritical() << "[ERROR] ExportManager::createExportFile: could not open file '"+getFullFilename()+"'. Export not possible.";
        return false;
    }
}

void ExportManager::setFilename(const QString &filename){
    if (m_filename == filename)
        return;

    m_filename = filename;
    emit filenameChanged();
}

void ExportManager::setSeparator(const ExportManager::SeparatorRole separator){
    if (m_separator == separator || separator == NUMBER_OF_SEPARATORS)
        return;

    m_separator = separator;
    emit separatorChanged();
}
