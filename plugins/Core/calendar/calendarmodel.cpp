/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "calendarmodel.h"

int CalendarModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant CalendarModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case DATEROLE:
        return QVariant(m_list.at(index.row()).date);
    case MODELROLE:
        return QVariant::fromValue<CalendarDayModel*>(m_list.at(index.row()).model);
    }
    return QVariant();
}

QHash<int, QByteArray> CalendarModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[DATEROLE]  = "calEntryDate";
    names[MODELROLE] = "calEntryModel";
    return names;
}

uint CalendarModel::getSessionIdAt(const int day, const qreal timeOfDay) const{
    if (day >= m_list.size() || day < 0)
        return 0;

    return m_list.at(day).model->getSessionIdAt(timeOfDay);
}

void CalendarModel::onFirstDayChanged(const QDate &firstDay){
    if (m_firstDay == firstDay)
        return;

    m_firstDay = firstDay;
    for (int i=0; i<m_list.size(); i++){
        const QDate newDate = m_firstDay.addDays(i);
        if (m_list[i].date != newDate){
            m_list[i].date = newDate;
            updateSessions(i);
        }
    }
    emit dataChanged(index(0),index(m_list.size()-1),{DATEROLE});
}

void CalendarModel::onDisplayedDaysChanged(const uint displayedDays){
    if (m_list.size() == displayedDays)
        return;

    if (m_list.size() < displayedDays){
        beginInsertRows(QModelIndex(),m_list.size(),displayedDays-1);
        for (int i=m_list.size(); i<displayedDays; i++){
            CalendarElement e;
            e.date = m_firstDay.addDays(i);
            e.model = new CalendarDayModel;

            m_list.insert(i,e);
            updateSessions(i);
        }
        endInsertRows();
        emit countChanged();
    } else {
        beginRemoveRows(QModelIndex(),displayedDays,m_list.size()-1);
        for (int i=m_list.size()-1; i>=displayedDays; i--){
            delete m_list.back().model;
            m_list.removeLast();
        }
        endRemoveRows();
        emit countChanged();
    }
}

void CalendarModel::onSessionAdded(const uint uid){
    const Session s = m_sessions->getSession(uid);
    if (s.uid == 0)
        return;

    const QDate dmin = s.start().date();
    const QDate dmax = s.end().date();
    for (CalendarElement &ce : m_list){
        if (ce.date >= dmin && ce.date <= dmax){
            CalendarDayElement e;
            e.session = s.uid;
            e.project = s.project;
            const int dayOffset = ce.date.daysTo(s.startDate);
            for (const SessionInterval &si : s.intervals){
                e.subsession = si.uid;
                e.startSecs  = dayOffset*86400 + si.startSecs();
                e.endSecs    = dayOffset*86400 + si.endSecs();
                if (e.endSecs > 0 && e.startSecs < 86400)
                    ce.model->add(e);
            }
        }
    }
}

void CalendarModel::onSessionRemoved(const uint uid){
    for (CalendarElement &ce : m_list){
        ce.model->removeSession(uid);
    }
}

void CalendarModel::onSessionUpdated(const uint uid){
    onSessionRemoved(uid);
    onSessionAdded(uid);
}

void CalendarModel::updateSessions(const uint idx){
    if (idx >= m_list.size())
        return;

    const QDate &date = m_list.at(idx).date;

    m_list[idx].model->clear();
    for (const Session &s : m_sessions->allSessions()){
        if (s.start().date() <= date &&
            s.end().date()   >= date){
            CalendarDayElement e;
            e.session = s.uid;
            e.project = s.project;
            const int dayOffset = date.daysTo(s.startDate);
            for (const SessionInterval &si : s.intervals){
                e.subsession = si.uid;
                e.startSecs  = dayOffset*86400 + si.startSecs();
                e.endSecs    = dayOffset*86400 + si.endSecs();
                if (e.endSecs > 0 && e.startSecs < 86400)
                    m_list[idx].model->add(e);
            }
        }
    }
}
