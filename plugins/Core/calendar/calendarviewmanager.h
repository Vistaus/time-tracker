/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef CALENDARVIEWMANAGER_H
#define CALENDARVIEWMANAGER_H

#include <QObject>
#include <QDebug>
#include <QDate>

#include "../sessions/sessionmanager.h"
#include "../projects/projectmanager.h"

#include "calendarmodel.h"

class CalendarViewManager : public QObject
{
    Q_OBJECT
    Q_ENUMS(CalendarMode)
    Q_PROPERTY(CalendarMode mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(QDate firstDay READ firstDay NOTIFY firstDayChanged)
    Q_PROPERTY(int displayedDays READ displayedDays NOTIFY displayedDaysChanged)
    Q_PROPERTY(CalendarModel *model READ model CONSTANT)

public:
    enum CalendarMode {
        WEEKLY,
        MONTHLY
    };

    explicit CalendarViewManager(const SessionManager *sessions = nullptr, const ProjectManager *projects = nullptr, QObject *parent = nullptr);

    /* ----------------------- *
     * ---- QML interface ---- */
    inline CalendarMode mode() const {return m_mode;}
    inline QDate firstDay() const {return m_firstDay;}
    int displayedDays() const;
    inline CalendarModel *model() const {return m_model;}

    Q_INVOKABLE void showPreviousPage();
    Q_INVOKABLE void showNextPage();
    Q_INVOKABLE void showToday();

    /* ----------------------- *
     * ---- C++ interface ---- */
    void updateFirstDay(const QDate referenceDate = QDate::currentDate());

public slots:
    void setMode(const CalendarMode mode);

signals:
    void firstDayChanged(const QDate &firstDay);
    void modeChanged();
    void displayedDaysChanged(const uint displayedDays);

private:
    const SessionManager *m_sessions;
    const ProjectManager *m_projects;

    CalendarMode m_mode;
    QDate m_firstDay;

    CalendarModel *m_model;

};

#endif // CALENDARVIEWMANAGER_H
