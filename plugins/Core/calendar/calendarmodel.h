/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef CALENDARMODEL_H
#define CALENDARMODEL_H

#include <QAbstractListModel>
#include <QDate>

#include "calendardaymodel.h"
#include "sessions/sessionmanager.h"

struct CalendarElement{
    QDate date;
    CalendarDayModel *model = nullptr;
};


class CalendarModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
public:
    enum Roles{
        DATEROLE,
        MODELROLE
    };

    inline CalendarModel(const SessionManager *sessions = nullptr, QObject *parent = nullptr) : QAbstractListModel(parent), m_sessions(sessions){}


    /* ----------------------- *
     * ---- QML interface ---- */
    inline uint count() const {return m_list.size();}

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE uint getSessionIdAt(const int day, const qreal timeOfDay) const;

    /* ----------------------- *
     * ---- C++ interface ---- */

public slots:
    void onFirstDayChanged(const QDate &firstDay);
    void onDisplayedDaysChanged(const uint displayedDays);

    void onSessionAdded(const uint uid);
    void onSessionRemoved(const uint uid);
    void onSessionUpdated(const uint uid);

signals:
    void countChanged();

private:
    const SessionManager *m_sessions;
    QVector<CalendarElement> m_list;
    QDate m_firstDay;

    void updateSessions(const uint idx);
};

#endif // CALENDARMODEL_H
