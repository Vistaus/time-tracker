/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "calendardaymodel.h"

int CalendarDayModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant CalendarDayModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case SESSIONROLE:
        return QVariant(m_list.at(index.row()).session);
    case SUBSESSIONROLE:
        return QVariant(m_list.at(index.row()).subsession);
    case PROJECTROLE:
        return QVariant(m_list.at(index.row()).project);
    case STARTSECSROLE:
        return QVariant(m_list.at(index.row()).startSecs);
    case ENDSECSROLE:
        return QVariant(m_list.at(index.row()).endSecs);
    }
    return QVariant();
}

QHash<int, QByteArray> CalendarDayModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[SESSIONROLE]    = "entrySession";
    names[SUBSESSIONROLE] = "entrySubsession";
    names[PROJECTROLE]    = "entryProject";
    names[STARTSECSROLE]  = "entryStartSecs";
    names[ENDSECSROLE]    = "entryEndSecs";
    return names;
}

uint CalendarDayModel::getSessionIdAt(const qreal timeOfDay) const{
    if (timeOfDay < 0 || timeOfDay > 1)
        return 0;

    const int secs = timeOfDay*86400;
    for (const CalendarDayElement &e : m_list)
        if (e.startSecs < secs && e.endSecs > secs)
            return e.session;

    return 0;
}



void CalendarDayModel::add(const CalendarDayElement &e){
    const uint idx = m_list.size();

    beginInsertRows(QModelIndex(),idx,idx);
    m_list.insert(idx,e);
    endInsertRows();
    emit countChanged();
}

void CalendarDayModel::replace(const CalendarDayElement &e){
    for (int i=0; i<m_list.size(); i++){
        if (m_list.at(i).subsession == e.subsession){
            m_list.replace(i,e);
            emit dataChanged(index(i),index(i));
        }
    }
}

void CalendarDayModel::removeSession(const uint session){
    for (int i=0; i<m_list.size(); i++){
        if (m_list.at(i).session == session){
            beginRemoveRows(QModelIndex(),i,i);
            m_list.removeAt(i);
            endRemoveRows();
            emit countChanged();
            i--;
        }
    }
}

void CalendarDayModel::clear(){
    if (m_list.size() == 0)
        return;

    beginResetModel();
    m_list.clear();
    endResetModel();
    emit countChanged();
}
