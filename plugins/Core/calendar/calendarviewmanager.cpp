/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "calendarviewmanager.h"

CalendarViewManager::CalendarViewManager(const SessionManager *sessions, const ProjectManager *projects, QObject *parent)
    : QObject(parent), m_sessions(sessions), m_projects(projects)
    , m_mode(WEEKLY)
    , m_model(new CalendarModel(sessions)){

    connect(this,SIGNAL(displayedDaysChanged(uint)),m_model,SLOT(onDisplayedDaysChanged(uint)));
    connect(this,SIGNAL(firstDayChanged(QDate)),m_model,SLOT(onFirstDayChanged(QDate)));

    updateFirstDay();

    m_model->onFirstDayChanged(m_firstDay);
    m_model->onDisplayedDaysChanged(displayedDays());

    connect(m_sessions,SIGNAL(sessionAdded(uint))  ,m_model,SLOT(onSessionAdded(uint)));
    connect(m_sessions,SIGNAL(sessionRemoved(uint)),m_model,SLOT(onSessionRemoved(uint)));
    connect(m_sessions,SIGNAL(sessionUpdated(uint)),m_model,SLOT(onSessionUpdated(uint)));

    qInfo() << "[INIT] CalendarViewManager created.";
}

int CalendarViewManager::displayedDays() const{
    switch (m_mode) {
    case WEEKLY:
        return 7;
    case MONTHLY:
        return m_firstDay.daysInMonth();
    }
}

void CalendarViewManager::showPreviousPage(){
    updateFirstDay(m_firstDay.addDays(-1));
}

void CalendarViewManager::showNextPage(){
    updateFirstDay(m_firstDay.addDays(displayedDays()));
}

void CalendarViewManager::showToday(){
    updateFirstDay();
}

void CalendarViewManager::updateFirstDay(const QDate referenceDate){
    const int oldDisplayedDays = displayedDays();
    const QDate oldDate = m_firstDay;
    switch (m_mode) {
    case WEEKLY:
        m_firstDay = referenceDate.addDays(1-referenceDate.dayOfWeek());
        break;
    case MONTHLY:
        m_firstDay = referenceDate.addDays(1-referenceDate.day());
        break;
    }

    if (m_firstDay != oldDate)
        emit firstDayChanged(m_firstDay);

    if (displayedDays() != oldDisplayedDays)
        emit displayedDaysChanged(displayedDays());
}

void CalendarViewManager::setMode(const CalendarMode mode){
    const int oldDisplayedDays = displayedDays();
    if (m_mode == mode)
        return;

    m_mode = mode;
    emit modeChanged();

    updateFirstDay(m_firstDay);

    if (displayedDays() != oldDisplayedDays)
        emit displayedDaysChanged(displayedDays());
}
