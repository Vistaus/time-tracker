/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef CALENDARDAYMODEL_H
#define CALENDARDAYMODEL_H

#include <QAbstractListModel>

struct CalendarDayElement{
    uint session    = 0;
    uint subsession = 0;
    uint project    = 0;
    int  startSecs  = 0;
    int  endSecs    = 0;
};


class CalendarDayModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
public:
    enum Roles{
        SESSIONROLE,
        SUBSESSIONROLE,
        PROJECTROLE,
        STARTSECSROLE,
        ENDSECSROLE
    };

    inline CalendarDayModel(QObject *parent = nullptr) : QAbstractListModel(parent){}


    /* ----------------------- *
     * ---- QML interface ---- */
    inline uint count() const {return m_list.size();}

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE uint getSessionIdAt(const qreal timeOfDay) const;

    /* ----------------------- *
     * ---- C++ interface ---- */
    inline const QVector<CalendarDayElement> list() const {return m_list;}

    void add(const CalendarDayElement &e);
    void replace(const CalendarDayElement &e);
    void removeSession(const uint session);
    void clear();

signals:
    void countChanged();

private:
    QVector<CalendarDayElement> m_list;

};

#endif // CALENDARDAYMODEL_H
