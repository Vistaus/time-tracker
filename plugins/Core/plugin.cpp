/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "backend.h"

#include "topics/topicmanager.h"
#include "topics/topicmodel.h"
#include "topics/selectedtopic.h"

#include "projects/projectmanager.h"
#include "projects/projectmodel.h"

#include "sessions/sessionmanager.h"
#include "sessions/dailysessionsmodel.h"
#include "sessions/sessionmodel.h"
#include "sessions/selectedsession.h"

#include "calendar/calendarviewmanager.h"
#include "calendar/calendardaymodel.h"
#include "calendar/calendarmodel.h"

#include "export/exportmanager.h"

#include "types/dailyvaluemodel.h"

void ExamplePlugin::registerTypes(const char *uri) {
    //@uri TTCore
    qmlRegisterSingletonType<Backend>(uri, 1, 0, "Backend", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Backend; });

    qmlRegisterType<TopicModel>(uri, 1, 0, "TopicModel");
    qmlRegisterType<TopicManager>(uri, 1, 0, "TopicManager");
    qmlRegisterType<SelectedTopic>(uri, 1, 0, "SelectedTopic");

    qmlRegisterType<ProjectManager>(uri, 1, 0, "ProjectManager");
    qmlRegisterType<ProjectModel>(uri, 1, 0, "ProjectModel");

    qmlRegisterType<SessionManager>(uri, 1, 0, "SessionManager");
    qmlRegisterType<DailySessionsModel>(uri, 1, 0, "DailySessionsModel");
    qmlRegisterType<SessionModel>(uri, 1, 0, "SessionModel");
    qmlRegisterType<SelectedSession>(uri, 1, 0, "SelectedSession");

    qmlRegisterType<CalendarViewManager>(uri, 1, 0, "CalendarViewManager");
    qmlRegisterType<CalendarDayModel>(uri, 1, 0, "CalendarDayModel");
    qmlRegisterType<CalendarModel>(uri, 1, 0, "CalendarModel");

    qmlRegisterType<ExportManager>(uri, 1, 0, "ExportManager");

    qmlRegisterType<DailyValueModel>(uri, 1, 0, "DailyValueModel");
}
