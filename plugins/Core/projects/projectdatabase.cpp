/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "projectdatabase.h"

ProjectDatabase::ProjectDatabase(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",m_db_name);
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + m_db_name + ".sqlite");
    if (!db.open()){
        qCritical() << "[ERROR] ProjectDatabase::ProjectDatabase() - could not open database: " << db.lastError().text();
        return;
    }

    QSqlQuery query(db);
    query.prepare("CREATE TABLE projects ("
                  "uid INTEGER PRIMARY KEY,"
                  "title TEXT DEFAULT '',"
                  "colorHue INTEGER DEFAULT 0,"
                  "colorLightness INTEGER DEFAULT 0,"
                  "topic INTEGER DEFAULT 0,"
                  "rank INTEGER DEFAULT 0,"
                  "status INTEGER DEFAULT 0,"
                  "creationDate TEXT DEFAULT '')"
                  );

    if (query.exec())
        qInfo() << "ProjectDatabase: table for projects created";

    db.close();
}

bool ProjectDatabase::read(ProjectList &list){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] ProjectDatabase::read() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("SELECT * FROM projects");

    if (!query.exec()){
        qCritical() << "[ERROR] ProjectDatabase::read() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    list.clear();
    while (query.next()){
        try{
            Project p;
            p.uid   = query.value("uid").toUInt();
            p.title = query.value("title").toString();
            p.colorHue = query.value("colorHue").toInt();
            p.colorLightness = query.value("colorLightness").toInt();
            p.topic = query.value("topic").toUInt();
            p.rank  = query.value("rank").toUInt();
            p.status = (Project::Status) query.value("status").toInt();
            p.creationDate  = QDate::fromString(query.value("creationDate").toString(),"yyyy/MM/dd");

            int i;
            for (i=0; i<list.size(); i++)
                if (list.at(i).rank > p.rank)
                    break;
            list.insert(i,p);
        } catch (std::exception e) {
            qWarning() << "[ERROR] ProjectDatabase::read() - could not parse a row: " << e.what() << " - Ignore and continue with next.";
        }
    }

    db.close();
    return true;
}

bool ProjectDatabase::insert(Project &p){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] ProjectDatabase::insert() - could not open database: " << db.lastError().text();
        return false;
    }

    p.creationDate = QDate::currentDate();

    QSqlQuery queryRankMax(db);
    queryRankMax.prepare("SELECT MAX(rank) FROM projects");

    if (!queryRankMax.exec()){
        qCritical() << "[ERROR] ProjectDatabase::insert() - could not find max rank: " << queryRankMax.lastError().text();
        db.close();
        return false;
    }

    const uint maxRank = queryRankMax.next() ? queryRankMax.value(0).toUInt() : 0;
    p.rank = maxRank + 1;

    QSqlQuery query(db);
    query.prepare("INSERT INTO projects (title,colorHue,colorLightness,topic,rank,status,creationDate) VALUES("
                  "'"+p.title+"',"
                  +QString::number(p.colorHue)+","
                  +QString::number(p.colorLightness)+","
                  +QString::number(p.topic)+","
                  +QString::number(p.rank)+","
                  +QString::number(p.status)+","
                  "'"+p.creationDate.toString("yyyy/MM/dd")+"'"
                  ")");

    if (!query.exec()){
        qCritical() << "[ERROR] ProjectDatabase::insert() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    p.uid = query.lastInsertId().toUInt();
    db.close();
    return true;
}

bool ProjectDatabase::update(const Project &p){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] ProjectDatabase::update() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("UPDATE projects SET "
                  "title='"+p.title+"',"
                  "colorHue="+QString::number(p.colorHue)+","
                  "colorLightness="+QString::number(p.colorLightness)+","
                  "topic="+QString::number(p.topic)+","
                  "rank="+QString::number(p.rank)+","
                  "status="+QString::number(p.status)+
                  " WHERE uid="+QString::number(p.uid)
                  );

    if (!query.exec()){
        qCritical() << "[ERROR] ProjectDatabase::update() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}

bool ProjectDatabase::update(const ProjectList &list){
    for (const Project &p : list)
        if (!update(p))
            return false;
    return true;
}

bool ProjectDatabase::remove(const uint uid){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "[ERROR] ProjectDatabase::remove() - could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query(db);
    query.prepare("DELETE FROM projects WHERE uid="+QString::number(uid));

    if (!query.exec()){
        qCritical() << "[ERROR] ProjectDatabase::remove() - could not execute query: " << query.lastError().text();
        db.close();
        return false;
    }

    db.close();
    return true;
}
