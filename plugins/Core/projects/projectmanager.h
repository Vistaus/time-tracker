/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QObject>
#include <QDebug>
#include <QHash>

#include "project.h"
#include "projectmodel.h"
#include "projectdatabase.h"

#include "topics/topicmanager.h"
#include "sessions/sessionmanager.h"
#include "sessions/sessionstatsrecord.h"

typedef QHash<uint,SessionStatsRecordList> SessionStatsHash;


class ProjectManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ProjectModel *selectedTopicModel READ selectedTopicModel CONSTANT)
    Q_PROPERTY(ProjectModel *selectorModel READ selectorModel CONSTANT)
public:
    explicit ProjectManager(TopicManager *topics = nullptr,const SessionManager *sessions = nullptr, QObject *parent = nullptr);

    /* --------------------- *
     * --- QML interface --- */
    inline ProjectModel *selectedTopicModel() const {return m_selectedTopicModel;}
    inline ProjectModel *selectorModel()      const {return m_selectorModel;}

    Q_INVOKABLE QColor getColor(const QColor &baseColor, const int hue, const int lightness) const;
    Q_INVOKABLE QColor getColor(const uint uid) const;
    Q_INVOKABLE QColor getTopicColor(const uint uid) const;
    Q_INVOKABLE QString getTitle(const uint uid) const;
    Q_INVOKABLE QString getTopicTitle(const uint uid) const;

    Q_INVOKABLE uint getTopicUid(const uint uid) const;

    Q_INVOKABLE void add(const QString &title, const int colorHue, const int colorLightness, const uint topic);
    Q_INVOKABLE void remove(const uint uid);
    Q_INVOKABLE void update(const uint uid, const QString &title, const int colorHue, const int colorLightness);
    Q_INVOKABLE void swap(const uint uid1, const uint uid2);
    Q_INVOKABLE void setStatus(const uint uid, const uint status);

    Q_INVOKABLE void setSelectorModelTopic(const uint topic);

    /* --------------------- *
     * --- C++ interface --- */

public slots:
    void onTopicAdded(const uint uid);
    void onTopicRemoved(const uint uid);
    void onTopicSelected(const uint uid);
    void onTopicColorChanged(const uint uid);

    void onSessionAdded(const uint uid);
    void onSessionRemoved(const uint uid);
    void onSessionUpdated(const uint uid);

signals:
    void projectRemoved(const uint uid);

private:
    TopicManager *m_topics;
    const SessionManager *m_sessions;
    ProjectDatabase m_db;
    ProjectList m_allProjects;
    SessionStatsHash m_sessionStats;

    uint selectedTopicUid = 0;
    uint m_selectorTopicUid = 0;
    ProjectModel *m_selectedTopicModel;
    ProjectModel *m_selectorModel;

};

#endif // PROJECTMANAGER_H
