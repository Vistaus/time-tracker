/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef PROJECTDATABASE_H
#define PROJECTDATABASE_H

#include <QDebug>
#include <QString>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>

#include "project.h"

class ProjectDatabase
{
public:
    ProjectDatabase();

    bool read(ProjectList &list);

    bool insert(Project &p);
    bool update(const Project &p);
    bool update(const ProjectList &list);
    bool remove(const uint uid);

private:
    const QString m_db_name = "projects";
};

#endif // PROJECTDATABASE_H
