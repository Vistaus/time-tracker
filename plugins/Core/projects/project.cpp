/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "project.h"

QColor Project::getColor(const QColor &baseColor, const int hue, const int lightness){
    qreal h,s,l;
    baseColor.getHslF(&h,&s,&l);
    h += hue * projectColorHueStepWidth;
    s -= abs(hue) * projectColorSaturationStepWidth;
    l += lightness * projectColorLightnessStepWidth;

    while (h<0)
        h += 1;
    while (h>1)
        h -= 1;

    QColor c;
    c.setHslF(h,s,l);
    return c;
}

bool operator==(const Project &t1, const Project &t2){
    return t1.uid == t2.uid;
}


/* ------------------------------- *
 * --------- ProjectList --------- *
 * ------------------------------- */

void ProjectList::removeByUid(const uint uid){
    for (int i=0; i<size(); i++){
        if (at(i).uid == uid){
            removeAt(i);
            return;
        }
    }
}

int ProjectList::find(const int uid) const {
    for (int i=0; i<size(); i++)
        if (at(i).uid == uid)
            return i;
    return -1;
}
