/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "projectmanager.h"

ProjectManager::ProjectManager(TopicManager *topics, const SessionManager *sessions, QObject *parent)
    : QObject(parent), m_topics(topics), m_sessions(sessions)
    , m_selectedTopicModel(new ProjectModel)
    , m_selectorModel(new ProjectModel){

    if (!topics)
        qCritical() << "[ERROR] ProjectManager: no link to topic manager provided. ProjectManager will not work.";
    if (!sessions)
        qCritical() << "[ERROR] ProjectManager: no link to session manager provided. ProjectManager will not work.";


    // read projects from DB
    m_db.read(m_allProjects);
    for (Project &p : m_allProjects)
        p.color = Project::getColor(topics->getColor(p.topic),p.colorHue,p.colorLightness);

    // read session stats
    for (const Session &s : m_sessions->allSessions()){
        SessionDailyDuration dailyDuration = s.dailyDurations();

        SessionStatsRecord rec(s.uid);
        for (int dayOffset : dailyDuration.keys()){
            rec.date = s.startDate.addDays(dayOffset);
            rec.duration = dailyDuration[dayOffset];
            m_sessionStats[s.project].append(rec);
        }
    }

    // fill daily durations of projects and topics
    for (Project &p : m_allProjects){
        for (const SessionStatsRecord &rec : m_sessionStats[p.uid]){
            p.dailyDurations->add(rec.date,rec.duration);
            p.dailyDurations30days->add(rec.date,rec.duration);
            p.dailyDurations7days->add(rec.date,rec.duration);
            m_topics->addDailyDuration(p.topic,rec.date,rec.duration);
        }
        p.dailyDurations30days->setTimeRange(QDate::currentDate().addDays(-29),QDate::currentDate());
        p.dailyDurations7days->setTimeRange(QDate::currentDate().addDays(-6),QDate::currentDate());
    }
    qInfo() << "[INIT] ProjectManager created.";
}

QColor ProjectManager::getColor(const QColor &baseColor, const int hue, const int lightness) const{
    return Project::getColor(baseColor,hue,lightness);
}

QColor ProjectManager::getColor(const uint uid) const{
    const int idx = m_allProjects.find(uid);
    return (idx > -1) ? m_allProjects.at(idx).color : QColor();

}

QColor ProjectManager::getTopicColor(const uint uid) const{
    const int idx = m_allProjects.find(uid);
    return (idx > -1) ? m_topics->getColor(m_allProjects.at(idx).topic) : QColor();
}

QString ProjectManager::getTitle(const uint uid) const{
    const int idx = m_allProjects.find(uid);
    return (idx > -1) ? m_allProjects.at(idx).title : QString();

}

QString ProjectManager::getTopicTitle(const uint uid) const{
    const int idx = m_allProjects.find(uid);
    return (idx > -1) ? m_topics->getTitle(m_allProjects.at(idx).topic) : QString();
}

uint ProjectManager::getTopicUid(const uint uid) const{
    const int idx = m_allProjects.find(uid);
    return (idx > -1) ? m_allProjects.at(idx).topic : 0;
}

void ProjectManager::add(const QString &title, const int colorHue, const int colorLightness, const uint topic){
    Project p;
    p.title = title;
    p.topic = topic;
    p.colorHue = colorHue;
    p.colorLightness = colorLightness;
    p.color = Project::getColor(m_topics->getColor(p.topic),p.colorHue,p.colorLightness);
    p.status = Project::ACTIVE;

    if (m_db.insert(p)){
        m_allProjects.append(p);

        if (p.topic == selectedTopicUid)
            m_selectedTopicModel->insert(p);

        if (p.topic == m_selectorTopicUid && p.status == Project::ACTIVE)
            m_selectorModel->insert(p);
    }
}

void ProjectManager::remove(const uint uid){
    if (m_db.remove(uid)){
        const int idx = m_allProjects.find(uid);
        if (idx > -1){
            Project &p = m_allProjects[idx];
            delete p.dailyDurations;
            delete p.dailyDurations30days;
        }
        m_allProjects.removeByUid(uid);
        m_selectedTopicModel->removeProject(uid);
        m_selectorModel->removeProject(uid);
        emit projectRemoved(uid);
    }
}

void ProjectManager::update(const uint uid, const QString &title, const int colorHue, const int colorLightness){
    uint idx;
    for(idx=0; idx<m_allProjects.size(); idx++)
        if (m_allProjects.at(idx).uid == uid)
            break;

    if (idx >= m_allProjects.size())
        return;

    Project p = m_allProjects.at(idx);
    p.title = title;
    p.colorHue = colorHue;
    p.colorLightness = colorLightness;
    p.color = Project::getColor(m_topics->getColor(p.topic),colorHue,colorLightness);

    if (m_db.update(p)){
        m_allProjects.replace(idx,p);
        m_selectedTopicModel->update(p);

        const int selectorModelIdx = m_selectorModel->indexOf(p.uid);
        if (p.topic == m_selectorTopicUid && p.status == Project::ACTIVE){
            if (selectorModelIdx < 0)
                m_selectorModel->insert(p);
            else
                m_selectorModel->update(p);
        } else {
            m_selectorModel->removeProject(p.uid);
        }
    }
}

void ProjectManager::swap(const uint uid1, const uint uid2){
    const int idx1 = m_allProjects.find(uid1);
    const int idx2 = m_allProjects.find(uid2);
    if (idx1 < 0 || idx2 < 0)
        return;

    Project project1 = m_allProjects.at(idx1);
    Project project2 = m_allProjects.at(idx2);

    // swap ranks
    uint rank1    = project1.rank;
    project1.rank = project2.rank;
    project2.rank = rank1;

    // update DB
    if (m_db.update(project1) && m_db.update(project2)){
        // update list
        m_allProjects.replace(idx1,project2);
        m_allProjects.replace(idx2,project1);

        // update model
        m_selectedTopicModel->update(project1);
        m_selectedTopicModel->update(project2);
        m_selectedTopicModel->swap(project1.uid,project2.uid);

        // update selectorModel
        m_selectorModel->update(project1);
        m_selectorModel->update(project2);
        m_selectorModel->swap(project1.uid,project2.uid);
    }
}

void ProjectManager::setStatus(const uint uid, const uint status){
    const int idx = m_allProjects.find(uid);
    if (idx < 0)
        return;

    Project project = m_allProjects.at(idx);

    const Project::Status oldStatus = project.status;

    switch (status) {
    case 0:
        project.status = Project::ACTIVE;
        break;
    case 1:
        project.status = Project::INACTIVE;
        break;
    }

    if (project.status == oldStatus)
        return;

    if (m_db.update(project)){
        // update list
        m_allProjects.replace(idx,project);

        // update model
        m_selectedTopicModel->update(project);

        // update selectorModel
        if (project.status == Project::ACTIVE)
            m_selectorModel->update(project,true);
        else
            m_selectorModel->removeProject(project.uid);
    }
}

void ProjectManager::setSelectorModelTopic(const uint topic){
    if (topic == m_selectorTopicUid)
        return;

    m_selectorTopicUid = topic;

    m_selectorModel->clear();
    for (const Project &p : m_allProjects)
        if (p.topic == topic && p.status == Project::ACTIVE)
            m_selectorModel->insert(p);
}

void ProjectManager::onTopicAdded(const uint uid){
    add(m_topics->getTitle(uid),0,0,uid);
}

void ProjectManager::onTopicRemoved(const uint uid){
    for (int i=m_allProjects.size()-1; i>=0; i--)
        if (m_allProjects.at(i).topic == uid)
            remove(m_allProjects.at(i).uid);
}

void ProjectManager::onTopicSelected(const uint uid){
    ProjectList list;
    for (const Project &p : m_allProjects)
        if (p.topic == uid)
            list.append(p);

    selectedTopicUid = uid;
    m_selectedTopicModel->setList(list);
}

void ProjectManager::onTopicColorChanged(const uint uid){
    const QColor baseColor = m_topics->getColor(uid);
    for (Project &p : m_allProjects){
        if (p.topic == uid){
            p.color = Project::getColor(baseColor,p.colorHue,p.colorLightness);
            m_selectedTopicModel->update(p);
        }
    }
}

void ProjectManager::onSessionAdded(const uint uid){
    const Session s = m_sessions->getSession(uid);

    //update stats
    const int idx = m_allProjects.find(s.project);
    if (idx < 0)
        return;

    Project &p = m_allProjects[idx];

    SessionDailyDuration dailyDuration = s.dailyDurations();
    SessionStatsRecord rec(s.uid);
    for (int dayOffset : dailyDuration.keys()){
        rec.date = s.startDate.addDays(dayOffset);
        rec.duration = dailyDuration[dayOffset];
        m_sessionStats[s.project].append(rec);

        p.dailyDurations->add(rec.date,rec.duration);
        if (p.dailyDurations30days->isInRange(rec.date))
            p.dailyDurations30days->add(rec.date,rec.duration);
        if (p.dailyDurations7days->isInRange(rec.date))
            p.dailyDurations7days->add(rec.date,rec.duration);

        m_topics->addDailyDuration(p.topic,rec.date,rec.duration);
    }
}

void ProjectManager::onSessionRemoved(const uint uid){
    for (int project : m_sessionStats.keys()){
        SessionStatsRecordList &recs = m_sessionStats[project];
        for (int i=0; i<recs.size(); i++){
            const SessionStatsRecord &rec = recs.at(i);
            if (rec.session == uid){
                const int idx = m_allProjects.find(project);
                if (idx < 0)
                    continue;

                Project &p = m_allProjects[idx];

                p.dailyDurations->add(rec.date,-rec.duration);
                if (p.dailyDurations30days->isInRange(rec.date))
                    p.dailyDurations30days->add(rec.date,-rec.duration);
                if (p.dailyDurations7days->isInRange(rec.date))
                    p.dailyDurations7days->add(rec.date,-rec.duration);

                m_topics->addDailyDuration(p.topic,rec.date,-rec.duration);

                recs.removeAt(i--);
            }
        }
    }
}

void ProjectManager::onSessionUpdated(const uint uid){
    onSessionRemoved(uid);
    onSessionAdded(uid);
}
