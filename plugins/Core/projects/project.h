/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef PROJECT_H
#define PROJECT_H

#include <QString>
#include <QColor>
#include <QDate>
#include <QVector>

#include "types/dailyvaluemodel.h"

const qreal projectColorHueStepWidth        = 1./90.;
const qreal projectColorSaturationStepWidth = 0.2;
const qreal projectColorLightnessStepWidth  = 0.1;

struct Project{
    enum Status{
        ACTIVE,
        INACTIVE,
        ARCHIVED
    };

    Project() : dailyDurations(new DailyValueModel), dailyDurations30days(new DailyValueModel), dailyDurations7days(new DailyValueModel){
        dailyDurations30days->setTimeRange(QDate::currentDate().addDays(-29),QDate::currentDate());
        dailyDurations7days->setTimeRange(QDate::currentDate().addDays(-6),QDate::currentDate());
    }

    uint    uid = 0;
    QString title;
    QColor  color;
    int     colorHue = 0;
    int     colorLightness = 0;
    uint    topic = 0;
    uint    rank = 0;
    QDate   creationDate;
    Status  status = ACTIVE;

    DailyValueModel *dailyDurations;
    DailyValueModel *dailyDurations30days;
    DailyValueModel *dailyDurations7days;

    static QColor getColor(const QColor &baseColor, const int hue, const int lightness);
};

bool operator==(const Project &t1, const Project &t2);

class ProjectList : public QVector<Project>
{
public:
    void removeByUid(const uint uid);
    int find(const int uid) const;
};

#endif // PROJECT_H
