/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "projectmodel.h"

ProjectModel::ProjectModel(QObject *parent)
    : QAbstractListModel(parent){
}

int ProjectModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant ProjectModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case UIDROLE:
        return QVariant(m_list.at(index.row()).uid);
    case TITLEROLE:
        return QVariant(m_list.at(index.row()).title);
    case COLORROLE:
        return QVariant(m_list.at(index.row()).color);
    case COLORHUEROLE:
        return QVariant(m_list.at(index.row()).colorHue);
    case COLORLIGHTNESSROLE:
        return QVariant(m_list.at(index.row()).colorLightness);
    case TOPICROLE:
        return QVariant(m_list.at(index.row()).topic);
    case RANKROLE:
        return QVariant(m_list.at(index.row()).rank);
    case STATUSROLE:
        return QVariant(m_list.at(index.row()).status);
    case DAILYDURATIONROLE:
        return QVariant::fromValue<DailyValueModel*>(m_list.at(index.row()).dailyDurations);
    case DAILYDURATION30DAYSROLE:
        return QVariant::fromValue<DailyValueModel*>(m_list.at(index.row()).dailyDurations30days);
    case DAILYDURATION7DAYSROLE:
        return QVariant::fromValue<DailyValueModel*>(m_list.at(index.row()).dailyDurations7days);
    case CREATIONDATEROLE:
        return QVariant(m_list.at(index.row()).creationDate);
    }
    return QVariant();
}

QHash<int, QByteArray> ProjectModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[UIDROLE]    = "projectUid";
    names[TITLEROLE]  = "projectTitle";
    names[COLORROLE]  = "projectColor";
    names[COLORHUEROLE]  = "projectColorHue";
    names[COLORLIGHTNESSROLE]  = "projectColorLightness";
    names[RANKROLE]   = "projectRank";
    names[TOPICROLE]  = "projectTopic";
    names[STATUSROLE] = "projectStatus";
    names[DAILYDURATIONROLE] = "projectDailyDurationsModel";
    names[DAILYDURATION30DAYSROLE] = "projectDailyDurations30DaysModel";
    names[DAILYDURATION7DAYSROLE] = "projectDailyDurations7DaysModel";
    names[CREATIONDATEROLE] = "projectCreationDate";
    return names;
}

int ProjectModel::indexOf(const uint uid) const{
    for (int i=0; i<m_list.size(); i++)
        if (m_list.at(i).uid == uid)
            return i;
    return -1;
}

Project ProjectModel::get(const uint idx) const{
    return (idx < m_list.size()) ? m_list.at(idx) : Project();
}

void ProjectModel::setList(const ProjectList &list){
    const uint oldCount = m_list.size();

    beginResetModel();
    m_list.clear();
    for (const Project &p : list){
        int i;
        for (i=0; i<m_list.size(); i++)
            if (m_list.at(i).rank > p.rank)
                break;
        m_list.insert(i,p);
    }

    endResetModel();

    if (m_list.count() != oldCount)
        emit countChanged();

    updateStatusCounter();
}

void ProjectModel::insert(const Project &p){
    const int L = m_list.size();
    int i;
    for (i=0; i<L; i++)
        if (p.rank < m_list.at(i).rank)
            break;

    beginInsertRows(QModelIndex(),i,i);
    m_list.insert(i,p);
    endInsertRows();
    emit countChanged();

    switch (p.status) {
    case Project::ACTIVE:
        m_activeCount++;
        emit activeCountChanged();
        break;
    case Project::INACTIVE:
        m_inactiveCount++;
        emit inactiveCountChanged();
        break;
    }


}

void ProjectModel::update(const Project &p, const bool insertIfNotFound){
    const int idx = m_list.indexOf(p);
    if (idx < 0){
        if (insertIfNotFound)
            insert(p);
        return;
    }

    m_list.replace(idx,p);
    emit dataChanged(index(idx),index(idx));

    updateStatusCounter();
}

void ProjectModel::removeProject(const uint uid){
    for (int i=0; i< m_list.size(); i++){
        if (m_list.at(i).uid == uid){
            switch (m_list.at(i).status) {
            case Project::ACTIVE:
                m_activeCount--;
                emit activeCountChanged();
                break;
            case Project::INACTIVE:
                m_inactiveCount--;
                emit inactiveCountChanged();
                break;
            }

            beginRemoveRows(QModelIndex(),i,i);
            m_list.removeAt(i);
            endRemoveRows();
            emit countChanged();

            return;
        }
    }
}

void ProjectModel::swap(const uint uid1, const uint uid2){
    const int idx1 = m_list.find(uid1);
    if (idx1 < 0)
        return;
    const int idx2 = m_list.find(uid2);
    if (idx2 < 0)
        return;

    beginMoveRows(QModelIndex(),idx1,idx1,QModelIndex(),idx2);
    m_list.move(idx1,idx2);
    endMoveRows();
}

void ProjectModel::clear(){
    if (m_list.size() == 0)
        return;

    beginResetModel();
    m_list.clear();
    endResetModel();

    emit countChanged();

    m_activeCount = 0;
    emit activeCountChanged();
    m_inactiveCount = 0;
    emit inactiveCountChanged();
}

void ProjectModel::updateStatusCounter(){
    const uint oldActiveCount = m_activeCount;
    const uint oldInactiveCount = m_inactiveCount;

    uint activeCount = 0;
    uint inactiveCount = 0;

    for (const Project &p : m_list){
        switch (p.status) {
        case Project::ACTIVE:
            activeCount++;
            break;
        case Project::INACTIVE:
            inactiveCount++;
            break;
        }
    }

    if (activeCount != oldActiveCount){
        m_activeCount = activeCount;
        emit activeCountChanged();
    }

    if (inactiveCount != oldInactiveCount){
        m_inactiveCount = inactiveCount;
        emit inactiveCountChanged();
    }
}
