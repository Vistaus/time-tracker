/*
 * Copyright (C) 2022  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QAbstractListModel>

#include "project.h"

class ProjectModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged)
    Q_PROPERTY(uint activeCount READ activeCount NOTIFY activeCountChanged)
    Q_PROPERTY(uint inactiveCount READ inactiveCount NOTIFY inactiveCountChanged)
public:
    enum Roles{
        UIDROLE = Qt::UserRole,
        TITLEROLE,
        COLORROLE,
        COLORHUEROLE,
        COLORLIGHTNESSROLE,
        TOPICROLE,
        RANKROLE,
        STATUSROLE,
        DAILYDURATIONROLE,
        DAILYDURATION30DAYSROLE,
        DAILYDURATION7DAYSROLE,
        CREATIONDATEROLE
    };

    explicit ProjectModel(QObject *parent = nullptr);


    /* --------------------- *
     * --- QML interface --- */

    inline uint count() const {return m_list.size();}
    inline uint activeCount() const {return m_activeCount;}
    inline uint inactiveCount() const {return m_inactiveCount;}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE int indexOf(const uint uid) const;

    /* --------------------- *
     * --- C++ interface --- */

    Project get(const uint idx) const;

    void setList(const ProjectList &list);

    void insert(const Project &p);
    void update(const Project &p, const bool insertIfNotFound = false);
    void removeProject(const uint uid);
    void swap(const uint uid1, const uint uid2);
    void clear();

signals:
    void countChanged();
    void activeCountChanged();
    void inactiveCountChanged();

private:
    ProjectList m_list;

    uint m_activeCount = 0;
    uint m_inactiveCount = 0;

    void updateStatusCounter();

};

#endif // PROJECTMODEL_H
